@extends('frontend.layouts.app')
@section('title','Our Values')
@section('css')
    <link rel='stylesheet' id='sage/app.css-css' href="{{asset('assets/frontend/css/app.css')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='sage/app.css-css' href="{{asset('assets/frontend/css/reset.css')}}" type='text/css' media='all' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{asset('assets/frontend/css/style.min.css')}}" />
    <style>
        .zoom-explore-story {
            zoom: 1.7;
        }

        .concentrato-slider-arrows .svg-inline--fa {
            background-color: #e01020;
            width: 50px;
            height: 50px;
            border-radius: 50%;
            padding: 10px;
        }

        .concentrato-video .video-js .vjs-big-play-button {
            background-color: rgba(43, 51, 63, 0.7);
        }

        @media (min-width: 1024px) {
            .fr-mutti-professionel-carousel .swiper-pagination.swiper-pagination {
                margin-top: 1rem;
            }
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide {
            order: 0;
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide-thumb-active {
            order: 1;
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide-thumb-active+.swiper-slide {
            order: 2;
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide-thumb-active:first-child+.swiper-slide {
            order: 0;
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide:not(.swiper-slide-thumb-active)+.swiper-slide:not(.swiper-slide-thumb-active) {
            order: 2;
        }

        .mutti-professionel .swiper-pagination-bullet:not(.bg-primary) {
            background-color: #CDCAC0 !important;
            opacity: 1 !important;
        }
        .swiper-container-initialized .swiper-slide {

            display:none!important;
        }
        .swiper-container-initialized .swiper-slide-active{

            display:block!important;
        }
        .swiper-button-next svg, .swiper-button-prev svg {
            width: 50%;
            height: 20%;

        }
    </style>
@endsection
@section('content')
    <!-- zina house section start -->
    <div data-spai-bg-prepared="1"
         data-vue-wrapper=""
         data-vue-initialized="true"
         class="acf-hero-video-player alignfull acf-video-player has-fill-stretch bg-cover bg-center"
         @if($our_values_setting && $our_values_setting->section_1_image)
            style="background-image: url('{{asset('storage/our_value/'.$our_values_setting->section_1_image)}}');
    @else
          style="background-image: url('{{asset('assets/frontend/images/zina_house.jpg')}}');
    @endif
    --vue-placeholder-height: 600px;">
    <div class="responsive-embed " style="padding-bottom: 35.3125%;">
        <div class="responsive-embed-item">
            <div class="relative h-full">
                <div class="transition-opacity absolute inset-0 opacity-0">
                    <video playsinline="" controls="controls" loop="loop" autoplay="autoplay"
                           @if($our_values_setting && $our_values_setting->section_1_image)
                               poster="{{asset('storage/our_value/'.$our_values_setting->section_1_image)}}"
                           @else
                               poster="{{asset('assets/frontend/images/zina_house.jpg')}}"
                           @endif
                           controlslist="nofullscreen nodownload noremoteplayback noplaybackrate"
                           disablepictureinpicture="" preload="metadata" data-name="Hero: about"
                           class="video-js w-full absolute inset-0" data-initialized-tracking="true"
                           src="#">
                    </video>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="wp-block-cover alignwide">
        <span aria-hidden="true" class="wp-block-cover__background has-pitchblack-background-color has-background-dim"></span>
        <img decoding="async" class="wp-block-cover__image-background wp-image-300000165" alt=""
             @if($our_values_setting && $our_values_setting->section_2_bg_image)
                 src="{{asset('storage/our_value/'.$our_values_setting->section_2_bg_image)}}"
             @else
                 src="{{asset('assets/frontend/images/section_2_bg_image.jpg')}}"
             @endif
             data-spai="1" style="object-position:29% 24%"
             data-object-fit="cover"
             data-object-position="29% 24%" data-spai-loading="lazy"
             data-spai-lazy-loaded="true" data-spai-width="1440" data-spai-height="446.375" loading="lazy">
        <noscript data-spai="1">
            <img decoding="async" class="wp-block-cover__image-background wp-image-300000165"
                 @if($our_values_setting && $our_values_setting->section_2_bg_image)
                     src="{{asset('storage/our_value/'.$our_values_setting->section_2_bg_image)}}"
                 @else
                     src="{{asset('assets/frontend/images/section_2_bg_image.jpg')}}"
                 @endif
                 data-spai-egr="1" style="object-position:29% 24%" data-object-fit="cover" data-object-position="29% 24%"
                 @if($our_values_setting && $our_values_setting->section_2_bg_image)
                     srcset="{{asset('storage/our_value/'.$our_values_setting->section_2_bg_image)}}"
                 @else
                     srcset="{{asset('assets/frontend/images/section_2_bg_image.jpg')}}"
                 @endif
                 sizes="(max-width: 2400px) 100vw, 2400px" />
        </noscript>
        <div class="wp-block-cover__inner-container is-layout-flow wp-block-cover-is-layout-flow">
            <div class="acf-breadcrumb my-4 md:my-8 text-center">
                <nav>
                    <ol class="list-none text-md">
                        <li class="inline">
                            <a class="text-inherit" href="{{route('welcome')}}">
                                @if($front_navbar && $front_navbar->welcome_menu_text  )
                                    {{$front_navbar->welcome_menu_text  }}
                                @else
                                    Welcome
                                @endif
                            </a>
                            <span aria-hidden="true"> /&nbsp;</span>
                        </li>
                        <li class="inline">
                            <a class="text-inherit" href="{{route('our_values')}}">
                                @if($front_navbar && $front_navbar->our_values_menu_text   )
                                    {{$front_navbar->our_values_menu_text   }}
                                @else
                                    Our Value
                                @endif
                            </a>
                        </li>
                    </ol>
                </nav>
            </div>
            <h1 class="has-text-align-center wp-block-heading">
                @if($our_values_setting && $our_values_setting->section_2_heading)
                    {{$our_values_setting->section_2_heading}}
                @else
                    ABOUT ZINA
                @endif
            </h1>
            <br>
            <p class="has-text-align-center is-style-cursive" style='font-family: "Montserrat", sans-serif;'>
                @if($our_values_setting && $our_values_setting->section_2_text)
                    {{$our_values_setting->section_2_text}}
                @else
                    A long love affair with the Tunisian tomato
                @endif
            </p>
            <p class="has-text-align-center">
                @if($our_values_setting && $our_values_setting->section_2_paragraph)
                    {{$our_values_setting->section_2_paragraph}}
                @else
                    As a company, our mission is to give Tunisian tomatoes the means of expression they deserve. This is the objective that has driven us for one hundred and twenty years. Even today, we learn new lessons from this wonderful fruit and continually improve every aspect of its production to make the best canned tomatoes and tomato sauces.
                @endif
            </p>
            <p class="has-text-align-center is-style-cursive">
                @if($our_values_setting && $our_values_setting->section_2_bottom_text)
                    {{$our_values_setting->section_2_bottom_text}}
                @else
                    It all starts with…
                @endif
            </p>
        </div>
    </div>
    <!-- our product -->
    <section id="ourProduct" >
        <div class="container">
            <main class="max-w-container mx-auto px-4 lg:px-3 flex-grow w-full menu:mt-40 mb-12">
                <div class="max-w-content mx-auto entry-content post-4 page type-page status-publish hentry">
                    <div class="wp-block-media-text alignwide is-stacked-on-mobile">
                        <figure class="wp-block-media-text__media">
                            <img decoding="async"
                                 @if($our_values_setting && $our_values_setting->section_3_side_image)
                                     src="{{asset('storage/our_value/'.$our_values_setting->section_3_side_image)}}"
                                 @else
                                     src="{{asset('assets/frontend/images/section-3-side.jpg')}}"
                                 @endif
                                 data-spai="1" alt="" class="wp-image-30000048 size-full"
                                 sizes="(max-width: 600px) 100vw, (min-width: 1440px) 792px,55vw" data-spai-crop="false" srcset=" "
                                 data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="792" data-spai-height="528.25" loading="lazy">
                            <noscript data-spai="1">
                                <img decoding="async"
                                     @if($our_values_setting && $our_values_setting->section_3_side_image)
                                         src="{{asset('storage/our_value/'.$our_values_setting->section_3_side_image)}}"
                                     @else
                                         src="{{asset('assets/frontend/images/section-3-side.jpg')}}"
                                     @endif
                                     data-spai-egr="1" alt="" class="wp-image-30000048 size-full"
                                     sizes="(max-width: 600px) 100vw, (min-width: 1440px) 792px,55vw" data-spai-crop="false"
                                     @if($our_values_setting && $our_values_setting->section_3_side_image)
                                         srcset="{{asset('storage/our_value/'.$our_values_setting->section_3_side_image)}}"
                                     @else
                                         srcset="{{asset('assets/frontend/images/section-3-side.jpg')}}"
                                     @endif
                                      />
                            </noscript>
                        </figure>
                        <div class="wp-block-media-text__content">
                            <h2 class="wp-block-heading">

                                @if($our_values_setting && $our_values_setting->section_3_top_heading)
                                    {{$our_values_setting->section_3_top_heading}}
                                @else
                                    Passion like the first day
                                @endif
                            </h2>
                            <p class="is-style-cursive has-xxl-font-size">

                                @if($our_values_setting && $our_values_setting->section_3_text)
                                    {{$our_values_setting->section_3_text}}
                                @else
                                    Our society today
                                @endif
                            </p>
                            <p>
                                @if($our_values_setting && $our_values_setting->section_3_paragraph)
                                    {{$our_values_setting->section_3_paragraph}}
                                @else
                                    We are keen to create the best canned tomatoes for our customers. Our headquarters is in Parma, Italy, where our company was founded 120 years ago. In the future, we will continue to share with millions of new fans our love for the flavors of Tunisian tomatoes, for innovation and sustainability
                                @endif
                            </p>
                            <p class="is-style-cursive has-xxl-font-size">
                                @if($our_values_setting && $our_values_setting->section_3_bottom_heading)
                                    {{$our_values_setting->section_3_bottom_heading}}
                                @else
                                    For people looking for…
                                @endif
                            </p>
                            <h3 class="wp-block-heading">
                                @if($our_values_setting && $our_values_setting->section_3_last_text)
                                    {{$our_values_setting->section_3_last_text}}
                                @else
                                    THE BEST OF 100% TUNISIAN TOMATOES
                                @endif
                            </h3>
                        </div>
                    </div>
                    <div class="wp-block-group alignwide is-style-gradient has-secondary-background-color has-background is-layout-flow wp-block-group-is-layout-flow">
                        <h2 class="has-text-align-center wp-block-heading">

                            @if($our_values_setting && $our_values_setting->section_4_heading)
                                {{$our_values_setting->section_4_heading}}
                            @else
                                METHOD AND KNOW-HOW
                            @endif
                        </h2>
                        <p class="has-text-align-center">

                            @if($our_values_setting && $our_values_setting->section_4_paragraph)
                                {{$our_values_setting->section_4_paragraph}}
                            @else
                                Zina's passion for Tunisian tomatoes is based on three watchwords: quality, transparency and reliability.
                                Working well requires dedication, consistency and intelligence, but also great passion and honesty
                            @endif
                        </p>
                        <p class="has-text-align-center is-style-cursive">
                            @if($our_values_setting && $our_values_setting->section_4_ceo_name)
                                {{$our_values_setting->section_4_ceo_name}}
                            @else
                                Francesco Mutti
                            @endif
                        </p>
                        <p class="has-text-align-center has-primary-color has-text-color">

                            @if($our_values_setting && $our_values_setting->section_4_bottom_text)
                                {{$our_values_setting->section_4_bottom_text}}
                            @else
                                CEO of Zina SpA
                            @endif
                        </p>
                    </div>
                    <div class="wp-block-group alignwide is-style-default">
                        <div class="wp-block-group__inner-container is-layout-flow wp-block-group-is-layout-flow">
                            <div class="wp-block-columns alignwide is-layout-flex wp-container-9 wp-block-columns-is-layout-flex">
                                <div class="wp-block-column is-layout-flow wp-block-column-is-layout-flow">
                                    <h2 class="wp-block-heading" id="sustainability">
                                        @if($our_values_setting && $our_values_setting->section_5_text)
                                            {{$our_values_setting->section_5_text}}
                                        @else
                                            SUSTAINABILITY
                                        @endif
                                    </h2>
                                    <p class="is-style-cursive">
                                        @if($our_values_setting && $our_values_setting->section_5_heading)
                                            {{$our_values_setting->section_5_heading}}
                                        @else
                                            For us, sustainability is synonymous with respect for the earth
                                        @endif

                                    </p>
                                    <h4 class="wp-block-heading">
                                        @if($our_values_setting && $our_values_setting->section_5_bottom_text)
                                            {{$our_values_setting->section_5_bottom_text}}
                                        @else
                                            COOPERATION WITH WWF
                                        @endif
                                    </h4>
                                    <p>
                                        @if($our_values_setting && $our_values_setting->section_5_paragraph)
                                            {{$our_values_setting->section_5_paragraph}}
                                        @else
                                            Zina has worked closely with WWF Italy to help farmers find sustainable ways to reduce their water consumption and CO2 emissions. WWF Italy not only set targets, but it also allowed farmers to analyze and review the data obtained in order to identify actions they could take. For example, the organization recommended using special sensors that measure soil moisture and provide useful information to rationalize water consumption.
                                        @endif
                                    </p>
                                    <figure class="wp-block-image size-large is-resized">
                                        <img decoding="async" src="https://cdn.shortpixel.ai/spai/q_lossy+w_130+h_173+to_webp+ret_img/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf.png" data-spai="1" alt="" class="wp-image-16372" width="129" height="173" srcset=" " sizes="(max-width: 129px) 100vw, 129px" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="129" data-spai-height="172" loading="lazy">
                                        <noscript data-spai="1">
                                            <img decoding="async" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf-768x1024.png" data-spai-egr="1" alt="" class="wp-image-16372" width="129" height="173" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf-768x1024.png 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf-375x500.png 375w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf-150x200.png 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf-600x800.png 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf-1152x1536.png 1152w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf.png 1251w" sizes="(max-width: 129px) 100vw, 129px" />
                                        </noscript>
                                    </figure>
                                </div>
                                <div class="wp-block-column is-layout-flow wp-block-column-is-layout-flow">
                                    <div class="wp-block-group has-green-background-color has-background">
                                        <div class="wp-block-group__inner-container is-layout-flow wp-block-group-is-layout-flow">
                                            <h2 class="has-text-align-center wp-block-heading">

                                                @if($our_values_setting && $our_values_setting->section_5_results_text)
                                                    {{$our_values_setting->section_5_results_text}}
                                                @else
                                                    RESULTS
                                                @endif
                                            </h2>
                                            <div class="wp-block-columns is-layout-flex wp-container-6 wp-block-columns-is-layout-flex">
                                                <div class="wp-block-column is-layout-flow wp-block-column-is-layout-flow">
                                                    <div class="wp-block-image">
                                                        <figure class="aligncenter size-large is-resized">
                                                            <img decoding="async" src="https://cdn.shortpixel.ai/spai/q_lossy+to_webp+ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/icon-sustainable-water.svg" data-spai="1" alt="" class="wp-image-30000085" width="53" height="91" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="53" data-spai-height="91.2344" loading="lazy">
                                                            <noscript data-spai="1">
                                                                <img decoding="async" src="https://cdn.shortpixel.ai/spai/ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/icon-sustainable-water.svg" data-spai-egr="1" alt="" class="wp-image-30000085" width="53" height="91" />
                                                            </noscript>
                                                        </figure>
                                                    </div>
                                                    <h4 class="has-text-align-center wp-block-heading">
                                                        @if($our_values_setting && $our_values_setting->section_5_drop_heading)
                                                            {{$our_values_setting->section_5_drop_heading}}
                                                        @else
                                                            1,000 MILLION LITERS OF WATER SAVED
                                                        @endif
                                                    </h4>
                                                    <p class="has-text-align-center has-sm-font-size">
                                                        @if($our_values_setting && $our_values_setting->section_5_drop_paragraph)
                                                            {{$our_values_setting->section_5_drop_paragraph}}
                                                        @else
                                                            between 2012 and 2014 (Source: data collected by WWF and the Euro-Mediterranean Center on Climate Change in Italy)
                                                        @endif
                                                    </p>

                                                </div>
                                                <div class="wp-block-column is-layout-flow wp-block-column-is-layout-flow">
                                                    <div class="wp-block-image">
                                                        <figure class="aligncenter size-large is-resized">
                                                            <img decoding="async"
                                                                 src="https://cdn.shortpixel.ai/spai/q_lossy+to_webp+ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/icon-sustainable-co2.svg"
                                                                 data-spai="1" alt="" class="wp-image-30000086"
                                                                 width="114" height="88" data-spai-loading="lazy"
                                                                 data-spai-lazy-loaded="true" data-spai-width="114" data-spai-height="88.1875" loading="lazy">
                                                            <noscript data-spai="1">
                                                                <img decoding="async"
                                                                     src="https://cdn.shortpixel.ai/spai/ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/icon-sustainable-co2.svg"
                                                                     data-spai-egr="1" alt="" class="wp-image-30000086" width="114" height="88" />
                                                            </noscript>
                                                        </figure>
                                                    </div>
                                                    <h4 class="has-text-align-center wp-block-heading">
                                                        @if($our_values_setting && $our_values_setting->section_5_co2_heading)
                                                            {{$our_values_setting->section_5_co2_heading}}
                                                        @else
                                                            31,530 TONNES OF CO2 (CARBON DIOXIDE) AVOIDED.
                                                        @endif
                                                    </h4>
                                                    <p class="has-text-align-center has-sm-font-size">
                                                        @if($our_values_setting && $our_values_setting->section_5_co2_paragraph)
                                                            {{$our_values_setting->section_5_co2_paragraph}}
                                                        @else
                                                            between 2010 and 2015 compared to 2009 levels (Source: data collected by WWF and energy efficiency group Officinae Verd
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wp-block-media-text alignwide is-stacked-on-mobile">
                        <figure class="wp-block-media-text__media">
                            <img decoding="async"
                                 @if($our_values_setting && $our_values_setting->section_6_side_image)
                                     src="{{asset('storage/our_value/'.$our_values_setting->section_6_side_image)}}"
                                 @else
                                     src="{{asset('assets/frontend/images/section_7.png')}}"
                                 @endif
                                 data-spai="1" alt="" class="wp-image-300000162 size-full"
                                 sizes="(max-width: 600px) 100vw, (min-width: 1440px) 792px,55vw"
                                 data-spai-crop="false" srcset=" "
                                 data-spai-loading="lazy" data-spai-lazy-loaded="true"
                                 data-spai-width="792" data-spai-height="528.25" loading="lazy">
                            <noscript data-spai="1">
                                <img decoding="async"
                                     @if($our_values_setting && $our_values_setting->section_6_side_image)
                                         src="{{asset('storage/our_value/'.$our_values_setting->section_6_side_image)}}"
                                     @else
                                         src="{{asset('assets/frontend/images/section_7.png')}}"
                                     @endif
                                     data-spai-egr="1" alt=""
                                     class="wp-image-300000162 size-full" sizes="(max-width: 600px) 100vw, (min-width: 1440px) 792px,55vw"
                                     data-spai-crop="false"
                                     @if($our_values_setting && $our_values_setting->section_6_side_image)
                                         srcset="{{asset('storage/our_value/'.$our_values_setting->section_6_side_image)}}"
                                     @else
                                         srcset="{{asset('assets/frontend/images/section_7.png')}}"
                                     @endif
                                />
                            </noscript>
                        </figure>
                        <div class="wp-block-media-text__content">
                            <h2 class="wp-block-heading">
                                @if($our_values_setting && $our_values_setting->section_6_top_text)
                                    {{$our_values_setting->section_6_top_text}}
                                @else
                                    THE GOLDEN TOMATO: “POMODORINO D’ORO”
                                @endif
                            </h2>
                            <br>
                            <p class="is-style-cursive has-xxl-font-size">

                                @if($our_values_setting && $our_values_setting->section_6_heading)
                                    {{$our_values_setting->section_6_heading}}
                                @else
                                    The quest for perfection
                                @endif
                            </p>
                            <p>
                                @if($our_values_setting && $our_values_setting->section_6_paragraph)
                                    {{$our_values_setting->section_6_paragraph}}
                                @else
                                    We do everything to offer you the best quality products. This is why, every year, Zina gives a quality prize to farmers with the Pomodorino d´oro (Golden Tomato).

                                @endif
                            </p>
                            <div class="wp-block-button">
                                <a class="wp-block-button__link" href="#">
                                    @if($our_values_setting && $our_values_setting->section_6_learn_more)
                                        {{$our_values_setting->section_6_learn_more}}
                                    @else
                                        Learn More
                                    @endif
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="wp-block-media-text alignwide has-media-on-the-right is-stacked-on-mobile">
                        <figure class="wp-block-media-text__media">
                            <img decoding="async"
                                 @if($our_values_setting && $our_values_setting->section_7_image)
                                     src="{{asset('storage/zina_house/'.$our_values_setting->section_7_image)}}"
                                 @else
                                     src="{{asset('assets/frontend/images/section_8.png')}}"
                                 @endif
                                 data-spai="1" alt="" class="wp-image-30000010 size-full" data-spai-crop="false" data-spai-loading="lazy"
                                 data-spai-lazy-loaded="true" data-spai-width="792" data-spai-height="792" loading="lazy">
                            <noscript data-spai="1">
                                <img decoding="async"
                                     @if($our_values_setting && $our_values_setting->section_7_image)
                                         src="{{asset('storage/zina_house/'.$our_values_setting->section_7_image)}}"
                                     @else
                                         src="{{asset('assets/frontend/images/section_8.png')}}"
                                     @endif

                                     data-spai-egr="1" alt="" class="wp-image-30000010 size-full"
                                     sizes="(max-width: 600px) 100vw, (min-width: 1440px) 792px,55vw" data-spai-crop="false"
                                     @if($our_values_setting && $our_values_setting->section_7_image)
                                         srcset="{{asset('storage/zina_house/'.$our_values_setting->section_7_image)}}"
                                     @else
                                         srcset="{{asset('assets/frontend/images/section_8.png')}}" />
                                @endif

                            </noscript>
                        </figure>
                        <div class="wp-block-media-text__content">
                            <h2 class="wp-block-heading">

                                @if($our_values_setting && $our_values_setting->section_7_top_text)
                                    {{$our_values_setting->section_7_top_text}}
                                @else
                                    WHERE DO THE BEST TOMATOES COME FROM?
                                @endif
                            </h2>
                            <p>
                                @if($our_values_setting && $our_values_setting->section_7_paragraph)
                                    {{$our_values_setting->section_7_paragraph}}
                                @else
                                    The story of our Tunisian tomatoes begins in Italian fields. Each soil type and tomato variety has its own unique characteristics. Every step of the process, from field to table, affects taste.
                                @endif
                            </p>
                            <div class="wp-block-button">
                                <a class="wp-block-button__link" href="#">
                                    @if($our_values_setting && $our_values_setting->section_7_heading)
                                        {{$our_values_setting->section_7_heading}}
                                    @else
                                        LEARN MORE ABOUT OUR TUNISIAN TOMATOES
                                    @endif
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </main>


        </div>
    </section>
    <!-- our values section start -->
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
    <script>

        var Swipes = new Swiper('.swiper-container_top', {
            loop: true,
            slidesPerView: 4,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
            },
        });

        var Swipes = new Swiper ('.swiper-container_catg_product', {
            // Optional parameters
            direction: 'horizontal',
            loop: false,
            slidesPerView: 1,
            spaceBetween: 25,
            breakpoints: {
                320: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                },
                600: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                },
                991: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                },
                1240: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                },
                1600: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                }
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        })
        var Swipes = new Swiper('.swiper-container-initialized ', {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
            },
        });
    </script>
@endsection


