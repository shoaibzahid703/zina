@extends('frontend.layouts.app')
@section('title',$product->title)
@section('css')

@endsection

@section('content')

    <section id="ourProduct" class="py-lg-5 py-3">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center d-flex justify-content-center">
                    <img src="{{asset('storage/products/'.$product->product_image )}}" class="img-fluid">
                </div>
                <div class="col-md-12 text-center mt-2">
                    <h1>{{$product->title}}</h1>
                </div>
                <div class="col-md-12 mt-2">
                    {!! $product->description !!}
                </div>
            </div>
        </div>

    </section>
    <!-- our values section start -->

@endsection
