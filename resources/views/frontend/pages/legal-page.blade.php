@extends('frontend.layouts.app')
@section('title', $legal_page->name)
@section('css')
    <link rel='stylesheet' id='sage/app.css-css' href="{{asset('assets/frontend/css/app.css')}}" type='text/css'
          media='all'/>
    <link rel='stylesheet' id='sage/app.css-css' href="{{asset('assets/frontend/css/reset.css')}}" type='text/css'
          media='all'/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css"/>
    <link rel="stylesheet" href="{{asset('assets/frontend/css/style.min.css')}}"/>
@endsection
@section('content')

    <section id="ourProduct" class="py-lg-5 py-3">
        <div class="container">

            <div class="max-w-content mx-auto entry-content post-8 page type-page status-publish hentry">
                @if($legal_page && $legal_page->page_image)
                    <div data-spai-bg-prepared="1"
                         class="wp-block-cover alignfull has-black-background-color has-background-dim"
                         style="background-image:url('{{asset('storage/pages/'.$legal_page->page_image)}}');background-position:54.70059712727865% 47.59843480868602%">
                        @else
                            <div data-spai-bg-prepared="1"
                                 class="wp-block-cover alignfull has-black-background-color has-background-dim"
                                 style="background-image:url(https://cdn.shortpixel.ai/spai/w_1920+q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/05/actualites-mutti-france.jpg);background-position:54.70059712727865% 47.59843480868602%">
                                @endif
                                <div
                                    class="wp-block-cover__inner-container is-layout-flow wp-block-cover-is-layout-flow">
                                    <div class="acf-breadcrumb my-4 md:my-8 text-center">
                                        <nav>
                                            <ol class="list-none text-md">
                                                <li class="inline">
                                                    <a class="text-inherit" href="{{route('welcome')}}">
                                                        Welcome
                                                    </a>
                                                    <span aria-hidden="true"> /&nbsp;</span>
                                                </li>
                                                <li class="inline">
                                                    <a class="text-inherit" href="{{route('page',$legal_page)}}">
                                                        {{$legal_page->name}}
                                                    </a>
                                                </li>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>
                            </div>


                            <div class="acf-content-listing alignwide">
                                <div class="flex -mx-6 -mt-5 md:-mt-6 flex-wrap">
                                    {!! $legal_page->description  !!}
                                </div>
                            </div>

                    </div>
            </div>

    </section>
@endsection
@section('js')

    <script src="{{asset('assets/frontend/js/owl.carousel.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
@endsection
