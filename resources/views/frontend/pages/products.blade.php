@extends('frontend.layouts.app')
@section('title','Products')
@section('css')
    <link rel='stylesheet' id='sage/app.css-css' href="{{asset('assets/frontend/css/app.css')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='sage/app.css-css' href="{{asset('assets/frontend/css/reset.css')}}" type='text/css' media='all' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{asset('assets/frontend/css/style.min.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/frontend/css/owl.carousel.min.css')}}">
    <style>
        .zoom-explore-story {
            zoom: 1.7;
        }

        .concentrato-slider-arrows .svg-inline--fa {
            background-color: #e01020;
            width: 50px;
            height: 50px;
            border-radius: 50%;
            padding: 10px;
        }

        .concentrato-video .video-js .vjs-big-play-button {
            background-color: rgba(43, 51, 63, 0.7);
        }

        @media (min-width: 1024px) {
            .fr-mutti-professionel-carousel .swiper-pagination.swiper-pagination {
                margin-top: 1rem;
            }
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide {
            order: 0;
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide-thumb-active {
            order: 1;
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide-thumb-active+.swiper-slide {
            order: 2;
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide-thumb-active:first-child+.swiper-slide {
            order: 0;
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide:not(.swiper-slide-thumb-active)+.swiper-slide:not(.swiper-slide-thumb-active) {
            order: 2;
        }

        .mutti-professionel .swiper-pagination-bullet:not(.bg-primary) {
            background-color: #CDCAC0 !important;
            opacity: 1 !important;
        }

        .owl-carousel {
            position: relative; }
        .owl-carousel .owl-item {
            opacity: 1; }
        .owl-carousel .owl-item.active {
            opacity: 1; }
        .owl-carousel .owl-nav {
            position: absolute;
            top: 50%;
            width: 100%; }
        .owl-carousel .owl-nav .owl-prev,
        .owl-carousel .owl-nav .owl-next {
            position: absolute;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
            margin-top: -60px;
            color: rgba(0, 0, 0, 0.1) !important;
            -webkit-transition: 0.7s;
            -o-transition: 0.7s;
            transition: 0.7s;
            opacity: 0; }
        @media (prefers-reduced-motion: reduce) {
            .owl-carousel .owl-nav .owl-prev,
            .owl-carousel .owl-nav .owl-next {
                -webkit-transition: none;
                -o-transition: none;
                transition: none; } }
        .owl-carousel .owl-nav .owl-prev span:before,
        .owl-carousel .owl-nav .owl-next span:before {
            font-size: 30px; }
        .owl-carousel .owl-nav .owl-prev {
            left: 0; }
        .owl-carousel .owl-nav .owl-next {
            right: 0; }
        .owl-carousel .owl-dots {
            text-align: center;
            margin-top: 20px; }
        .owl-carousel .owl-dots .owl-dot {
            width: 10px;
            height: 10px;
            margin: 5px;
            border-radius: 50%;
            background: rgba(0, 0, 0, 0.1);
            position: relative; }
        .owl-carousel .owl-dots .owl-dot:hover, .owl-carousel .owl-dots .owl-dot:focus {
            outline: none !important; }
        .owl-carousel .owl-dots .owl-dot.active {
            background: #1089ff; }
        .owl-carousel:hover .owl-nav .owl-prev,
        .owl-carousel:hover .owl-nav .owl-next {
            opacity: 1; }
        .owl-carousel:hover .owl-nav .owl-prev {
            left: -25px; }
        .owl-carousel:hover .owl-nav .owl-next {
            right: -25px; }

        .owl-carousel.owl-drag .owl-item {
            -ms-touch-action: pan-y;
            touch-action: pan-y; }

        .work {
            width: 100%; }
        .work .img {
            width: 100%;
            height: 300px;
            position: relative;
            -webkit-box-shadow: 0px 20px 35px -30px rgba(0, 0, 0, 0.26);
            -moz-box-shadow: 0px 20px 35px -30px rgba(0, 0, 0, 0.26);
            box-shadow: 0px 20px 35px -30px rgba(0, 0, 0, 0.26); }
        .work .img .icon {
            width: 70px;
            height: 70px;
            border-radius: 50%;
            background: #fff;
            display: block;
            opacity: 0;
            -webkit-transition: 0.3s;
            -o-transition: 0.3s;
            transition: 0.3s; }
        @media (prefers-reduced-motion: reduce) {
            .work .img .icon {
                -webkit-transition: none;
                -o-transition: none;
                transition: none; } }
        .work .text h3 {
            font-size: 18px;
            font-weight: 500; }
        .work .text h3 a {
            color: #000; }
        .work .text span {
            font-size: 12px;
            letter-spacing: 1px;
            color: rgba(0, 0, 0, 0.3);
            text-transform: uppercase;
            font-weight: 500; }
        .work:hover .img .icon {
            opacity: 1; }

        .swiper-container_catg_product .swiper-slide {

            opacity:0!important;
        }
        .swiper-container_catg_product .swiper-slide-active{

            opacity:1!important;
        }
        .swiper-button-next svg, .swiper-button-prev svg {
            width: 50%;
            height: 20%;

        }

        .is_mobile {
            display:none!important;

        }
        .h4 {
            text-rendering: optimizeLegibility;
            color: #e01020;
            font-size: var(--h4-font-size);
            font-weight: 700;
            line-height: 1;
            margin-bottom: var(--heading-mb);
            margin-top: var(--heading-mt);
            text-transform: uppercase;
        }
        @media screen and (max-width: 600px) {
            .is_web {
                display:none!important;

            }
            .is_mobile {
                display:block!important;

            }
        }
    </style>
@endsection
@section('content')



    <div data-spai-bg-prepared="1" data-vue-wrapper="" data-vue-initialized="true" class="acf-hero-video-player alignfull acf-video-player has-fill-stretch bg-cover bg-center" style="background-image: url(&quot;https://cdn.shortpixel.ai/spai/w_1920+q_lossy+ret_img+to_webp/mutti-parma.com/app/themes/mutti/dist/images/video/products.jpg?id=bf9c17137b0530d578b1&quot;); --vue-placeholder-height: 600px;">
        <div class="responsive-embed " style="padding-bottom: 35.3125%;">
            <div class="responsive-embed-item">
                <div class="relative h-full">
                    <div class="transition-opacity opacity-100 absolute inset-0">
                        @if($product_page_setting && $product_page_setting->first_section_top_video)
                            <video playsinline="" controls="controls" loop="loop" autoplay="autoplay" poster="https://mutti-parma.com/app/themes/mutti/dist/images/video/products.jpg?id=bf9c17137b0530d578b1" controlslist="nofullscreen nodownload noremoteplayback noplaybackrate" disablepictureinpicture="" preload="metadata" data-name="Hero: products" class="video-js w-full absolute inset-0" data-initialized-tracking="true" src="{{asset('storage/product_video/'.$product_page_setting->first_section_top_video)}}"></video>
                        @else
                            <video playsinline="" controls="controls" loop="loop" autoplay="autoplay" poster="https://mutti-parma.com/app/themes/mutti/dist/images/video/products.jpg?id=bf9c17137b0530d578b1" controlslist="nofullscreen nodownload noremoteplayback noplaybackrate" disablepictureinpicture="" preload="metadata" data-name="Hero: products" class="video-js w-full absolute inset-0" data-initialized-tracking="true" src="blob:https://mutti-parma.com/449a441c-6df7-46aa-b677-2fb639656d56"></video>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wp-block-cover alignwide has-background-dim-40 has-pitchblack-background-color has-background-dim">

        @if($product_page_setting && $product_page_setting->first_section_bg_image)
            <img decoding="async" class="wp-block-cover__image-background wp-image-300000158" alt="" src="{{asset('storage/products/'.$product_page_setting->first_section_bg_image)}}" data-spai="1" data-object-fit="cover" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="1440" data-spai-height="489.125" loading="lazy">
        @else
            <img decoding="async" class="wp-block-cover__image-background wp-image-300000158" alt="" src="https://cdn.shortpixel.ai/spai/q_lossy+w_1546+h_525+to_webp+ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/banner-product-archive.jpg" data-spai="1" data-object-fit="cover" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="1440" data-spai-height="489.125" loading="lazy">
        @endif


        <div class="wp-block-cover__inner-container is-layout-flow wp-block-cover-is-layout-flow">
            <div class="acf-breadcrumb my-4 md:my-8 text-center">
                <nav>
                    <ol class="list-none text-md">
                        <li class="inline">
                            @if($product_page_setting && $product_page_setting->first_section_top_text)
                                <span>{{$product_page_setting->first_section_top_text}}</span>
                            @else
                                <span>Nos conserves de tomates et sauces tomates</span>
                            @endif
                        </li>
                    </ol>
                </nav>
            </div>

            @if($product_page_setting && $product_page_setting->first_section_top_heading)

                <h1 class="has-text-align-center wp-block-heading">{{$product_page_setting->first_section_top_heading}}</h1>
            @else
                <h1 class="has-text-align-center wp-block-heading">Trouvez la tomate parfaite pour n’importe quelle recette</h1>

            @endif
            @if($product_page_setting && $product_page_setting->first_section_paragraph)
                <p class="has-text-align-center is-style-default">{{$product_page_setting->first_section_paragraph}}
                </p>
            @else

                <p class="has-text-align-center is-style-default">Le meilleur de la tomate depuis plus de 120 ans. <br>
                    <br> Des classiques méditerranéens à la cuisine moderne la plus innovante, nous vous proposons des tomates en conserves et sauces tomates de qualité supérieure pour réaliser votre sauce tomate maison ou toute autre recette italienne à base de tomates. Sur cette page, vous pouvez naviguer à travers la sélection des produits Mutti et en savoir plus sur la manière d’obtenir les meilleurs résultats dans votre cuisine.
                </p>
            @endif

            @if($product_page_setting && $product_page_setting->first_section_bottom_heading)
                <p class="has-text-align-center is-style-cursive">{{$product_page_setting->first_section_bottom_heading}}</p>
            @else
                <p class="has-text-align-center is-style-cursive">Commençons par…</p>

            @endif
        </div>
    </div>
    <!-- zina house section start -->

    <!-- our product -->
    <section id="ourProduct" class="py-lg-5 py-3">
        <div class="container">
            <h2 class="has-text-align-center wp-block-heading">LES ESSENTIELS MUTTI</h2>
            <p class="has-text-align-center is-style-cursive has-xxl-font-size">Polpa, Passata, Double concentré et Tomates pelées </p>

            <div id="slider_qshr6563h" data-slideshow="" class="mx-8 lg:-mx-12">
                <div class="relative is_web">
                    <div class="featured-carousel_web owl-carousel">

                        @foreach($all_products as $product)
                            <div class="item">
                                <div class="work">
                                    <div class="text-center hover:lift-img mx-auto max-w-28">
                                        <a href="{{route('product_detail',$product)}}" class="block">
                                            <div class="responsive-embed is-contain" style="padding-bottom: 188.235%;">
                                                <img width="1358" height="2000" src="{{asset('storage/products/'.$product->product_image)}}" data-spai="1" alt="Pelées – Pelati" decoding="async" data-spai-crop="false" title="Pelées – Pelati" class="inline-block mt-2" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="126" data-spai-height="237.172" loading="lazy">

                                            </div>
                                        </a>
                                        <a href="{{route('product_detail',$product)}}" class="block underline mb-2 mt-2">
                                            <h3 class="h4">{{$product->title}}</h3>
                                        </a>
                                        <a href="{{route('product_detail',$product)}}" class="block hover:no-underline text-inherit"> {{ \Illuminate\Support\Str::limit(strip_tags($product->description),40)  }} </a>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>


                <div class="relative is_mobile">
                    <div class="featured-carousel_mobile owl-carousel">


                        @foreach($all_products as $product)
                            <div class="item">
                                <div class="work">
                                    <div class="text-center hover:lift-img mx-auto max-w-28">
                                        <a href="https://mutti-parma.com/fr/produits/pelees/" class="block">
                                            <div class="responsive-embed is-contain" style="padding-bottom: 188.235%;">
                                                <img width="1358" height="2000" src="{{asset('storage/products/'.$product->product_image)}}" data-spai="1" alt="Pelées – Pelati" decoding="async" data-spai-crop="false" title="Pelées – Pelati" class="inline-block mt-2" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="126" data-spai-height="237.172" loading="lazy">

                                            </div>
                                        </a>
                                        <a href="https://mutti-parma.com/fr/produits/pelees/" class="block underline mb-2 mt-2">
                                            <h3 class="h4">{{$product->title}}</h3>
                                        </a>
                                        <a href="https://mutti-parma.com/fr/produits/pelees/" class="block hover:no-underline text-inherit"> {{ \Illuminate\Support\Str::limit(strip_tags($product->description),40)  }} </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>


                <div class="acf-product-category-listing is-style-featured alignwide">
                    <div class="flex flex-wrap justify-center">
                        <div class="w-full my-6">
                            <div class="flex flex-wrap">

                                <div class="w-full md:w-2/6 max-h-32 md:max-h-full md:min-h-96 flex hover:zoom-img">
                                    @if($product_page_setting && $product_page_setting->second_section_image)
                                        <img width="800" height="600" src="{{asset('storage/products/'.$product_page_setting->second_section_image)}}" data-spai="1" class="object-cover w-full" alt="" decoding="async" data-spai-crop="true" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="480" data-spai-height="432" loading="lazy">

                                    @else

                                        <img width="800" height="600" src="https://cdn.shortpixel.ai/spai/q_lossy+w_493+h_444+to_webp+ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/texture-banner-mutti-polpa.jpg" data-spai="1" class="object-cover w-full" alt="" decoding="async" data-spai-crop="true" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="480" data-spai-height="432" loading="lazy">

                                    @endif
                                    @if($product_page_setting && $product_page_setting->second_section_image)
                                        <noscript data-spai="1">
                                            <img width="800" height="600" src="{{asset('storage/products/'.$product_page_setting->second_section_image)}}" data-spai-egr="1" class="object-cover w-full" alt="" decoding="async" data-spai-crop="true" />
                                        </noscript>
                                    @else
                                        <noscript data-spai="1">
                                            <img width="800" height="600" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/texture-banner-mutti-polpa.jpg" data-spai-egr="1" class="object-cover w-full" alt="" decoding="async" data-spai-crop="true" />
                                        </noscript>

                                    @endif
                                </div>
                                <div class="w-full flex-1 bg-beige px-8 lg:pl-16 xl:px-16 py-4 md:py-8 flex">
                                    <div class="flex flex-wrap justify-center sm:justify-between text-center sm:text-left items-center w-full">
                                        <div class="w-full sm:w-1/2">
                                            <a class="block no-underline hover:underline mb-0" href="https://mutti-parma.com/fr/categorie/pulpes-de-tomates/">
                                                @if($product_page_setting && $product_page_setting->second_section_first_heading)
                                                    <h3 class="h2">{{$product_page_setting->second_section_first_heading}}</h3>
                                                @else
                                                    <h3 class="h2">Pulpe</h3>

                                                @endif
                                            </a>
                                            <a class="block mb-2" href="https://mutti-parma.com/fr/categorie/pulpes-de-tomates/">

                                                @if($product_page_setting && $product_page_setting->second_section_sec_heading)

                                                    <h2 class="is-style-cursive underline">{{$product_page_setting->second_section_sec_heading}}</h2>
                                                @else
                                                    <h2 class="is-style-cursive underline">Pulpe de tomates</h2>

                                                @endif
                                            </a>
                                            <p>
                                                @if($product_page_setting && $product_page_setting->second_section_paragraph)
                                                    <a class="block hover:no-underline text-black mt-4" href="https://mutti-parma.com/fr/categorie/pulpes-de-tomates/"> {{$product_page_setting->second_section_paragraph}}
                                                    </a>
                                                @else
                                                    <a class="block hover:no-underline text-black mt-4" href="https://mutti-parma.com/fr/categorie/pulpes-de-tomates/"> La pulpe de tomates Mutti est un moyen simple de donner à vos sauces tomates l’arôme de tomates estivales fraîchement cueillies. Idéales pour les aliments nécessitant une longue cuisson, elles sont aussi parfaitement adaptées à la cuisine quotidienne pour réaliser des sauces tomates rapides. Toutes nos pulpes de tomates sont notées A par le Nutri-Score (comme tous nos produits excepté notre ketchup noté D pour sa teneur en sucre). <span class="underline">En savoir plus </span>
                                                    </a>

                                                @endif
                                            </p>
                                        </div>
                                        <div data-vue-wrapper="" data-vue-initialized="true" class="w-full sm:w-1/2 md:w-2/5 max-w-64 sm:max-w-48 md:max-w-64" style="--vue-placeholder-height: 325px;">
                                            <div id="slider" data-slideshow="" class="">
                                                <div class="relative">

                                                    <div class="featured-carousel_categ_products owl-carousel">
                                                        @foreach($all_products as $product)
                                                            <div class="item">
                                                                <div class="work">
                                                                    <div class="text-center hover:lift-img mx-auto max-w-28">
                                                                        <a href="https://mutti-parma.com/fr/produits/pelees/" class="block">
                                                                            <div class="responsive-embed is-contain" style="padding-bottom: 188.235%;">
                                                                                <img width="1358" height="2000" src="{{asset('storage/products/'.$product->product_image)}}" data-spai="1" alt="Pelées – Pelati" decoding="async" data-spai-crop="false" title="Pelées – Pelati" class="inline-block mt-2" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="126" data-spai-height="237.172" loading="lazy">

                                                                            </div>
                                                                        </a>
                                                                        <a href="https://mutti-parma.com/fr/produits/pelees/" class="block underline mb-2 mt-2">
                                                                            <h3 class="h4">{{$product->title}}</h3>
                                                                        </a>
                                                                        <a href="https://mutti-parma.com/fr/produits/pelees/" class="block hover:no-underline text-inherit"> {{ \Illuminate\Support\Str::limit(strip_tags($product->description),40)  }} </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
@section('js')
    <script src="{{asset('assets/frontend/js/owl.carousel.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
    <script>

        var Swipes = new Swiper('.swiper-container_top', {
            loop: true,
            slidesPerView: 4,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
            },
        });

        var Swipes = new Swiper ('.swiper-container_catg_product', {
            // Optional parameters
            direction: 'horizontal',
            loop: false,
            slidesPerView: 1,
            spaceBetween: 25,
            breakpoints: {
                320: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                },
                600: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                },
                991: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                },
                1240: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                },
                1600: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                }
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        })
        var Swipes = new Swiper('.swiper-container_catg_products', {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
            },
        });

        (function($) {

            "use strict";

            var fullHeight = function() {

                $('.js-fullheight').css('height', $(window).height());
                $(window).resize(function(){
                    $('.js-fullheight').css('height', $(window).height());
                });

            };
            fullHeight();

            var carousel_mobile = function() {
                $('.featured-carousel_mobile').owlCarousel({
                    loop:true,
                    autoplay: true,
                    margin:10,
                    animateOut: 'fadeOut',
                    animateIn: 'fadeIn',
                    nav:true,
                    dots: true,
                    autoplayHoverPause: false,
                    items: 1,
                    navText : ["<span class='ion-ios-arrow-back'></span>","<span class='ion-ios-arrow-forward'></span>"],

                });

            };

            var carousel = function() {
                $('.featured-carousel_web').owlCarousel({
                    loop:true,
                    autoplay: true,
                    margin:10,
                    animateOut: 'fadeOut',
                    animateIn: 'fadeIn',
                    nav:true,
                    dots: true,
                    autoplayHoverPause: false,
                    items: 4,
                    navText : ["<span class='ion-ios-arrow-back'></span>","<span class='ion-ios-arrow-forward'></span>"],

                });

            };

            var carousel_categ_products = function() {
                $('.featured-carousel_categ_products').owlCarousel({
                    loop:true,
                    autoplay: true,
                    margin:10,
                    animateOut: 'fadeOut',
                    animateIn: 'fadeIn',
                    nav:true,
                    dots: true,
                    autoplayHoverPause: false,
                    items: 1,
                    navText : ["<span class='ion-ios-arrow-back'></span>","<span class='ion-ios-arrow-forward'></span>"],

                });

            };
            carousel();
            carousel_mobile();
            carousel_categ_products();
        })(jQuery);
    </script>
@endsection

