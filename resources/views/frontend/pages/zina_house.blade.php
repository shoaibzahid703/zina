@extends('frontend.layouts.app')
@section('title','Zina House')
@section('css')
    <link rel='stylesheet' id='sage/app.css-css' href="{{asset('assets/frontend/css/app.css')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='sage/app.css-css' href="{{asset('assets/frontend/css/reset.css')}}" type='text/css' media='all' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{asset('assets/frontend/css/style.min.css')}}" />
    <style>
        .zoom-explore-story {
            zoom: 1.7;
        }

        .concentrato-slider-arrows .svg-inline--fa {
            background-color: #e01020;
            width: 50px;
            height: 50px;
            border-radius: 50%;
            padding: 10px;
        }

        .concentrato-video .video-js .vjs-big-play-button {
            background-color: rgba(43, 51, 63, 0.7);
        }

        @media (min-width: 1024px) {
            .fr-mutti-professionel-carousel .swiper-pagination.swiper-pagination {
                margin-top: 1rem;
            }
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide {
            order: 0;
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide-thumb-active {
            order: 1;
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide-thumb-active+.swiper-slide {
            order: 2;
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide-thumb-active:first-child+.swiper-slide {
            order: 0;
        }

        .mutti-professionel .swiper-container-thumbs .swiper-slide:not(.swiper-slide-thumb-active)+.swiper-slide:not(.swiper-slide-thumb-active) {
            order: 2;
        }

        .mutti-professionel .swiper-pagination-bullet:not(.bg-primary) {
            background-color: #CDCAC0 !important;
            opacity: 1 !important;
        }
        .swiper-container-initialized .swiper-slide {

            display:none!important;
        }
        .swiper-container-initialized .swiper-slide-active{

            display:block!important;
        }
        .swiper-button-next svg, .swiper-button-prev svg {
            width: 50%;
            height: 20%;

        }
    </style>
@endsection
@section('content')
    <!-- zina house section start -->
    <div data-spai-bg-prepared="1"
         data-vue-wrapper=""
         data-vue-initialized="true"
         class="acf-hero-video-player alignfull acf-video-player has-fill-stretch bg-cover bg-center"
         style="background-image: url('{{asset('assets/frontend/images/zina_house.jpg')}}'); --vue-placeholder-height: 600px;">
        <div class="responsive-embed " style="padding-bottom: 35.3125%;">
            <div class="responsive-embed-item">
                <div class="relative h-full">
                    <div class="transition-opacity absolute inset-0 opacity-0">
                        <video playsinline="" controls="controls" loop="loop" autoplay="autoplay"
                               @if($zina_house_setting && $zina_house_setting->section_1_image)
                                   poster="{{asset('storage/zina_house/'.$zina_house_setting->section_1_image)}}"
                               @else
                                   poster="{{asset('assets/frontend/images/zina_house.jpg')}}"
                               @endif
                               controlslist="nofullscreen nodownload noremoteplayback noplaybackrate"
                               disablepictureinpicture="" preload="metadata" data-name="Hero: about"
                               class="video-js w-full absolute inset-0" data-initialized-tracking="true"
                               src="#">
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wp-block-cover alignwide">
        <span aria-hidden="true" class="wp-block-cover__background has-pitchblack-background-color has-background-dim"></span>
        <img decoding="async" class="wp-block-cover__image-background wp-image-300000165" alt=""
             @if($zina_house_setting && $zina_house_setting->section_2_bg_image)
                 src="{{asset('storage/zina_house/'.$zina_house_setting->section_2_bg_image)}}"
             @else
                 src="{{asset('assets/frontend/images/section_2_bg_image.jpg')}}"
             @endif
             data-spai="1" style="object-position:29% 24%"
             data-object-fit="cover"
             data-object-position="29% 24%" data-spai-loading="lazy"
             data-spai-lazy-loaded="true" data-spai-width="1440" data-spai-height="446.375" loading="lazy">
        <noscript data-spai="1">
            <img decoding="async" class="wp-block-cover__image-background wp-image-300000165"
                 @if($zina_house_setting && $zina_house_setting->section_2_bg_image)
                     src="{{asset('storage/zina_house/'.$zina_house_setting->section_2_bg_image)}}"
                 @else
                     src="{{asset('assets/frontend/images/section_2_bg_image.jpg')}}"
                 @endif
                 data-spai-egr="1" style="object-position:29% 24%" data-object-fit="cover" data-object-position="29% 24%"
                 @if($zina_house_setting && $zina_house_setting->section_2_bg_image)
                     srcset="{{asset('storage/zina_house/'.$zina_house_setting->section_2_bg_image)}}"
                 @else
                     srcset="{{asset('assets/frontend/images/section_2_bg_image.jpg')}}"
                 @endif
                 sizes="(max-width: 2400px) 100vw, 2400px" />
        </noscript>
        <div class="wp-block-cover__inner-container is-layout-flow wp-block-cover-is-layout-flow">
            <div class="acf-breadcrumb my-4 md:my-8 text-center">
                <nav>
                    <ol class="list-none text-md">
                        <li class="inline">
                            <a class="text-inherit" href="{{route('welcome')}}">
                                @if($front_navbar && $front_navbar->welcome_menu_text  )
                                    {{$front_navbar->welcome_menu_text  }}
                                @else
                                    Welcome
                                @endif
                            </a>
                            <span aria-hidden="true"> /&nbsp;</span>
                        </li>
                        <li class="inline">
                            <a class="text-inherit" href="{{route('zina_house')}}">
                                @if($front_navbar && $front_navbar->zina_house_menu_text  )
                                    {{$front_navbar->zina_house_menu_text  }}
                                @else
                                    ZINA HOUSE
                                @endif
                            </a>
                        </li>
                    </ol>
                </nav>
            </div>
            <h1 class="has-text-align-center wp-block-heading">
                @if($zina_house_setting && $zina_house_setting->section_2_heading)
                    {{$zina_house_setting->section_2_heading}}
                @else
                    ABOUT ZINA
                @endif
            </h1>
            <br>
            <p class="has-text-align-center is-style-cursive" style='font-family: "Montserrat", sans-serif;'>
                @if($zina_house_setting && $zina_house_setting->section_2_text)
                    {{$zina_house_setting->section_2_text}}
                @else
                    A long love affair with the Tunisian tomato
                @endif
            </p>
            <p class="has-text-align-center">
                @if($zina_house_setting && $zina_house_setting->section_2_paragraph)
                    {{$zina_house_setting->section_2_paragraph}}
                @else
                    As a company, our mission is to give Tunisian tomatoes the means of expression they deserve. This is the objective that has driven us for one hundred and twenty years. Even today, we learn new lessons from this wonderful fruit and continually improve every aspect of its production to make the best canned tomatoes and tomato sauces.
                @endif
            </p>
            <p class="has-text-align-center is-style-cursive">
                @if($zina_house_setting && $zina_house_setting->section_2_bottom_text)
                    {{$zina_house_setting->section_2_bottom_text}}
                @else
                    It all starts with…
                @endif
            </p>
        </div>
    </div>
    <!-- our product -->
    <section id="ourProduct" class="py-lg-5 py-3">
        <div class="container">

            <main class="max-w-container mx-auto px-4 lg:px-3 flex-grow w-full menu:mt-40 mb-12">
                <div class="max-w-content mx-auto entry-content post-4 page type-page status-publish hentry">

                    <div data-vue-wrapper="" data-vue-initialized="true" class="acf-timeline alignwide" style="--vue-placeholder-height: 16303px;">
                        <div class="flex flex-col">
                            <div>
                                <div id="slider_qopli0kov" data-slideshow="" class="">
                                    <div class="relative">
                                        <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events swiper-container-autoheight">
                                            <div class="swiper-wrapper">
                                                @if($zina_house_sliders->count() == 0)
                                                    <div class="swiper-slide" style="width: 1440px; margin-right: 60px;">
                                                        <div class="flex flex-wrap align-stretch">
                                                            <div class="flex-1 has-primary-background-color gradient-radial-primary p-8 lg:p-16 md:pl-24 md:mt-16 lg:pl-32 text-center md:text-left">
                                                                <p class="is-style-cursive text-white flex justify-between items-center">
                                                                    <button class="swiper-button-prev static md:hidden" style="background-image: none; margin-top: 0px;" data-spai-bg-on="1">
                                                                        <svg class="svg-inline--fa fa-chevron-left fa-w-10 text-2xl" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                                                                            <path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path>
                                                                        </svg>
                                                                        <!-- <i class="fas fa-chevron-left text-2xl"></i> Font Awesome fontawesome.com -->
                                                                    </button>
                                                                    <span>1850</span>
                                                                    <button class="swiper-button-next static md:hidden" style="background-image: none; margin-top: 0px;" data-spai-bg-on="1">
                                                                        <svg class="svg-inline--fa fa-chevron-right fa-w-10 text-2xl" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                                                                            <path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path>
                                                                        </svg>
                                                                        <!-- <i class="fas fa-chevron-right text-2xl"></i> Font Awesome fontawesome.com -->
                                                                    </button>
                                                                </p>
                                                                <h3 class="h2">
                                                                    @if($zina_house_setting && $zina_house_setting->section_3_heading)
                                                                        {{$zina_house_setting->section_3_heading}}
                                                                    @else
                                                                        A true pioneer
                                                                    @endif
                                                                </h3>
                                                                <div class="mt-4">
                                                                    <p>
                                                                        @if($zina_house_setting && $zina_house_setting->section_3_paragraph)
                                                                            {{$zina_house_setting->section_3_paragraph}}
                                                                        @else
                                                                            Thanks to Mr. Znaidi's initiative, innovation is taking shape through the adoption of crop rotation. This approach not only allows the soil to regain its nutrients, but also reduces dependence on chemical and natural fertilizers. Crop rotation remains a fundamental practice in agriculture, confirming that traditional methods remain essential for
                                                                            producing the best tomatoes.
                                                                        @endif
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="w-full md:w-1/2 order-first md:order-last max-h-64 md:max-h-none bg-white">
                                                                <img width="766" height="566"
                                                                     src="{{asset('assets/frontend/images/section_3_side_image.jpg')}}"
                                                                     data-spai="1" alt="historien om mutti" decoding="async"
                                                                     data-spai-crop="true" class="object-cover h-full w-full max-w-none attachment-full size-full"
                                                                     data-spai-loading="lazy" data-spai-lazy-loaded="true"
                                                                     data-spai-width="720" data-spai-height="570"
                                                                     loading="lazy">
                                                                <noscript data-spai="1">
                                                                    <img width="766" height="566"
                                                                         src="{{asset('assets/frontend/images/section_3_side_image.jpg')}}"
                                                                         data-spai-egr="1" alt="historien om mutti" decoding="async" data-spai-crop="true"
                                                                         srcset="{{asset('assets/frontend/images/section_3_side_image.jpg')}}"
                                                                         sizes="(max-width: 766px) 100vw, 766px"
                                                                         class="object-cover h-full w-full max-w-none attachment-full size-full"
                                                                         data-spai-target="src" data-spai-orig="">
                                                                </noscript>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-slide">
                                                        <div class="flex flex-wrap align-stretch">
                                                            <div class="flex-1 has-primary-background-color gradient-radial-primary p-8 lg:p-16 md:pl-24 md:mt-16 lg:pl-32 text-center md:text-left">
                                                                <p class="is-style-cursive text-white flex justify-between items-center">
                                                                    <button class="swiper-button-prev static md:hidden" style="background-image: none; margin-top: 0px;" data-spai-bg-on="1">
                                                                        <svg class="svg-inline--fa fa-chevron-left fa-w-10 text-2xl" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                                                                            <path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path>
                                                                        </svg>
                                                                        <!-- <i class="fas fa-chevron-left text-2xl"></i> Font Awesome fontawesome.com -->
                                                                    </button>
                                                                    <span>1971</span>
                                                                    <button class="swiper-button-next static md:hidden" style="background-image: none; margin-top: 0px;" data-spai-bg-on="1">
                                                                        <svg class="svg-inline--fa fa-chevron-right fa-w-10 text-2xl" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                                                                            <path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path>
                                                                        </svg>
                                                                        <!-- <i class="fas fa-chevron-right text-2xl"></i> Font Awesome fontawesome.com -->
                                                                    </button>
                                                                </p>
                                                                <h3 class="h2"> La Polpa – Conserve de tomate </h3>
                                                                <div class="mt-4">
                                                                    <p>Avec l’arrivée des années 1970, Ugo Zinase fixe l’objectif ambitieux de créer un produit frais dont la teneur en liquide serait inférieure à celle des tomates pelées. Grâce à la méthode de traitement à froid qui vient d’être inventée et après plusieurs tentatives, les légendaires tomates concassées fines de Zinasont créées. La Polpa est encore aujourd’hui le produit le plus important de Mutti. Une conserve de pulpe de tomates qui compte à travers le monde des millions d’adorateurs de son goût frais et de sa qualité.</p>
                                                                </div>
                                                            </div>
                                                            <div class="w-full md:w-1/2 order-first md:order-last max-h-64 md:max-h-none bg-white">
                                                                <img width="766" height="566" src="https://cdn.shortpixel.ai/spai/q_lossy+w_722+h_565+to_webp+ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/history-1971.jpg" data-spai="1" alt="eldgammel boks med tomat" decoding="async" data-spai-crop="true" class="object-cover h-full w-full max-w-none attachment-full size-full" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="720" data-spai-height="562.5" loading="lazy">
                                                                <noscript data-spai="1">
                                                                    <img width="766" height="566" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/history-1971.jpg" data-spai-egr="1" alt="eldgammel boks med tomat" decoding="async" data-spai-crop="true" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/history-1971.jpg 766w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/history-1971-200x148.jpg 200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/history-1971-500x369.jpg 500w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/history-1971-150x111.jpg 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/history-1971-600x443.jpg 600w" sizes="(max-width: 766px) 100vw, 766px" class="object-cover h-full w-full max-w-none attachment-full size-full" data-spai-target="src" data-spai-orig="">
                                                                </noscript>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-slide swiper-slide-active" style="width: 1440px; margin-right: 60px;">
                                                        <div class="flex flex-wrap align-stretch">
                                                            <div class="flex-1 has-primary-background-color gradient-radial-primary p-8 lg:p-16 md:pl-24 md:mt-16 lg:pl-32 text-center md:text-left">
                                                                <p class="is-style-cursive text-white flex justify-between items-center">
                                                                    <button class="swiper-button-prev static md:hidden" style="background-image: none; margin-top: 0px;" data-spai-bg-on="1">
                                                                        <svg class="svg-inline--fa fa-chevron-left fa-w-10 text-2xl" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                                                                            <path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path>
                                                                        </svg>
                                                                        <!-- <i class="fas fa-chevron-left text-2xl"></i> Font Awesome fontawesome.com -->
                                                                    </button>
                                                                    <span>2021</span>
                                                                    <button class="swiper-button-next static md:hidden" style="background-image: none; margin-top: 0px;" data-spai-bg-on="1">
                                                                        <svg class="svg-inline--fa fa-chevron-right fa-w-10 text-2xl" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                                                                            <path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path>
                                                                        </svg>
                                                                        <!-- <i class="fas fa-chevron-right text-2xl"></i> Font Awesome fontawesome.com -->
                                                                    </button>
                                                                </p>
                                                                <h3 class="h2"> Zina lance les 1ères tomates pelées Zéro résidu de pesticides sur le marché ! </h3>
                                                                <div class="mt-4">
                                                                    <p>Les tomates pelées Zina Zéro Résidu de Pesticides*, naturellement pulpeuses et fermes, sont récoltées en saison en moins de 24h. Elles sont délicatement pelées avant d’être conservées dans une purée de tomates veloutée et riche en saveurs.</p>
                                                                    <p>
                                                                        <strong>On retrouve grâce à cette innovation toute l’expertise Zina pour une garantie Zéro Résidu de pesticides* dans le respect :</strong>
                                                                    </p>
                                                                    <p>
                                                                        <strong>DE LA TOMATE ET DE SON TERROIR</strong>
                                                                        <br> Des tomates cultivées en pleins champs et récoltées pendant la saison en Italie, et transformées en moins de 24h pour préserver leur fraîcheur et goût authentique
                                                                    </p>
                                                                    <p>
                                                                        <strong>DES AGRICULTEURS</strong>
                                                                        <br> Une relation durable et un accompagnement <br> dans une démarche spécifique plus responsable <br> et respectueuse de l’environnement
                                                                    </p>
                                                                    <p>
                                                                        <strong>DU CONSOMMATEUR</strong>
                                                                        <br> Des contrôles systématiques tout au long <br> de la production, pour vous garantir le meilleur de la tomate 100% tunisienne
                                                                    </p>
                                                                    <p>Retrouvez toutes les informations sur notre production intégrée Zéro résidu de pesticides <a href="https://mutti-parma.com/fr/zero-residu-de-pesticides/">ici.&nbsp;</a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="w-full md:w-1/2 order-first md:order-last max-h-64 md:max-h-none bg-white">
                                                                <img width="345" height="489" src="https://cdn.shortpixel.ai/spai/q_lossy+w_371+h_1067+to_webp+ret_img/mutti-parma.com/app/uploads/sites/17/2021/09/product-e1690909157579.png" data-spai="1" alt="" decoding="async" data-spai-crop="true" class="object-contain h-full mx-auto" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="345" data-spai-height="991.5" loading="lazy">
                                                                <noscript data-spai="1">
                                                                    <img width="345" height="489" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2021/09/product-e1690909157579.png" data-spai-egr="1" alt="" decoding="async" data-spai-crop="true" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2021/09/product-e1690909157579.png 345w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2021/09/product-e1690909157579-141x200.png 141w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2021/09/product-e1690909157579-150x213.png 150w" sizes="(max-width: 345px) 100vw, 345px" class="object-contain h-full mx-auto" data-spai-target="src" data-spai-orig="">
                                                                </noscript>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    @foreach($zina_house_sliders as $key => $slider)
                                                        <div class="swiper-slide @if($key == 0)swiper-slide-active @endif" style="width: 1440px; margin-right: 60px;">
                                                            <div class="flex flex-wrap align-stretch">
                                                                <div class="flex-1 has-primary-background-color gradient-radial-primary p-8 lg:p-16 md:pl-24 md:mt-16 lg:pl-32 text-center md:text-left">
                                                                    <p class="is-style-cursive text-white flex justify-between items-center">
                                                                        <button class="swiper-button-prev static md:hidden" style="background-image: none; margin-top: 0px;" data-spai-bg-on="1">
                                                                            <svg class="svg-inline--fa fa-chevron-left fa-w-10 text-2xl" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                                                                                <path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path>
                                                                            </svg>
                                                                            <!-- <i class="fas fa-chevron-left text-2xl"></i> Font Awesome fontawesome.com -->
                                                                        </button>
                                                                        <span>{{$slider->slider_year}}</span>
                                                                        <button class="swiper-button-next static md:hidden" style="background-image: none; margin-top: 0px;" data-spai-bg-on="1">
                                                                            <svg class="svg-inline--fa fa-chevron-right fa-w-10 text-2xl" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                                                                                <path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path>
                                                                            </svg>
                                                                            <!-- <i class="fas fa-chevron-right text-2xl"></i> Font Awesome fontawesome.com -->
                                                                        </button>
                                                                    </p>
                                                                    <h3 class="h2">
                                                                        {{$slider->slider_title}}
                                                                    </h3>
                                                                    <div class="mt-4">
                                                                        <p>
                                                                            {{$slider->slider_description}}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="w-full md:w-1/2 order-first md:order-last max-h-64 md:max-h-none bg-white">
                                                                    <img width="766" height="566"
                                                                         src="{{asset('storage/zina_house_slider/'.$slider->slider_image)}}"
                                                                         data-spai="1" alt="historien om mutti" decoding="async"
                                                                         data-spai-crop="true" class="object-cover h-full w-full max-w-none attachment-full size-full"
                                                                         data-spai-loading="lazy" data-spai-lazy-loaded="true"
                                                                         data-spai-width="720" data-spai-height="570"
                                                                         loading="lazy">
                                                                    <noscript data-spai="1">
                                                                        <img width="766" height="566"
                                                                             src="{{asset('storage/zina_house_slider/'.$slider->slider_image)}}"
                                                                             data-spai-egr="1" alt="historien om mutti" decoding="async" data-spai-crop="true"
                                                                             srcset="{{asset('storage/zina_house_slider/'.$slider->slider_image)}}"
                                                                             sizes="(max-width: 766px) 100vw, 766px"
                                                                             class="object-cover h-full w-full max-w-none attachment-full size-full"
                                                                             data-spai-target="src" data-spai-orig="">
                                                                    </noscript>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    @endforeach
                                                @endif
                                            </div>
                                            <!---->
                                        </div>
                                        <div class="swiper-button-prev justify-start outline-none h-full top-0 flex items-center z-25" style="background-image: none; margin-top: 0px;" data-spai-bg-on="1">
                                            <svg class="svg-inline--fa fa-chevron-left fa-w-10 text-white text-2xl md:text-4xl ml-10 mt-2 md:mt-16 hidden md:block" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                                                <path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path>
                                            </svg>
                                            <!-- <i class="fas fa-chevron-left text-white text-2xl md:text-4xl ml-10 mt-2 md:mt-16 hidden md:block"></i> Font Awesome fontawesome.com -->
                                        </div>
                                        <div class="swiper-button-next justify-end outline-none h-full top-0 flex items-center z-25" style="background-image: none; margin-top: 0px;" data-spai-bg-on="1">
                                            <svg class="svg-inline--fa fa-chevron-right fa-w-10 text-white md:text-primary text-2xl md:text-4xl mr-10 mt-2 md:mt-16 hidden md:block" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                                                <path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path>
                                            </svg>
                                            <!-- <i class="fas fa-chevron-right text-white md:text-primary text-2xl md:text-4xl mr-10 mt-2 md:mt-16 hidden md:block"></i> Font Awesome fontawesome.com -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bg-beige flex flex-wrap md:mx-16 relative order-first md:order-last">
                                <!---->
                                <ul class="flex list-none overflow-x-auto mb-0 cursor-grab active:cursor-grabbing md:flex-1 md:mx-auto">
                                    @foreach($zina_house_sliders_years as $year)
                                        <li class="h4 p-6 m-0 whitespace-no-wrap flex items-center text-center">
                                            <span class="cursor-pointer">{{$year}} </span>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="bg-beige absolute opacity-75 inset-y-0 right-0 w-12 md:w-24 mt-6 mb-6"></div>
                            </div>
                        </div>
                    </div>
                    <div class="wp-block-media-text alignwide is-stacked-on-mobile">
                        <figure class="wp-block-media-text__media">
                            <img decoding="async"
                                 @if($zina_house_setting && $zina_house_setting->section_4_side_image)
                                     src="{{asset('storage/zina_house/'.$zina_house_setting->section_4_side_image)}}"
                                 @else
                                     src="{{asset('assets/frontend/images/section-3-side.jpg')}}"
                                 @endif
                                 data-spai="1" alt="" class="wp-image-30000048 size-full"
                                 sizes="(max-width: 600px) 100vw, (min-width: 1440px) 792px,55vw" data-spai-crop="false" srcset=" "
                                 data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="792" data-spai-height="528.25" loading="lazy">
                            <noscript data-spai="1">
                                <img decoding="async"
                                     @if($zina_house_setting && $zina_house_setting->section_4_side_image)
                                         src="{{asset('storage/zina_house/'.$zina_house_setting->section_4_side_image)}}"
                                     @else
                                         src="{{asset('assets/frontend/images/section-3-side.jpg')}}"
                                     @endif
                                     data-spai-egr="1" alt="" class="wp-image-30000048 size-full"
                                     sizes="(max-width: 600px) 100vw, (min-width: 1440px) 792px,55vw" data-spai-crop="false"
                                     @if($zina_house_setting && $zina_house_setting->section_4_side_image)
                                         srcset="{{asset('storage/zina_house/'.$zina_house_setting->section_4_side_image)}}"
                                     @else
                                         srcset="{{asset('assets/frontend/images/section-3-side.jpg')}}"
                                    @endif
                                />
                            </noscript>
                        </figure>
                        <div class="wp-block-media-text__content">
                            <h2 class="wp-block-heading">

                                @if($zina_house_setting && $zina_house_setting->section_4_top_heading)
                                    {{$zina_house_setting->section_4_top_heading}}
                                @else
                                    Passion like the first day
                                @endif
                            </h2>
                            <p class="is-style-cursive has-xxl-font-size">

                                @if($zina_house_setting && $zina_house_setting->section_4_text)
                                    {{$zina_house_setting->section_4_text}}
                                @else
                                    Our society today
                                @endif
                            </p>
                            <p>
                                @if($zina_house_setting && $zina_house_setting->section_4_paragraph)
                                    {{$zina_house_setting->section_4_paragraph}}
                                @else
                                    We are keen to create the best canned tomatoes for our customers. Our headquarters is in Parma, Italy, where our company was founded 120 years ago. In the future, we will continue to share with millions of new fans our love for the flavors of Tunisian tomatoes, for innovation and sustainability
                                @endif
                            </p>
                            <p class="is-style-cursive has-xxl-font-size">
                                @if($zina_house_setting && $zina_house_setting->section_4_bottom_heading)
                                    {{$zina_house_setting->section_4_bottom_heading}}
                                @else
                                    For people looking for…
                                @endif
                            </p>
                            <h3 class="wp-block-heading">
                                @if($zina_house_setting && $zina_house_setting->section_4_last_text)
                                    {{$zina_house_setting->section_4_last_text}}
                                @else
                                    THE BEST OF 100% TUNISIAN TOMATOES
                                @endif
                            </h3>
                        </div>
                    </div>
                    <div class="wp-block-group alignwide is-style-gradient has-secondary-background-color has-background is-layout-flow wp-block-group-is-layout-flow">
                        <h2 class="has-text-align-center wp-block-heading">

                            @if($zina_house_setting && $zina_house_setting->section_5_heading)
                                {{$zina_house_setting->section_5_heading}}
                            @else
                                METHOD AND KNOW-HOW
                            @endif
                        </h2>
                        <p class="has-text-align-center">

                            @if($zina_house_setting && $zina_house_setting->section_5_paragraph)
                                {{$zina_house_setting->section_5_paragraph}}
                            @else
                                Zina's passion for Tunisian tomatoes is based on three watchwords: quality, transparency and reliability.
                                Working well requires dedication, consistency and intelligence, but also great passion and honesty
                            @endif
                        </p>
                        <p class="has-text-align-center is-style-cursive">
                            @if($zina_house_setting && $zina_house_setting->section_5_ceo_name)
                                {{$zina_house_setting->section_5_ceo_name}}
                            @else
                                Francesco Mutti
                            @endif
                        </p>
                        <p class="has-text-align-center has-primary-color has-text-color">

                            @if($zina_house_setting && $zina_house_setting->section_5_bottom_text)
                                {{$zina_house_setting->section_5_bottom_text}}
                            @else
                                CEO of Zina SpA
                            @endif
                        </p>
                        <div class="acf-image-grid alignwide">
                            <div class="flex -mx-8 -my-4 md:-mt-8 flex-wrap">
                                @if($products->count() == 0)
                                    <div class=" w-1/1 sm:w-1/2 md:w-1/3 px-8 py-4 md:py-8">
                                        <a href="https://mutti-parma.com/fr/la-maison-mutti/methode-et-savoir-faire/#values" target="" class="hover:zoom-img">
                                            <div class="responsive-embed " style="padding-bottom: 75%;">
                                                <img width="800" height="600" src="https://cdn.shortpixel.ai/spai/q_lossy+w_408+h_306+to_webp+ret_img/mutti-parma.com/app/uploads/sites/17/2020/05/valeurs-de-lentreprise-mutti-800x600.jpg" data-spai="1" class="attachment-4:3 size-4:3" alt="Valeurs de l'entreprise Mutti" decoding="async" data-spai-crop="true" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="384" data-spai-height="288" loading="lazy">
                                                <noscript data-spai="1">
                                                    <img width="800" height="600" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/05/valeurs-de-lentreprise-mutti-800x600.jpg" data-spai-egr="1" class="attachment-4:3 size-4:3" alt="Valeurs de l&#039;entreprise Mutti" decoding="async" data-spai-crop="true" />
                                                </noscript>
                                            </div>
                                            <h5 class="text-center underline"> Valeurs de l'entreprise </h5>
                                        </a>
                                    </div>
                                    <div class=" w-1/1 sm:w-1/2 md:w-1/3 px-8 py-4 md:py-8">
                                        <a href="https://mutti-parma.com/fr/la-maison-mutti/methode-et-savoir-faire/#environmental-certifications" target="" class="hover:zoom-img">
                                            <div class="responsive-embed " style="padding-bottom: 75%;">
                                                <img width="800" height="600" src="https://cdn.shortpixel.ai/spai/q_lossy+w_408+h_306+to_webp+ret_img/mutti-parma.com/app/uploads/sites/17/2020/05/environnement-mutti-800x600.jpg" data-spai="1" class="attachment-4:3 size-4:3" alt="Environnement Mutti" decoding="async" data-spai-crop="true" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="384" data-spai-height="288" loading="lazy">
                                                <noscript data-spai="1">
                                                    <img width="800" height="600" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/05/environnement-mutti-800x600.jpg" data-spai-egr="1" class="attachment-4:3 size-4:3" alt="Environnement Mutti" decoding="async" data-spai-crop="true" />
                                                </noscript>
                                            </div>
                                            <h5 class="text-center underline"> Environnement </h5>
                                        </a>
                                    </div>
                                    <div class=" w-1/1 sm:w-1/2 md:w-1/3 px-8 py-4 md:py-8">
                                        <a href="https://mutti-parma.com/fr/la-maison-mutti/methode-et-savoir-faire/#ethics-and-legality" target="" class="hover:zoom-img">
                                            <div class="responsive-embed " style="padding-bottom: 75%;">
                                                <img width="800" height="600" src="https://cdn.shortpixel.ai/spai/q_lossy+w_408+h_306+to_webp+ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/section-about-ethics-legality-800x600.jpg" data-spai="1" class="attachment-4:3 size-4:3 attachment-4:3 size-4:3" alt="Zina tshirt" decoding="async" data-spai-crop="true" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="384" data-spai-height="288" loading="lazy">
                                                <noscript data-spai="1">
                                                    <img width="800" height="600" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-about-ethics-legality-800x600.jpg" data-spai-egr="1" class="attachment-4:3 size-4:3 attachment-4:3 size-4:3" alt="Zina tshirt" decoding="async" data-spai-crop="true" />
                                                </noscript>
                                            </div>
                                            <h5 class="text-center underline"> Ethique et légalité </h5>
                                        </a>
                                    </div>
                                    <div class=" w-1/1 sm:w-1/2 md:w-1/3 px-8 py-4 md:py-8">
                                        <a href="https://mutti-parma.com/fr/la-maison-mutti/methode-et-savoir-faire/#responsibility" target="" class="hover:zoom-img">
                                            <div class="responsive-embed " style="padding-bottom: 75%;">
                                                <img width="600" height="500" src="https://cdn.shortpixel.ai/spai/q_lossy+w_408+h_306+to_webp+ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/section-process-planting.jpg" data-spai="1" class="attachment-4:3 size-4:3 attachment-4:3 size-4:3" alt="process planting" decoding="async" data-spai-crop="true" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="384" data-spai-height="288" loading="lazy">
                                                <noscript data-spai="1">
                                                    <img width="600" height="500" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-process-planting.jpg" data-spai-egr="1" class="attachment-4:3 size-4:3 attachment-4:3 size-4:3" alt="process planting" decoding="async" data-spai-crop="true" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-process-planting.jpg 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-process-planting-200x167.jpg 200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-process-planting-500x417.jpg 500w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-process-planting-150x125.jpg 150w" sizes="(max-width: 600px) 100vw, 600px" />
                                                </noscript>
                                            </div>
                                            <h5 class="text-center underline"> Code d'éthique </h5>
                                        </a>
                                    </div>
                                    <div class=" w-1/1 sm:w-1/2 md:w-1/3 px-8 py-4 md:py-8">
                                        <a href="https://mutti-parma.com/fr/la-maison-mutti/methode-et-savoir-faire/#organization-and-control" target="" class="hover:zoom-img">
                                            <div class="responsive-embed " style="padding-bottom: 75%;">
                                                <img width="800" height="600" src="https://cdn.shortpixel.ai/spai/q_lossy+w_408+h_306+to_webp+ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/section-golden-tomato-winners-growing-cycle-800x600.jpg" data-spai="1" class="attachment-4:3 size-4:3 attachment-4:3 size-4:3" alt="tomatoes" decoding="async" data-spai-crop="true" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="384" data-spai-height="288" loading="lazy">
                                                <noscript data-spai="1">
                                                    <img width="800" height="600" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-golden-tomato-winners-growing-cycle-800x600.jpg" data-spai-egr="1" class="attachment-4:3 size-4:3 attachment-4:3 size-4:3" alt="tomatoes" decoding="async" data-spai-crop="true" />
                                                </noscript>
                                            </div>
                                            <h5 class="text-center underline"> Code de conduite </h5>
                                        </a>
                                    </div>
                                    <div class=" w-1/1 sm:w-1/2 md:w-1/3 px-8 py-4 md:py-8">
                                        <a href="https://mutti-parma.com/fr/la-maison-mutti/methode-et-savoir-faire/#responsibility" target="" class="hover:zoom-img">
                                            <div class="responsive-embed " style="padding-bottom: 75%;">
                                                <img width="800" height="600" src="https://cdn.shortpixel.ai/spai/q_lossy+w_408+h_306+to_webp+ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/section-about-us-whistleblowing-1024x768.jpg" data-spai="1" class="attachment-4:3 size-4:3 attachment-4:3 size-4:3" alt="campo coltivato di pomodori" decoding="async" data-spai-crop="true" srcset=" " sizes="(max-width: 800px) 100vw, 800px" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="384" data-spai-height="288" loading="lazy">
                                                <noscript data-spai="1">
                                                    <img width="800" height="600" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-about-us-whistleblowing-800x600.jpg" data-spai-egr="1" class="attachment-4:3 size-4:3 attachment-4:3 size-4:3" alt="campo coltivato di pomodori" decoding="async" data-spai-crop="true" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-about-us-whistleblowing-800x600.jpg 800w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-about-us-whistleblowing-200x150.jpg 200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-about-us-whistleblowing-500x375.jpg 500w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-about-us-whistleblowing-768x576.jpg 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-about-us-whistleblowing-1024x768.jpg 1024w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-about-us-whistleblowing-150x113.jpg 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-about-us-whistleblowing-600x450.jpg 600w" sizes="(max-width: 800px) 100vw, 800px" />
                                                </noscript>
                                            </div>
                                            <h5 class="text-center underline"> Responsabilité </h5>
                                        </a>
                                    </div>
                                @else
                                    @foreach($products as $product)
                                        <div class=" w-1/1 sm:w-1/2 md:w-1/3 px-8 py-4 md:py-8">
                                            <a href="#"
                                               target="" class="hover:zoom-img">
                                                <div class="responsive-embed " style="padding-bottom: 75%;">
                                                    <img width="800" height="600"
                                                         @if($product->product_image)
                                                             src="{{asset('storage/products/'.$product->product_image)}}"
                                                         @else
                                                             src="{{asset('assets/frontend/images/product-image-dummy.pngg')}}"
                                                         @endif
                                                         data-spai="1"
                                                         class="attachment-4:3 size-4:3" alt="Valeurs de l'entreprise Mutti"
                                                         decoding="async" data-spai-crop="true" data-spai-loading="lazy" data-spai-lazy-loaded="true"
                                                         data-spai-width="384" data-spai-height="288" loading="lazy">
                                                    <noscript data-spai="1">
                                                        <img width="800" height="600"
                                                             @if($product->product_image)
                                                                 src="{{asset('storage/products/'.$product->product_image)}}"
                                                             @else
                                                                 src="{{asset('assets/frontend/images/product-image-dummy.pngg')}}"
                                                             @endif
                                                             data-spai-egr="1" class="attachment-4:3 size-4:3" alt="Valeurs de l&#039;entreprise Mutti" decoding="async"
                                                             data-spai-crop="true" />
                                                    </noscript>
                                                </div>
                                                <h5 class="text-center underline"> {{$product->title}} </h5>
                                            </a>
                                        </div>
                                    @endforeach
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="wp-block-group alignwide is-style-default">
                        <div class="wp-block-group__inner-container is-layout-flow wp-block-group-is-layout-flow">
                            <div class="wp-block-columns alignwide is-layout-flex wp-container-9 wp-block-columns-is-layout-flex">
                                <div class="wp-block-column is-layout-flow wp-block-column-is-layout-flow">
                                    <h2 class="wp-block-heading" id="sustainability">
                                        @if($zina_house_setting && $zina_house_setting->section_6_text)
                                            {{$zina_house_setting->section_6_text}}
                                        @else
                                            SUSTAINABILITY
                                        @endif
                                    </h2>
                                    <p class="is-style-cursive">
                                        @if($zina_house_setting && $zina_house_setting->section_6_heading)
                                            {{$zina_house_setting->section_6_heading}}
                                        @else
                                            For us, sustainability is synonymous with respect for the earth
                                        @endif

                                    </p>
                                    <h4 class="wp-block-heading">
                                        @if($zina_house_setting && $zina_house_setting->section_6_bottom_text)
                                            {{$zina_house_setting->section_6_bottom_text}}
                                        @else
                                            COOPERATION WITH WWF
                                        @endif
                                    </h4>
                                    <p>
                                        @if($zina_house_setting && $zina_house_setting->section_6_paragraph)
                                            {{$zina_house_setting->section_6_paragraph}}
                                        @else
                                            Zina has worked closely with WWF Italy to help farmers find sustainable ways to reduce their water consumption and CO2 emissions. WWF Italy not only set targets, but it also allowed farmers to analyze and review the data obtained in order to identify actions they could take. For example, the organization recommended using special sensors that measure soil moisture and provide useful information to rationalize water consumption.
                                        @endif
                                    </p>
                                    <figure class="wp-block-image size-large is-resized">
                                        <img decoding="async" src="https://cdn.shortpixel.ai/spai/q_lossy+w_130+h_173+to_webp+ret_img/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf.png" data-spai="1" alt="" class="wp-image-16372" width="129" height="173" srcset=" " sizes="(max-width: 129px) 100vw, 129px" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="129" data-spai-height="172" loading="lazy">
                                        <noscript data-spai="1">
                                            <img decoding="async" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf-768x1024.png" data-spai-egr="1" alt="" class="wp-image-16372" width="129" height="173" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf-768x1024.png 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf-375x500.png 375w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf-150x200.png 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf-600x800.png 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf-1152x1536.png 1152w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2022/05/logo-wwf.png 1251w" sizes="(max-width: 129px) 100vw, 129px" />
                                        </noscript>
                                    </figure>
                                </div>
                                <div class="wp-block-column is-layout-flow wp-block-column-is-layout-flow">
                                    <div class="wp-block-group has-green-background-color has-background">
                                        <div class="wp-block-group__inner-container is-layout-flow wp-block-group-is-layout-flow">
                                            <h2 class="has-text-align-center wp-block-heading">

                                                @if($zina_house_setting && $zina_house_setting->section_6_results_text)
                                                    {{$zina_house_setting->section_6_results_text}}
                                                @else
                                                    RESULTS
                                                @endif
                                            </h2>
                                            <div class="wp-block-columns is-layout-flex wp-container-6 wp-block-columns-is-layout-flex">
                                                <div class="wp-block-column is-layout-flow wp-block-column-is-layout-flow">
                                                    <div class="wp-block-image">
                                                        <figure class="aligncenter size-large is-resized">
                                                            <img decoding="async" src="https://cdn.shortpixel.ai/spai/q_lossy+to_webp+ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/icon-sustainable-water.svg" data-spai="1" alt="" class="wp-image-30000085" width="53" height="91" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="53" data-spai-height="91.2344" loading="lazy">
                                                            <noscript data-spai="1">
                                                                <img decoding="async" src="https://cdn.shortpixel.ai/spai/ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/icon-sustainable-water.svg" data-spai-egr="1" alt="" class="wp-image-30000085" width="53" height="91" />
                                                            </noscript>
                                                        </figure>
                                                    </div>
                                                    <h4 class="has-text-align-center wp-block-heading">
                                                        @if($zina_house_setting && $zina_house_setting->section_6_drop_heading)
                                                            {{$zina_house_setting->section_6_drop_heading}}
                                                        @else
                                                            1,000 MILLION LITERS OF WATER SAVED
                                                        @endif
                                                    </h4>
                                                    <p class="has-text-align-center has-sm-font-size">
                                                        @if($zina_house_setting && $zina_house_setting->section_6_drop_paragraph)
                                                            {{$zina_house_setting->section_6_drop_paragraph}}
                                                        @else
                                                            between 2012 and 2014 (Source: data collected by WWF and the Euro-Mediterranean Center on Climate Change in Italy)
                                                        @endif
                                                    </p>

                                                </div>
                                                <div class="wp-block-column is-layout-flow wp-block-column-is-layout-flow">
                                                    <div class="wp-block-image">
                                                        <figure class="aligncenter size-large is-resized">
                                                            <img decoding="async"
                                                                 src="https://cdn.shortpixel.ai/spai/q_lossy+to_webp+ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/icon-sustainable-co2.svg"
                                                                 data-spai="1" alt="" class="wp-image-30000086"
                                                                 width="114" height="88" data-spai-loading="lazy"
                                                                 data-spai-lazy-loaded="true" data-spai-width="114" data-spai-height="88.1875" loading="lazy">
                                                            <noscript data-spai="1">
                                                                <img decoding="async"
                                                                     src="https://cdn.shortpixel.ai/spai/ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/icon-sustainable-co2.svg"
                                                                     data-spai-egr="1" alt="" class="wp-image-30000086" width="114" height="88" />
                                                            </noscript>
                                                        </figure>
                                                    </div>
                                                    <h4 class="has-text-align-center wp-block-heading">
                                                        @if($zina_house_setting && $zina_house_setting->section_6_co2_heading)
                                                            {{$zina_house_setting->section_6_co2_heading}}
                                                        @else
                                                            31,530 TONNES OF CO2 (CARBON DIOXIDE) AVOIDED.
                                                        @endif
                                                    </h4>
                                                    <p class="has-text-align-center has-sm-font-size">
                                                        @if($zina_house_setting && $zina_house_setting->section_6_co2_paragraph)
                                                            {{$zina_house_setting->section_6_co2_paragraph}}
                                                        @else
                                                            between 2010 and 2015 compared to 2009 levels (Source: data collected by WWF and energy efficiency group Officinae Verd
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wp-block-media-text alignwide is-stacked-on-mobile">
                        <figure class="wp-block-media-text__media">
                            <img decoding="async"
                                 @if($zina_house_setting && $zina_house_setting->section_7_side_image)
                                     src="{{asset('storage/zina_house/'.$zina_house_setting->section_7_side_image)}}"
                                 @else
                                     src="{{asset('assets/frontend/images/section_7.png')}}"
                                 @endif
                                 data-spai="1" alt="" class="wp-image-300000162 size-full"
                                 sizes="(max-width: 600px) 100vw, (min-width: 1440px) 792px,55vw"
                                 data-spai-crop="false" srcset=" "
                                 data-spai-loading="lazy" data-spai-lazy-loaded="true"
                                 data-spai-width="792" data-spai-height="528.25" loading="lazy">
                            <noscript data-spai="1">
                                <img decoding="async"
                                     @if($zina_house_setting && $zina_house_setting->section_7_side_image)
                                         src="{{asset('storage/zina_house/'.$zina_house_setting->section_7_side_image)}}"
                                     @else
                                         src="{{asset('assets/frontend/images/section_7.png')}}"
                                     @endif
                                     data-spai-egr="1" alt=""
                                     class="wp-image-300000162 size-full" sizes="(max-width: 600px) 100vw, (min-width: 1440px) 792px,55vw"
                                     data-spai-crop="false"
                                     @if($zina_house_setting && $zina_house_setting->section_7_side_image)
                                         srcset="{{asset('storage/zina_house/'.$zina_house_setting->section_7_side_image)}}"
                                     @else
                                         srcset="{{asset('assets/frontend/images/section_7.png')}}"
                                    @endif
                                />
                            </noscript>
                        </figure>
                        <div class="wp-block-media-text__content">
                            <h2 class="wp-block-heading">
                                @if($zina_house_setting && $zina_house_setting->section_7_top_text)
                                    {{$zina_house_setting->section_7_top_text}}
                                @else
                                    THE GOLDEN TOMATO: “POMODORINO D’ORO”
                                @endif
                            </h2>
                            <br>
                            <p class="is-style-cursive has-xxl-font-size">

                                @if($zina_house_setting && $zina_house_setting->section_7_heading)
                                    {{$zina_house_setting->section_7_heading}}
                                @else
                                    The quest for perfection
                                @endif
                            </p>
                            <p>
                                @if($zina_house_setting && $zina_house_setting->section_7_paragraph)
                                    {{$zina_house_setting->section_7_paragraph}}
                                @else
                                    We do everything to offer you the best quality products. This is why, every year, Zina gives a quality prize to farmers with the Pomodorino d´oro (Golden Tomato).

                                @endif
                            </p>
                            <div class="wp-block-button">
                                <a class="wp-block-button__link" href="#">
                                    @if($zina_house_setting && $zina_house_setting->section_7_learn_more)
                                        {{$zina_house_setting->section_7_learn_more}}
                                    @else
                                        Learn More
                                    @endif
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="wp-block-media-text alignwide has-media-on-the-right is-stacked-on-mobile">
                        <figure class="wp-block-media-text__media">
                            <img decoding="async"
                                 @if($zina_house_setting && $zina_house_setting->section_8_image)
                                     src="{{asset('storage/zina_house/'.$zina_house_setting->section_8_image)}}"
                                 @else
                                     src="{{asset('assets/frontend/images/section_8.png')}}"
                                 @endif
                                 data-spai="1" alt="" class="wp-image-30000010 size-full" data-spai-crop="false" data-spai-loading="lazy"
                                 data-spai-lazy-loaded="true" data-spai-width="792" data-spai-height="792" loading="lazy">
                            <noscript data-spai="1">
                                <img decoding="async"
                                     @if($zina_house_setting && $zina_house_setting->section_8_image)
                                         src="{{asset('storage/zina_house/'.$zina_house_setting->section_8_image)}}"
                                     @else
                                         src="{{asset('assets/frontend/images/section_8.png')}}"
                                     @endif

                                     data-spai-egr="1" alt="" class="wp-image-30000010 size-full"
                                     sizes="(max-width: 600px) 100vw, (min-width: 1440px) 792px,55vw" data-spai-crop="false"
                                     @if($zina_house_setting && $zina_house_setting->section_8_image)
                                         srcset="{{asset('storage/zina_house/'.$zina_house_setting->section_8_image)}}"
                                     @else
                                         srcset="{{asset('assets/frontend/images/section_8.png')}}" />
                                @endif

                            </noscript>
                        </figure>
                        <div class="wp-block-media-text__content">
                            <h2 class="wp-block-heading">

                                @if($zina_house_setting && $zina_house_setting->section_8_top_text)
                                    {{$zina_house_setting->section_8_top_text}}
                                @else
                                    WHERE DO THE BEST TOMATOES COME FROM?
                                @endif
                            </h2>
                            <p>
                                @if($zina_house_setting && $zina_house_setting->section_8_paragraph)
                                    {{$zina_house_setting->section_8_paragraph}}
                                @else
                                    The story of our Tunisian tomatoes begins in Italian fields. Each soil type and tomato variety has its own unique characteristics. Every step of the process, from field to table, affects taste.
                                @endif
                            </p>
                            <div class="wp-block-button">
                                <a class="wp-block-button__link" href="#">
                                    @if($zina_house_setting && $zina_house_setting->section_8_heading)
                                        {{$zina_house_setting->section_8_heading}}
                                    @else
                                        LEARN MORE ABOUT OUR TUNISIAN TOMATOES
                                    @endif
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </main>


        </div>
    </section>
    <!-- our values section start -->
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
    <script>

        var Swipes = new Swiper('.swiper-container_top', {
            loop: true,
            slidesPerView: 4,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
            },
        });

        var Swipes = new Swiper ('.swiper-container_catg_product', {
            // Optional parameters
            direction: 'horizontal',
            loop: false,
            slidesPerView: 1,
            spaceBetween: 25,
            breakpoints: {
                320: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                },
                600: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                },
                991: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                },
                1240: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                },
                1600: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 25
                }
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        })
        var Swipes = new Swiper('.swiper-container-initialized ', {
            loop: true,
            slidesPerView: 1,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
            },
        });
    </script>
@endsection


