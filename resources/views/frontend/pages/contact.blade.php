@extends('frontend.layouts.app')
@section('title','Contact')
@section('content')

    <section id="ourProduct" class="py-lg-5 py-3">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    @if($message = Session::get("success"))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                             {{$message}}
                        </div>
                    @endif
                    <form method="post" action="{{route('contact_save')}}">
                        @csrf
                        <div class="card">
                            <div class="card-body">
                                <h2 class="text-center">Contact</h2>
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input class="form-control @error('name') is-invalid @enderror" id="name" placeholder="name" name="name">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input class="form-control @error('email') is-invalid @enderror" id="email" placeholder="email" name="email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="phone_no">Phone Number</label>
                                    <input class="form-control @error('phone_no') is-invalid @enderror" id="phone_no" placeholder="phone number" name="phone_no">
                                    @error('phone_no')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="message">Message</label>
                                    <textarea class="form-control  @error('message') is-invalid @enderror" id="message" placeholder="message" name="message"></textarea>
                                    @error('message')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group text-center mt-3">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </section>
@endsection
@section('js')

    <script src="{{asset('assets/frontend/js/owl.carousel.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
@endsection
