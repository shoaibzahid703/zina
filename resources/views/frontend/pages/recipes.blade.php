@extends('frontend.layouts.app')
@section('title','Recipes')
@section('css')
    <link rel='stylesheet' id='sage/app.css-css' href="{{asset('assets/frontend/css/app.css')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='sage/app.css-css' href="{{asset('assets/frontend/css/reset.css')}}" type='text/css' media='all' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{asset('assets/frontend/css/style.min.css')}}" />
@endsection

@section('content')

    <!-- our product -->
    <section id="ourProduct" class="py-lg-5 py-3">
        <div class="container">

            <div class="max-w-content mx-auto">
                @if($recipe_setting->count() == 0)
                    <div data-spai-bg-prepared="1" data-vue-wrapper="" data-vue-initialized="true" class="acf-hero-video-player alignfull acf-video-player has-fill-stretch bg-cover bg-center" style="background-image: url(&quot;https://cdn.shortpixel.ai/spai/w_1920+q_lossy+ret_img+to_webp/mutti-parma.com/app/themes/mutti/dist/images/video/recipes.jpg?id=b1fd165348a41b5e12b2&quot;); --vue-placeholder-height: 600px;">
                        <div class="responsive-embed " style="padding-bottom: 35.3125%;">
                            <div class="responsive-embed-item">
                                <div class="relative h-full">
                                    <div class="transition-opacity opacity-100 absolute inset-0"><video playsinline="" controls="controls" loop="loop" autoplay="autoplay" poster="https://mutti-parma.com/app/themes/mutti/dist/images/video/recipes.jpg?id=b1fd165348a41b5e12b2" controlslist="nofullscreen nodownload noremoteplayback noplaybackrate" disablepictureinpicture="" preload="metadata" data-name="Hero: recipes" class="video-js w-full absolute inset-0" data-initialized-tracking="true" src="blob:https://mutti-parma.com/66972ea7-3d8b-4b66-b4b6-890b7ab4b3d7"></video></div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="acf-breadcrumb my-4 md:my-8 text-center md:text-left">
                        <nav>
                            <ol class="list-none text-md">
                                <li class="inline">
                                    <span>Nos recettes avec des tomates 100% tunisienne</span>
                                </li>
                            </ol>
                        </nav>
                    </div>

                    <div class="wp-block-media-text alignwide is-stacked-on-mobile" style="grid-template-columns:44% auto">
                        <figure class="wp-block-media-text__media"><img decoding="async" src="https://cdn.shortpixel.ai/spai/q_lossy+w_656+to_webp+ret_img/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home.jpg" data-spai="1" alt="" class="wp-image-300000157 size-full" sizes="(max-width: 600px) 100vw, (min-width: 1440px) 634px,44vw" data-spai-crop="false" srcset=" " data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="633.594" data-spai-height="571.031" loading="lazy"><noscript data-spai="1"><img decoding="async" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home-1024x923.jpg" data-spai-egr="1" alt="" class="wp-image-300000157 size-full" sizes="(max-width: 600px) 100vw, (min-width: 1440px) 634px,44vw" data-spai-crop="false" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home-1024x923.jpg 1024w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home-200x180.jpg 200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home-500x451.jpg 500w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home-768x692.jpg 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home-150x135.jpg 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home-600x541.jpg 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home.jpg 1580w" /></noscript></figure>
                        <div class="wp-block-media-text__content">
                            <h1 class="wp-block-heading has-text-align-left is-style-default">Nos recettes avec des tomates 100% tunisienne </h1>

                            <p class="has-text-align-center is-style-cursive has-xxl-font-size">Découvrez nos recettes gourmandes </p>

                            <p class="is-style-default">C’est l’heure de cuisiner avec Mutti ! <br>Préparons des recettes avec des tomates absolument délicieuses en revisitant les classiques de la gastronomie italienne ! Avec Mutti, il y en a pour tous les goûts et toutes les envies. Retrouvez nos recettes à base de pulpe de tomates, de purée de tomates, tomates pelées, concentré de tomates, sauce pizza et bien d’autres&nbsp;conserves de tomates et sauces tomates Mutti pour reproduire les grands classiques de la cuisine italienne ! <br> <br>Que faire avec des tomates en conserve ou une sauce tomate&nbsp;? <em> </em> <br>Vous êtes à la recherche d’une idée de recette avec des tomates&nbsp;? Mutti propose une large gamme de produits, riche en variétés de tomates de qualité supérieure : la Ronde de Parme, l’Oblongue, les tomates cerises, la Datterini ou encore la San Marzano. Consultez sans attendre ci-dessous notre sélection de recettes aux accents italiens et découvrez votre nouvelle recette préférée. </p>

                            <p class="has-text-align-center is-style-cursive">Commençons à cuisiner…</p>
                        </div>
                    </div>

                @else
                    @foreach($recipe_setting as $key => $recipe_settings)

                        <div data-spai-bg-prepared="1" data-vue-wrapper="" data-vue-initialized="true" class="acf-hero-video-player alignfull acf-video-player has-fill-stretch bg-cover bg-center" style="background-image: url(&quot;https://cdn.shortpixel.ai/spai/w_1920+q_lossy+ret_img+to_webp/mutti-parma.com/app/themes/mutti/dist/images/video/recipes.jpg?id=b1fd165348a41b5e12b2&quot;); --vue-placeholder-height: 600px;">
                            <div class="responsive-embed " style="padding-bottom: 35.3125%;">
                                <div class="responsive-embed-item">
                                    <div class="relative h-full">
                                        <div class="transition-opacity opacity-100 absolute inset-0"><video playsinline="" controls="controls" loop="loop" autoplay="autoplay" poster="https://mutti-parma.com/app/themes/mutti/dist/images/video/recipes.jpg?id=b1fd165348a41b5e12b2" controlslist="nofullscreen nodownload noremoteplayback noplaybackrate" disablepictureinpicture="" preload="metadata" data-name="Hero: recipes" class="video-js w-full absolute inset-0" data-initialized-tracking="true" src="{{asset('storage/recipe_video/'.$recipe_settings->first_section_top_video)}}"></video></div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="acf-breadcrumb my-4 md:my-8 text-center md:text-left">
                            <nav>
                                <ol class="list-none text-md">
                                    <li class="inline">
                                        <span>{{$recipe_settings->second_section_top_text_one}}</span>
                                    </li>
                                </ol>
                            </nav>
                        </div>

                        <div class="wp-block-media-text alignwide is-stacked-on-mobile" style="grid-template-columns:44% auto">
                            <figure class="wp-block-media-text__media"><img decoding="async" src="{{asset('storage/recipes/'.$recipe_settings->second_section_image)}}" data-spai="1" alt="" class="wp-image-300000157 size-full" sizes="(max-width: 600px) 100vw, (min-width: 1440px) 634px,44vw" data-spai-crop="false" srcset=" " data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="633.594" data-spai-height="571.031" loading="lazy"><noscript data-spai="1"><img decoding="async" src="{{asset('storage/recipes/'.$recipe_settings->second_section_image)}}" data-spai-egr="1" alt="" class="wp-image-300000157 size-full" sizes="(max-width: 600px) 100vw, (min-width: 1440px) 634px,44vw" data-spai-crop="false" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home-1024x923.jpg 1024w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home-200x180.jpg 200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home-500x451.jpg 500w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home-768x692.jpg 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home-150x135.jpg 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home-600x541.jpg 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/banner-recipe-archive-home.jpg 1580w" /></noscript></figure>
                            <div class="wp-block-media-text__content">
                                <h1 class="wp-block-heading has-text-align-left is-style-default">{{$recipe_settings->second_section_top_text_two}}</h1>

                                <p class="has-text-align-center is-style-cursive has-xxl-font-size">{{$recipe_settings->second_section_top_heading}}</p>

                                <p class="is-style-default">{{$recipe_settings->second_section_paragraph}}</p>

                                <p class="has-text-align-center is-style-cursive">{{$recipe_settings->second_section_bottom_heading}}</p>
                            </div>
                        </div>

                    @endforeach
                @endif
                @if($recipe_setting->count() == 0)
                    <div class="acf-listing-filters alignwide my-4 bg-beige p-2 pb-4 md:pb-2">
                        <div class="flex flex-wrap md:flex-no-wrap justify-center items-center p-2 md:p-6 pt-5">
                            <h2 class="h3 mb-6 md:mb-0 mt-1 w-full md:w-auto text-center md:text-left">Filtres de recherche</h2>

                            <div class="w-full md:w-auto md:mx-8">
                                <div class="">

                                    <div id="facets-search" class=" md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                        <div class="facetwp-facet facetwp-facet-search facetwp-type-search" data-name="search" data-type="search"><span class="facetwp-search-wrap flex items-center w-full md:w-48">
                    <svg class="svg-inline--fa fa-search fa-w-16 facetwp-btn top-auto mr-2 opacity-100 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                      <path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path>
                    </svg><!-- <i class="facetwp-btn fas fa-search top-auto mr-2 opacity-100 text-primary"></i> Font Awesome fontawesome.com -->
                    <input type="text" class="facetwp-search form-text m-0" value="" placeholder="Rechercher">
                  </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="w-full md:w-auto ">
                                <button class="form-text w-full md:w-48 text-left flex items-center justify-between" data-toggle="filters" data-gtm-track="" data-gtm-event="GAevent" data-gtm-event-id="06" data-gtm-category="Recipe Filter" data-gtm-action="Show filters" data-gtm-label="click">
                                    Filtres
                                    <svg class="svg-inline--fa fa-chevron-down fa-w-14 float-right" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                    </svg><!-- <i class="fas fa-chevron-down float-right"></i> Font Awesome fontawesome.com -->
                                </button>
                            </div>
                        </div>
                        <div class="bg-white p-4 mx-2 md:mx-0 md:p-10 -mt-3 md:mt-0 border-2 md:border-0 hidden" id="filters">
                            <h3 class="is-style-cursive md:-mt-4 mt-0 mb-4 text-center md:text-left">Nos meilleures recettes à base de tomates</h3>

                            <div class="flex flex-wrap -mx-4 md:-mx-8 md:-mt-8 border-secondary">
                                <div class="w-full md:w-1/3 md:p-8 ">
                                    <div class="border-t-2 md:border-2 border-secondary">
                                        <div class="flex h-full hover:zoom-img">
                                            <div class="w-1/3 overflow-hidden">
                                                <a href="https://mutti-parma.com/fr/cuisine/recettes-de-printemps-a-la-tomate/" class="flex h-full">
                                                    <img width="933" height="933" src="https://cdn.shortpixel.ai/spai/q_lossy+w_143+to_webp+ret_img/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567.jpg" data-spai="1" class="object-cover h-full w-full max-w-none" alt="Saumon en croute de sauce tomate Verdure avec chapelure" decoding="async" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="133.99970800015802" data-spai-height="NaN" loading="lazy"><noscript data-spai="1"><img width="933" height="933" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567.jpg" data-spai-egr="1" class="object-cover h-full w-full max-w-none" alt="Saumon en croute de sauce tomate Verdure avec chapelure" decoding="async" sizes="100px" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567.jpg 933w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567-200x200.jpg 200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567-500x500.jpg 500w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567-768x768.jpg 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567-150x150.jpg 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567-600x600.jpg 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567-50x50.jpg 50w" /></noscript>
                                                </a>
                                            </div>
                                            <div class="w-2/3 p-6 self-center">
                                                <a href="https://mutti-parma.com/fr/cuisine/recettes-de-printemps-a-la-tomate/">
                                                    <h4 class="mb-0">Recettes de printemps à la tomate</h4>
                                                </a>
                                                <a href="https://mutti-parma.com/fr/cuisine/recettes-de-printemps-a-la-tomate/">
                                                    <h4 class="mt-2 underline h6">Consulter </h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-full md:w-1/3 md:p-8 ">
                                    <div class="border-t-2 md:border-2 border-secondary">
                                        <div class="flex h-full hover:zoom-img">
                                            <div class="w-1/3 overflow-hidden">
                                                <a href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-la-passata-zero-residu-de-pesticides/" class="flex h-full">
                                                    <img width="2044" height="1789" src="https://cdn.shortpixel.ai/spai/q_lossy+w_143+to_webp+ret_img/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp.jpeg" data-spai="1" class="object-cover h-full w-full max-w-none" alt="" decoding="async" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="133.99970800015802" data-spai-height="NaN" loading="lazy"><noscript data-spai="1"><img width="2044" height="1789" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp.jpeg" data-spai-egr="1" class="object-cover h-full w-full max-w-none" alt="" decoding="async" sizes="100px" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp.jpeg 2044w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-500x438.jpeg 500w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-1024x896.jpeg 1024w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-200x175.jpeg 200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-768x672.jpeg 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-150x131.jpeg 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-600x525.jpeg 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-1536x1344.jpeg 1536w" /></noscript>
                                                </a>
                                            </div>
                                            <div class="w-2/3 p-6 self-center">
                                                <a href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-la-passata-zero-residu-de-pesticides/">
                                                    <h4 class="mb-0">Recettes avec la Passata Zéro Résidus de Pesticides</h4>
                                                </a>
                                                <a href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-la-passata-zero-residu-de-pesticides/">
                                                    <h4 class="mt-2 underline h6">Consulter </h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-full md:w-1/3 md:p-8 ">
                                    <div class="border-t-2 md:border-2 border-secondary">
                                        <div class="flex h-full hover:zoom-img">
                                            <div class="w-1/3 overflow-hidden">
                                                <a href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-nos-sauces-tomates-cuisinees/" class="flex h-full">
                                                    <img width="2560" height="2223" src="https://cdn.shortpixel.ai/spai/q_lossy+w_143+to_webp+ret_img/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-scaled.jpeg" data-spai="1" class="object-cover h-full w-full max-w-none" alt="" decoding="async" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="133.99970800015802" data-spai-height="NaN" loading="lazy"><noscript data-spai="1"><img width="2560" height="2223" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-scaled.jpeg" data-spai-egr="1" class="object-cover h-full w-full max-w-none" alt="" decoding="async" sizes="100px" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-scaled.jpeg 2560w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-500x434.jpeg 500w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-1024x889.jpeg 1024w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-200x174.jpeg 200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-768x667.jpeg 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-150x130.jpeg 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-600x521.jpeg 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-1536x1334.jpeg 1536w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-2048x1778.jpeg 2048w" /></noscript>
                                                </a>
                                            </div>
                                            <div class="w-2/3 p-6 self-center">
                                                <a href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-nos-sauces-tomates-cuisinees/">
                                                    <h4 class="mb-0">Recettes avec notre gamme de sauces tomates tunisienne</h4>
                                                </a>
                                                <a href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-nos-sauces-tomates-cuisinees/">
                                                    <h4 class="mt-2 underline h6">Consulter </h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex flex-wrap -mx-4 md:-mx-8 -mb-3 md:-mb-10">
                                <div class="w-full md:w-1/4 px-6 md:px-8 md:pt-0 md:pb-8 border-secondary border-t-2 md:border-t-0" id="facet-group-used_product">
                                    <div class="">
                                        <h4 class="hidden md:block">Produit en vedette</h4>
                                        <button class="md:hidden h4 w-full flex justify-between mt-4 mb-3" data-toggle="facets-used_product">
                                            <span>Produit en vedette</span>
                                            <svg class="svg-inline--fa fa-chevron-down fa-w-14 ml-2 mr-4" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                            </svg><!-- <i class="fas fa-chevron-down ml-2 mr-4"></i> Font Awesome fontawesome.com -->
                                        </button>

                                        <div id="facets-used_product" class="hidden md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                            <div class="facetwp-facet facetwp-facet-used_product facetwp-type-checkboxes" data-name="used_product" data-type="checkboxes">
                                                <div class="facetwp-checkbox" data-value="400">Double concentré <span class="facetwp-counter">(41)</span></div>
                                                <div class="facetwp-checkbox" data-value="426">Polpa <span class="facetwp-counter">(41)</span></div>
                                                <div class="facetwp-checkbox" data-value="439">Passata <span class="facetwp-counter">(33)</span></div>
                                                <div class="facetwp-checkbox" data-value="448">Pelées – Pelati <span class="facetwp-counter">(22)</span></div>
                                                <div class="facetwp-overflow facetwp-hidden">
                                                    <div class="facetwp-checkbox" data-value="853">Sauce Tomate et Parmigiano Reggiano <span class="facetwp-counter">(17)</span></div>
                                                    <div class="facetwp-checkbox" data-value="734">Pulpe de tomates bio <span class="facetwp-counter">(14)</span></div>
                                                    <div class="facetwp-checkbox" data-value="865">Sauce tomate au Basilic AOP de Gênes <span class="facetwp-counter">(14)</span></div>
                                                    <div class="facetwp-checkbox" data-value="823">Tomates cerises <span class="facetwp-counter">(14)</span></div>
                                                    <div class="facetwp-checkbox" data-value="958">Sauce tomate au Piment de Calabre <span class="facetwp-counter">(12)</span></div>
                                                    <div class="facetwp-checkbox" data-value="840">Pizza Sauce Aromatica <span class="facetwp-counter">(11)</span></div>
                                                    <div class="facetwp-checkbox" data-value="730">Pulpe en dés <span class="facetwp-counter">(11)</span></div>
                                                    <div class="facetwp-checkbox" data-value="948">Sauce Tomate et Légumes grillés <span class="facetwp-counter">(11)</span></div>
                                                    <div class="facetwp-checkbox" data-value="818">Tomates pelées <span class="facetwp-counter">(10)</span></div>
                                                    <div class="facetwp-checkbox" data-value="869">Sauce tomate aux Olives Leccino <span class="facetwp-counter">(9)</span></div>
                                                    <div class="facetwp-checkbox" data-value="828">Tomates Datterini <span class="facetwp-counter">(9)</span></div>
                                                    <div class="facetwp-checkbox" data-value="15758">Sauce tomates cuisinées <span class="facetwp-counter">(7)</span></div>
                                                    <div class="facetwp-checkbox" data-value="710">Polpa au basilic de gênes AOP <span class="facetwp-counter">(5)</span></div>
                                                    <div class="facetwp-checkbox" data-value="744">Passata Bio <span class="facetwp-counter">(4)</span></div>
                                                    <div class="facetwp-checkbox" data-value="19835">Passata Zéro Résidu de Pesticides&nbsp; <span class="facetwp-counter">(4)</span></div>
                                                    <div class="facetwp-checkbox" data-value="727">Polpa à l’ail de Voghiera <span class="facetwp-counter">(4)</span></div>
                                                    <div class="facetwp-checkbox" data-value="970">Ketchup <span class="facetwp-counter">(3)</span></div>
                                                    <div class="facetwp-checkbox" data-value="12425">Pesto de tomates rouges <span class="facetwp-counter">(3)</span></div>
                                                    <div class="facetwp-checkbox" data-value="962">Vinaigre de tomate <span class="facetwp-counter">(3)</span></div>
                                                    <div class="facetwp-checkbox" data-value="3186">Double concentré de tomates BIO <span class="facetwp-counter">(2)</span></div>
                                                    <div class="facetwp-checkbox" data-value="738">Passata au basilic <span class="facetwp-counter">(2)</span></div>
                                                    <div class="facetwp-checkbox" data-value="12429">Pesto de tomates orange <span class="facetwp-counter">(2)</span></div>
                                                    <div class="facetwp-checkbox" data-value="12427">Pesto de tomates vertes <span class="facetwp-counter">(1)</span></div>
                                                    <div class="facetwp-checkbox" data-value="19798">Sauce bolognaise végétale <span class="facetwp-counter">(1)</span></div>
                                                    <div class="facetwp-checkbox" data-value="810">Tomates pelées bio <span class="facetwp-counter">(1)</span></div>
                                                    <div class="facetwp-checkbox" data-value="14351">Tomates pelées zero residu de pesticides <span class="facetwp-counter">(1)</span></div>
                                                </div><a class="facetwp-toggle">Voir 26 plus</a><a class="facetwp-toggle facetwp-hidden">Voir moins</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-full md:w-1/4 px-6 md:px-8 md:pt-0 md:pb-8 border-secondary border-t-2 md:border-t-0" id="facet-group-course">
                                    <div class="">
                                        <h4 class="hidden md:block">Type de plat</h4>
                                        <button class="md:hidden h4 w-full flex justify-between mt-4 mb-3" data-toggle="facets-course">
                                            <span>Type de plat</span>
                                            <svg class="svg-inline--fa fa-chevron-down fa-w-14 ml-2 mr-4" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                            </svg><!-- <i class="fas fa-chevron-down ml-2 mr-4"></i> Font Awesome fontawesome.com -->
                                        </button>

                                        <div id="facets-course" class="hidden md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                            <div class="facetwp-facet facetwp-facet-course facetwp-type-checkboxes" data-name="course" data-type="checkboxes">
                                                <div class="facetwp-checkbox" data-value="plats">Plats avec des tomates <span class="facetwp-counter">(213)</span></div>
                                                <div class="facetwp-checkbox" data-value="entrees">Entrées avec des tomates <span class="facetwp-counter">(69)</span></div>
                                                <div class="facetwp-checkbox" data-value="batch-cooking-avec-des-tomates">Batch cooking <span class="facetwp-counter">(39)</span></div>
                                                <div class="facetwp-checkbox" data-value="petit-dejeuner-brunch">Petit déjeuner et brunch <span class="facetwp-counter">(23)</span></div>
                                                <div class="facetwp-overflow facetwp-hidden">
                                                    <div class="facetwp-checkbox" data-value="desserts">Desserts avec des tomates <span class="facetwp-counter">(6)</span></div>
                                                    <div class="facetwp-checkbox" data-value="boissons">Boissons <span class="facetwp-counter">(2)</span></div>
                                                </div><a class="facetwp-toggle">Voir 2 plus</a><a class="facetwp-toggle facetwp-hidden">Voir moins</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-full md:w-1/4 px-6 md:px-8 md:pt-0 md:pb-8 border-secondary border-t-2 md:border-t-0" id="facet-group-cuisine">
                                    <div class="">
                                        <h4 class="hidden md:block">Cuisine</h4>
                                        <button class="md:hidden h4 w-full flex justify-between mt-4 mb-3" data-toggle="facets-cuisine">
                                            <span>Cuisine</span>
                                            <svg class="svg-inline--fa fa-chevron-down fa-w-14 ml-2 mr-4" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                            </svg><!-- <i class="fas fa-chevron-down ml-2 mr-4"></i> Font Awesome fontawesome.com -->
                                        </button>

                                        <div id="facets-cuisine" class="hidden md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                            <div class="facetwp-facet facetwp-facet-cuisine facetwp-type-checkboxes" data-name="cuisine" data-type="checkboxes">
                                                <div class="facetwp-checkbox" data-value="italienne">Cuisine italienne <span class="facetwp-counter">(44)</span></div>
                                                <div class="facetwp-checkbox" data-value="recettes-dhiver-a-la-tomate">Recettes d'hiver à la tomate <span class="facetwp-counter">(30)</span></div>
                                                <div class="facetwp-checkbox" data-value="recettes-dete-a-la-tomate">Recettes d'été à la tomate <span class="facetwp-counter">(20)</span></div>
                                                <div class="facetwp-checkbox" data-value="recettes-de-printemps-a-la-tomate">Recettes de printemps à la tomate <span class="facetwp-counter">(13)</span></div>
                                                <div class="facetwp-overflow facetwp-hidden">
                                                    <div class="facetwp-checkbox" data-value="recettes-dautomne-a-la-tomate">Recettes d'automne à la tomate <span class="facetwp-counter">(11)</span></div>
                                                </div><a class="facetwp-toggle">Voir 1 plus</a><a class="facetwp-toggle facetwp-hidden">Voir moins</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-full md:w-1/4 px-6 md:px-8 md:pt-0 md:pb-8 border-secondary border-t-2 md:border-t-0" id="facet-group-dish">
                                    <div class="">
                                        <h4 class="hidden md:block">Plat</h4>
                                        <button class="md:hidden h4 w-full flex justify-between mt-4 mb-3" data-toggle="facets-dish">
                                            <span>Plat</span>
                                            <svg class="svg-inline--fa fa-chevron-down fa-w-14 ml-2 mr-4" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                            </svg><!-- <i class="fas fa-chevron-down ml-2 mr-4"></i> Font Awesome fontawesome.com -->
                                        </button>

                                        <div id="facets-dish" class="hidden md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                            <div class="facetwp-facet facetwp-facet-dish facetwp-type-checkboxes" data-name="dish" data-type="checkboxes">
                                                <div class="facetwp-checkbox" data-value="vegetarien">Recettes végétariennes à la tomate <span class="facetwp-counter">(61)</span></div>
                                                <div class="facetwp-checkbox" data-value="recettes-avec-nos-sauces-tomates-cuisinees">Recettes avec notre gamme de sauces tomates tunisienne <span class="facetwp-counter">(52)</span></div>
                                                <div class="facetwp-checkbox" data-value="pates-a-la-tomate">Pâtes à la tomate <span class="facetwp-counter">(49)</span></div>
                                                <div class="facetwp-checkbox" data-value="viandes">Viandes à la tomate <span class="facetwp-counter">(29)</span></div>
                                                <div class="facetwp-overflow facetwp-hidden">
                                                    <div class="facetwp-checkbox" data-value="pizzas">Recette de Pizzas maison <span class="facetwp-counter">(27)</span></div>
                                                    <div class="facetwp-checkbox" data-value="poissons">Recettes de poissons à la tomate <span class="facetwp-counter">(27)</span></div>
                                                    <div class="facetwp-checkbox" data-value="recettes-aperitives">Recettes apéritives à la tomate <span class="facetwp-counter">(22)</span></div>
                                                    <div class="facetwp-checkbox" data-value="recettes-avec-des-tomates-cerises">Recettes avec des tomates cerises <span class="facetwp-counter">(21)</span></div>
                                                    <div class="facetwp-checkbox" data-value="riz-a-la-tomate">Riz à la tomate <span class="facetwp-counter">(19)</span></div>
                                                    <div class="facetwp-checkbox" data-value="soupes">Soupes de tomates <span class="facetwp-counter">(18)</span></div>
                                                </div><a class="facetwp-toggle">Voir 6 plus</a><a class="facetwp-toggle facetwp-hidden">Voir moins</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-full md:w-1/4 px-6 md:px-8 md:pt-0 md:pb-8 border-secondary border-t-2 md:border-t-0" id="facet-group-difficulty">
                                    <div class="">
                                        <h4 class="hidden md:block">Difficulté</h4>
                                        <button class="md:hidden h4 w-full flex justify-between mt-4 mb-3" data-toggle="facets-difficulty">
                                            <span>Difficulté</span>
                                            <svg class="svg-inline--fa fa-chevron-down fa-w-14 ml-2 mr-4" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                            </svg><!-- <i class="fas fa-chevron-down ml-2 mr-4"></i> Font Awesome fontawesome.com -->
                                        </button>

                                        <div id="facets-difficulty" class="hidden md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                            <div class="facetwp-facet facetwp-facet-difficulty facetwp-type-checkboxes" data-name="difficulty" data-type="checkboxes">
                                                <div class="facetwp-checkbox" data-value="facile">Facile <span class="facetwp-counter">(217)</span></div>
                                                <div class="facetwp-checkbox" data-value="moyen">Moyen <span class="facetwp-counter">(51)</span></div>
                                                <div class="facetwp-checkbox" data-value="difficile">Difficile <span class="facetwp-counter">(8)</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-full md:w-1/4 px-6 md:px-8 md:pt-0 md:pb-8 border-secondary border-t-2 md:border-t-0" id="facet-group-video">
                                    <div class="">
                                        <h4 class="hidden md:block">Recettes vidéos</h4>
                                        <button class="md:hidden h4 w-full flex justify-between mt-4 mb-3" data-toggle="facets-video">
                                            <span>Recettes vidéos</span>
                                            <svg class="svg-inline--fa fa-chevron-down fa-w-14 ml-2 mr-4" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                            </svg><!-- <i class="fas fa-chevron-down ml-2 mr-4"></i> Font Awesome fontawesome.com -->
                                        </button>

                                        <div id="facets-video" class="hidden md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                            <div class="facetwp-facet facetwp-facet-video facetwp-type-checkboxes" data-name="video" data-type="checkboxes">
                                                <div class="facetwp-checkbox" data-value="1">Disponibles <span class="facetwp-counter">(24)</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    @foreach($recipe_setting as $key => $recipe_settings)
                        <div class="acf-listing-filters alignwide my-4 bg-beige p-2 pb-4 md:pb-2">
                            <div class="flex flex-wrap md:flex-no-wrap justify-center items-center p-2 md:p-6 pt-5">
                                <h2 class="h3 mb-6 md:mb-0 mt-1 w-full md:w-auto text-center md:text-left">Filtres de recherche</h2>

                                <div class="w-full md:w-auto md:mx-8">
                                    <div class="">

                                        <div id="facets-search" class=" md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                            <div class="facetwp-facet facetwp-facet-search facetwp-type-search" data-name="search" data-type="search"><span class="facetwp-search-wrap flex items-center w-full md:w-48">
                    <svg class="svg-inline--fa fa-search fa-w-16 facetwp-btn top-auto mr-2 opacity-100 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                      <path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path>
                    </svg><!-- <i class="facetwp-btn fas fa-search top-auto mr-2 opacity-100 text-primary"></i> Font Awesome fontawesome.com -->
                    <input type="text" class="facetwp-search form-text m-0" value="" placeholder="Rechercher">
                  </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="w-full md:w-auto ">
                                    <button class="form-text w-full md:w-48 text-left flex items-center justify-between" data-toggle="filters" data-gtm-track="" data-gtm-event="GAevent" data-gtm-event-id="06" data-gtm-category="Recipe Filter" data-gtm-action="Show filters" data-gtm-label="click">
                                        Filtres
                                        <svg class="svg-inline--fa fa-chevron-down fa-w-14 float-right" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                            <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                        </svg><!-- <i class="fas fa-chevron-down float-right"></i> Font Awesome fontawesome.com -->
                                    </button>
                                </div>
                            </div>
                            <div class="bg-white p-4 mx-2 md:mx-0 md:p-10 -mt-3 md:mt-0 border-2 md:border-0 hidden" id="filters">
                                <h3 class="is-style-cursive md:-mt-4 mt-0 mb-4 text-center md:text-left">Nos meilleures recettes à base de tomates</h3>

                                <div class="flex flex-wrap -mx-4 md:-mx-8 md:-mt-8 border-secondary">
                                    <div class="w-full md:w-1/3 md:p-8 ">
                                        <div class="border-t-2 md:border-2 border-secondary">
                                            <div class="flex h-full hover:zoom-img">
                                                <div class="w-1/3 overflow-hidden">
                                                    <a href="https://mutti-parma.com/fr/cuisine/recettes-de-printemps-a-la-tomate/" class="flex h-full">
                                                        <img width="933" height="933" src="https://cdn.shortpixel.ai/spai/q_lossy+w_143+to_webp+ret_img/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567.jpg" data-spai="1" class="object-cover h-full w-full max-w-none" alt="Saumon en croute de sauce tomate Verdure avec chapelure" decoding="async" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="133.99970800015802" data-spai-height="NaN" loading="lazy"><noscript data-spai="1"><img width="933" height="933" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567.jpg" data-spai-egr="1" class="object-cover h-full w-full max-w-none" alt="Saumon en croute de sauce tomate Verdure avec chapelure" decoding="async" sizes="100px" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567.jpg 933w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567-200x200.jpg 200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567-500x500.jpg 500w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567-768x768.jpg 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567-150x150.jpg 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567-600x600.jpg 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/07/saumon-en-croute-de-sauce-tomate-verdure-avec-chapelure-e1599139164567-50x50.jpg 50w" /></noscript>
                                                    </a>
                                                </div>
                                                <div class="w-2/3 p-6 self-center">
                                                    <a href="https://mutti-parma.com/fr/cuisine/recettes-de-printemps-a-la-tomate/">
                                                        <h4 class="mb-0">Recettes de printemps à la tomate</h4>
                                                    </a>
                                                    <a href="https://mutti-parma.com/fr/cuisine/recettes-de-printemps-a-la-tomate/">
                                                        <h4 class="mt-2 underline h6">Consulter </h4>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-full md:w-1/3 md:p-8 ">
                                        <div class="border-t-2 md:border-2 border-secondary">
                                            <div class="flex h-full hover:zoom-img">
                                                <div class="w-1/3 overflow-hidden">
                                                    <a href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-la-passata-zero-residu-de-pesticides/" class="flex h-full">
                                                        <img width="2044" height="1789" src="https://cdn.shortpixel.ai/spai/q_lossy+w_143+to_webp+ret_img/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp.jpeg" data-spai="1" class="object-cover h-full w-full max-w-none" alt="" decoding="async" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="133.99970800015802" data-spai-height="NaN" loading="lazy"><noscript data-spai="1"><img width="2044" height="1789" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp.jpeg" data-spai-egr="1" class="object-cover h-full w-full max-w-none" alt="" decoding="async" sizes="100px" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp.jpeg 2044w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-500x438.jpeg 500w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-1024x896.jpeg 1024w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-200x175.jpeg 200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-768x672.jpeg 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-150x131.jpeg 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-600x525.jpeg 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-passata-zrp-1536x1344.jpeg 1536w" /></noscript>
                                                    </a>
                                                </div>
                                                <div class="w-2/3 p-6 self-center">
                                                    <a href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-la-passata-zero-residu-de-pesticides/">
                                                        <h4 class="mb-0">Recettes avec la Passata Zéro Résidus de Pesticides</h4>
                                                    </a>
                                                    <a href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-la-passata-zero-residu-de-pesticides/">
                                                        <h4 class="mt-2 underline h6">Consulter </h4>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-full md:w-1/3 md:p-8 ">
                                        <div class="border-t-2 md:border-2 border-secondary">
                                            <div class="flex h-full hover:zoom-img">
                                                <div class="w-1/3 overflow-hidden">
                                                    <a href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-nos-sauces-tomates-cuisinees/" class="flex h-full">
                                                        <img width="2560" height="2223" src="https://cdn.shortpixel.ai/spai/q_lossy+w_143+to_webp+ret_img/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-scaled.jpeg" data-spai="1" class="object-cover h-full w-full max-w-none" alt="" decoding="async" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="133.99970800015802" data-spai-height="NaN" loading="lazy"><noscript data-spai="1"><img width="2560" height="2223" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-scaled.jpeg" data-spai-egr="1" class="object-cover h-full w-full max-w-none" alt="" decoding="async" sizes="100px" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-scaled.jpeg 2560w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-500x434.jpeg 500w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-1024x889.jpeg 1024w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-200x174.jpeg 200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-768x667.jpeg 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-150x130.jpeg 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-600x521.jpeg 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-1536x1334.jpeg 1536w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/10/categorie-sauces-tomates-2048x1778.jpeg 2048w" /></noscript>
                                                    </a>
                                                </div>
                                                <div class="w-2/3 p-6 self-center">
                                                    <a href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-nos-sauces-tomates-cuisinees/">
                                                        <h4 class="mb-0">Recettes avec notre gamme de sauces tomates tunisienne</h4>
                                                    </a>
                                                    <a href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-nos-sauces-tomates-cuisinees/">
                                                        <h4 class="mt-2 underline h6">Consulter </h4>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex flex-wrap -mx-4 md:-mx-8 -mb-3 md:-mb-10">
                                    <div class="w-full md:w-1/4 px-6 md:px-8 md:pt-0 md:pb-8 border-secondary border-t-2 md:border-t-0" id="facet-group-used_product">
                                        <div class="">
                                            <h4 class="hidden md:block">Produit en vedette</h4>
                                            <button class="md:hidden h4 w-full flex justify-between mt-4 mb-3" data-toggle="facets-used_product">
                                                <span>Produit en vedette</span>
                                                <svg class="svg-inline--fa fa-chevron-down fa-w-14 ml-2 mr-4" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                    <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                                </svg><!-- <i class="fas fa-chevron-down ml-2 mr-4"></i> Font Awesome fontawesome.com -->
                                            </button>

                                            <div id="facets-used_product" class="hidden md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                                <div class="facetwp-facet facetwp-facet-used_product facetwp-type-checkboxes" data-name="used_product" data-type="checkboxes">
                                                    <div class="facetwp-checkbox" data-value="400">Double concentré <span class="facetwp-counter">(41)</span></div>
                                                    <div class="facetwp-checkbox" data-value="426">Polpa <span class="facetwp-counter">(41)</span></div>
                                                    <div class="facetwp-checkbox" data-value="439">Passata <span class="facetwp-counter">(33)</span></div>
                                                    <div class="facetwp-checkbox" data-value="448">Pelées – Pelati <span class="facetwp-counter">(22)</span></div>
                                                    <div class="facetwp-overflow facetwp-hidden">
                                                        <div class="facetwp-checkbox" data-value="853">Sauce Tomate et Parmigiano Reggiano <span class="facetwp-counter">(17)</span></div>
                                                        <div class="facetwp-checkbox" data-value="734">Pulpe de tomates bio <span class="facetwp-counter">(14)</span></div>
                                                        <div class="facetwp-checkbox" data-value="865">Sauce tomate au Basilic AOP de Gênes <span class="facetwp-counter">(14)</span></div>
                                                        <div class="facetwp-checkbox" data-value="823">Tomates cerises <span class="facetwp-counter">(14)</span></div>
                                                        <div class="facetwp-checkbox" data-value="958">Sauce tomate au Piment de Calabre <span class="facetwp-counter">(12)</span></div>
                                                        <div class="facetwp-checkbox" data-value="840">Pizza Sauce Aromatica <span class="facetwp-counter">(11)</span></div>
                                                        <div class="facetwp-checkbox" data-value="730">Pulpe en dés <span class="facetwp-counter">(11)</span></div>
                                                        <div class="facetwp-checkbox" data-value="948">Sauce Tomate et Légumes grillés <span class="facetwp-counter">(11)</span></div>
                                                        <div class="facetwp-checkbox" data-value="818">Tomates pelées <span class="facetwp-counter">(10)</span></div>
                                                        <div class="facetwp-checkbox" data-value="869">Sauce tomate aux Olives Leccino <span class="facetwp-counter">(9)</span></div>
                                                        <div class="facetwp-checkbox" data-value="828">Tomates Datterini <span class="facetwp-counter">(9)</span></div>
                                                        <div class="facetwp-checkbox" data-value="15758">Sauce tomates cuisinées <span class="facetwp-counter">(7)</span></div>
                                                        <div class="facetwp-checkbox" data-value="710">Polpa au basilic de gênes AOP <span class="facetwp-counter">(5)</span></div>
                                                        <div class="facetwp-checkbox" data-value="744">Passata Bio <span class="facetwp-counter">(4)</span></div>
                                                        <div class="facetwp-checkbox" data-value="19835">Passata Zéro Résidu de Pesticides&nbsp; <span class="facetwp-counter">(4)</span></div>
                                                        <div class="facetwp-checkbox" data-value="727">Polpa à l’ail de Voghiera <span class="facetwp-counter">(4)</span></div>
                                                        <div class="facetwp-checkbox" data-value="970">Ketchup <span class="facetwp-counter">(3)</span></div>
                                                        <div class="facetwp-checkbox" data-value="12425">Pesto de tomates rouges <span class="facetwp-counter">(3)</span></div>
                                                        <div class="facetwp-checkbox" data-value="962">Vinaigre de tomate <span class="facetwp-counter">(3)</span></div>
                                                        <div class="facetwp-checkbox" data-value="3186">Double concentré de tomates BIO <span class="facetwp-counter">(2)</span></div>
                                                        <div class="facetwp-checkbox" data-value="738">Passata au basilic <span class="facetwp-counter">(2)</span></div>
                                                        <div class="facetwp-checkbox" data-value="12429">Pesto de tomates orange <span class="facetwp-counter">(2)</span></div>
                                                        <div class="facetwp-checkbox" data-value="12427">Pesto de tomates vertes <span class="facetwp-counter">(1)</span></div>
                                                        <div class="facetwp-checkbox" data-value="19798">Sauce bolognaise végétale <span class="facetwp-counter">(1)</span></div>
                                                        <div class="facetwp-checkbox" data-value="810">Tomates pelées bio <span class="facetwp-counter">(1)</span></div>
                                                        <div class="facetwp-checkbox" data-value="14351">Tomates pelées zero residu de pesticides <span class="facetwp-counter">(1)</span></div>
                                                    </div><a class="facetwp-toggle">Voir 26 plus</a><a class="facetwp-toggle facetwp-hidden">Voir moins</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-full md:w-1/4 px-6 md:px-8 md:pt-0 md:pb-8 border-secondary border-t-2 md:border-t-0" id="facet-group-course">
                                        <div class="">
                                            <h4 class="hidden md:block">Type de plat</h4>
                                            <button class="md:hidden h4 w-full flex justify-between mt-4 mb-3" data-toggle="facets-course">
                                                <span>Type de plat</span>
                                                <svg class="svg-inline--fa fa-chevron-down fa-w-14 ml-2 mr-4" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                    <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                                </svg><!-- <i class="fas fa-chevron-down ml-2 mr-4"></i> Font Awesome fontawesome.com -->
                                            </button>

                                            <div id="facets-course" class="hidden md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                                <div class="facetwp-facet facetwp-facet-course facetwp-type-checkboxes" data-name="course" data-type="checkboxes">
                                                    <div class="facetwp-checkbox" data-value="plats">Plats avec des tomates <span class="facetwp-counter">(213)</span></div>
                                                    <div class="facetwp-checkbox" data-value="entrees">Entrées avec des tomates <span class="facetwp-counter">(69)</span></div>
                                                    <div class="facetwp-checkbox" data-value="batch-cooking-avec-des-tomates">Batch cooking <span class="facetwp-counter">(39)</span></div>
                                                    <div class="facetwp-checkbox" data-value="petit-dejeuner-brunch">Petit déjeuner et brunch <span class="facetwp-counter">(23)</span></div>
                                                    <div class="facetwp-overflow facetwp-hidden">
                                                        <div class="facetwp-checkbox" data-value="desserts">Desserts avec des tomates <span class="facetwp-counter">(6)</span></div>
                                                        <div class="facetwp-checkbox" data-value="boissons">Boissons <span class="facetwp-counter">(2)</span></div>
                                                    </div><a class="facetwp-toggle">Voir 2 plus</a><a class="facetwp-toggle facetwp-hidden">Voir moins</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-full md:w-1/4 px-6 md:px-8 md:pt-0 md:pb-8 border-secondary border-t-2 md:border-t-0" id="facet-group-cuisine">
                                        <div class="">
                                            <h4 class="hidden md:block">Cuisine</h4>
                                            <button class="md:hidden h4 w-full flex justify-between mt-4 mb-3" data-toggle="facets-cuisine">
                                                <span>Cuisine</span>
                                                <svg class="svg-inline--fa fa-chevron-down fa-w-14 ml-2 mr-4" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                    <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                                </svg><!-- <i class="fas fa-chevron-down ml-2 mr-4"></i> Font Awesome fontawesome.com -->
                                            </button>

                                            <div id="facets-cuisine" class="hidden md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                                <div class="facetwp-facet facetwp-facet-cuisine facetwp-type-checkboxes" data-name="cuisine" data-type="checkboxes">
                                                    <div class="facetwp-checkbox" data-value="italienne">Cuisine italienne <span class="facetwp-counter">(44)</span></div>
                                                    <div class="facetwp-checkbox" data-value="recettes-dhiver-a-la-tomate">Recettes d'hiver à la tomate <span class="facetwp-counter">(30)</span></div>
                                                    <div class="facetwp-checkbox" data-value="recettes-dete-a-la-tomate">Recettes d'été à la tomate <span class="facetwp-counter">(20)</span></div>
                                                    <div class="facetwp-checkbox" data-value="recettes-de-printemps-a-la-tomate">Recettes de printemps à la tomate <span class="facetwp-counter">(13)</span></div>
                                                    <div class="facetwp-overflow facetwp-hidden">
                                                        <div class="facetwp-checkbox" data-value="recettes-dautomne-a-la-tomate">Recettes d'automne à la tomate <span class="facetwp-counter">(11)</span></div>
                                                    </div><a class="facetwp-toggle">Voir 1 plus</a><a class="facetwp-toggle facetwp-hidden">Voir moins</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-full md:w-1/4 px-6 md:px-8 md:pt-0 md:pb-8 border-secondary border-t-2 md:border-t-0" id="facet-group-dish">
                                        <div class="">
                                            <h4 class="hidden md:block">Plat</h4>
                                            <button class="md:hidden h4 w-full flex justify-between mt-4 mb-3" data-toggle="facets-dish">
                                                <span>Plat</span>
                                                <svg class="svg-inline--fa fa-chevron-down fa-w-14 ml-2 mr-4" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                    <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                                </svg><!-- <i class="fas fa-chevron-down ml-2 mr-4"></i> Font Awesome fontawesome.com -->
                                            </button>

                                            <div id="facets-dish" class="hidden md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                                <div class="facetwp-facet facetwp-facet-dish facetwp-type-checkboxes" data-name="dish" data-type="checkboxes">
                                                    <div class="facetwp-checkbox" data-value="vegetarien">Recettes végétariennes à la tomate <span class="facetwp-counter">(61)</span></div>
                                                    <div class="facetwp-checkbox" data-value="recettes-avec-nos-sauces-tomates-cuisinees">Recettes avec notre gamme de sauces tomates tunisienne <span class="facetwp-counter">(52)</span></div>
                                                    <div class="facetwp-checkbox" data-value="pates-a-la-tomate">Pâtes à la tomate <span class="facetwp-counter">(49)</span></div>
                                                    <div class="facetwp-checkbox" data-value="viandes">Viandes à la tomate <span class="facetwp-counter">(29)</span></div>
                                                    <div class="facetwp-overflow facetwp-hidden">
                                                        <div class="facetwp-checkbox" data-value="pizzas">Recette de Pizzas maison <span class="facetwp-counter">(27)</span></div>
                                                        <div class="facetwp-checkbox" data-value="poissons">Recettes de poissons à la tomate <span class="facetwp-counter">(27)</span></div>
                                                        <div class="facetwp-checkbox" data-value="recettes-aperitives">Recettes apéritives à la tomate <span class="facetwp-counter">(22)</span></div>
                                                        <div class="facetwp-checkbox" data-value="recettes-avec-des-tomates-cerises">Recettes avec des tomates cerises <span class="facetwp-counter">(21)</span></div>
                                                        <div class="facetwp-checkbox" data-value="riz-a-la-tomate">Riz à la tomate <span class="facetwp-counter">(19)</span></div>
                                                        <div class="facetwp-checkbox" data-value="soupes">Soupes de tomates <span class="facetwp-counter">(18)</span></div>
                                                    </div><a class="facetwp-toggle">Voir 6 plus</a><a class="facetwp-toggle facetwp-hidden">Voir moins</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-full md:w-1/4 px-6 md:px-8 md:pt-0 md:pb-8 border-secondary border-t-2 md:border-t-0" id="facet-group-difficulty">
                                        <div class="">
                                            <h4 class="hidden md:block">Difficulté</h4>
                                            <button class="md:hidden h4 w-full flex justify-between mt-4 mb-3" data-toggle="facets-difficulty">
                                                <span>Difficulté</span>
                                                <svg class="svg-inline--fa fa-chevron-down fa-w-14 ml-2 mr-4" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                    <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                                </svg><!-- <i class="fas fa-chevron-down ml-2 mr-4"></i> Font Awesome fontawesome.com -->
                                            </button>

                                            <div id="facets-difficulty" class="hidden md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                                <div class="facetwp-facet facetwp-facet-difficulty facetwp-type-checkboxes" data-name="difficulty" data-type="checkboxes">
                                                    <div class="facetwp-checkbox" data-value="facile">Facile <span class="facetwp-counter">(217)</span></div>
                                                    <div class="facetwp-checkbox" data-value="moyen">Moyen <span class="facetwp-counter">(51)</span></div>
                                                    <div class="facetwp-checkbox" data-value="difficile">Difficile <span class="facetwp-counter">(8)</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-full md:w-1/4 px-6 md:px-8 md:pt-0 md:pb-8 border-secondary border-t-2 md:border-t-0" id="facet-group-video">
                                        <div class="">
                                            <h4 class="hidden md:block">Recettes vidéos</h4>
                                            <button class="md:hidden h4 w-full flex justify-between mt-4 mb-3" data-toggle="facets-video">
                                                <span>Recettes vidéos</span>
                                                <svg class="svg-inline--fa fa-chevron-down fa-w-14 ml-2 mr-4" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                    <path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                                </svg><!-- <i class="fas fa-chevron-down ml-2 mr-4"></i> Font Awesome fontawesome.com -->
                                            </button>

                                            <div id="facets-video" class="hidden md:block -mx-6 md:mx-0 py-4 px-6 md:p-0 border-secondary border-t-2 md:border-t-0">
                                                <div class="facetwp-facet facetwp-facet-video facetwp-type-checkboxes" data-name="video" data-type="checkboxes">
                                                    <div class="facetwp-checkbox" data-value="1">Disponibles <span class="facetwp-counter">(24)</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif




                @if($recipe_setting->count() == 0)

                    <div class="acf-recipe-listing is-style-filtered alignwide pb-8 ">
                        <div class="facetwp-template">
                            <div class="sm:min-h-64 wp-block-media-text alignwide is-style-hero has-background-image is-stacked-on-mobile has-media-on-the-right-mobile">
                                <img decoding="async" class="wp-block-media-text__background" alt="" data-spai-crop="true" src="https://cdn.shortpixel.ai/spai/q_lossy+w_722+h_603+to_webp+ret_img/mutti-parma.com/app/uploads/sites/17/2023/09/bun-maison-haricots-blancs-a-la-tomate-et-burrata-scaled.jpg" data-spai="1" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="720" data-spai-height="601.328" loading="lazy"><noscript data-spai="1"><img decoding="async" class="wp-block-media-text__background" alt="" data-spai-crop="true" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/09/bun-maison-haricots-blancs-a-la-tomate-et-burrata-scaled.jpg" data-spai-egr="1" /></noscript>

                                <figure class="wp-block-media-text__media">
                                    <img width="2449" height="3601" src="https://cdn.shortpixel.ai/spai/q_lossy+w_371+h_545+to_webp+ret_img/mutti-parma.com/app/uploads/sites/17/2023/08/puree-de-tomates-zero-residus.png" data-spai="1" class="attachment-full size-full" alt="Passata Zéro Résidu de Pesticides&nbsp;" decoding="async" data-spai-crop="true" title="Passata Zéro Résidu de Pesticides&nbsp;" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="360" data-spai-height="529.328" loading="lazy"><noscript data-spai="1"><img width="2449" height="3601" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/08/puree-de-tomates-zero-residus.png" data-spai-egr="1" class="attachment-full size-full" alt="Passata Zéro Résidu de Pesticides&nbsp;" decoding="async" sizes="(min-width: 1440px) 480px, 30vw" data-spai-crop="true" title="Passata Zéro Résidu de Pesticides&nbsp;" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/08/puree-de-tomates-zero-residus.png 2449w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/08/puree-de-tomates-zero-residus-340x500.png 340w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/08/puree-de-tomates-zero-residus-696x1024.png 696w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/08/puree-de-tomates-zero-residus-136x200.png 136w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/08/puree-de-tomates-zero-residus-768x1129.png 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/08/puree-de-tomates-zero-residus-150x221.png 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/08/puree-de-tomates-zero-residus-600x882.png 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/08/puree-de-tomates-zero-residus-1045x1536.png 1045w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/08/puree-de-tomates-zero-residus-1393x2048.png 1393w" /></noscript>
                                </figure>
                                <div class="wp-block-media-text__content">
                                    <div class="">

                                        <a class="is-style-cursive block leading-none" href="https://mutti-parma.com/fr/produits/passata-zero-residu-de-pesticides/">
                                            Passata Zéro Résidu de Pesticides&nbsp;
                                        </a>

                                        <a class="block my-4" href="https://mutti-parma.com/fr/nos-recettes/buns-maison-haricots-blancs-a-la-tomate-et-burrata/">
                                            <h3 class="h2">Buns maison, haricots blancs à la tomate et burrata</h3>
                                        </a>

                                        <a class="block hover:no-underline text-black" href="https://mutti-parma.com/fr/nos-recettes/buns-maison-haricots-blancs-a-la-tomate-et-burrata/">

                                        </a>

                                        <div class="flex items-center justify-center sm:justify-start my-4">
                                            <div class="-ml-2 mr-2 pr-4 border-primary border-r-2 pt-2">
                                                <p class="flex flex-wrap items-end font-heavy text-primary leading-none justify-center">
                                                    <img decoding="async" alt="Total time" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/q_lossy+to_webp+ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-reduced-cooking-time.svg?id=dc737ade6befaad1eaaf" data-spai="1" width="54" height="26" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="54" data-spai-height="54" loading="lazy"><noscript data-spai="1"><img decoding="async" alt="Total time" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-reduced-cooking-time.svg?id=dc737ade6befaad1eaaf" data-spai-egr="1" width="54" height="26"></noscript>
                                                    <span class="mb-2">
                        3h
                      </span>
                                                </p>
                                            </div>
                                            <div class="pt-2">
                                                <p class="flex flex-wrap items-end font-heavy uppercase text-primary leading-none justify-center">
                                                    <img decoding="async" alt="Difficulty" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/q_lossy+to_webp+ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-stove-top.svg?id=5f4b0df898a96e10ac63" data-spai="1" width="54" height="18" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="54" data-spai-height="54" loading="lazy"><noscript data-spai="1"><img decoding="async" alt="Difficulty" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-stove-top.svg?id=5f4b0df898a96e10ac63" data-spai-egr="1" width="54" height="18"></noscript>
                                                    <span class="mb-2">
                        Moyen
                      </span>
                                                </p>
                                            </div>
                                        </div>

                                        <a class="btn mt-2 hidden sm:block" href="https://mutti-parma.com/fr/nos-recettes/buns-maison-haricots-blancs-a-la-tomate-et-burrata/">En savoir plus </a>
                                    </div>
                                </div>
                            </div>

                            <div class="alignwide">
                                <div class="flex -m-2 lg:-m-10 flex-wrap">
                                    @foreach($all_recipes as $recipe)
                                        <div class="w-full sm:w-1/2 md:w-1/3 p-4 lg:p-10">
                                            <div class="teaser--recipe text-center hover:zoom-img">
                                                <a href="{{route('recipe_detail',$recipe)}}" class="relative block mb-4 max-w-96 mx-auto" aria-hidden="true" tabindex="-1">
                                                    <div class="responsive-embed " style="padding-bottom: 108.79765395894%;">
                                                        <img width="1200" height="1600" src="{{asset('storage/recipes/'.$recipe->recipe_image)}}" data-spai="1" class="inline-block" alt="Poisson laqué sur concassé de tomates" decoding="async" data-spai-crop="true" title="Poisson laqué sur concassé de tomates" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="420" data-spai-height="456.938" loading="lazy"><noscript data-spai="1"><img width="1200" height="1600" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture.jpeg" data-spai-egr="1" class="inline-block" alt="Poisson laqué sur concassé de tomates" decoding="async" sizes="(max-width: 600px) 72vw, 431px" data-spai-crop="true" title="Poisson laqué sur concassé de tomates" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture.jpeg 1200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture-375x500.jpeg 375w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture-768x1024.jpeg 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture-150x200.jpeg 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture-600x800.jpeg 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture-1152x1536.jpeg 1152w" /></noscript>
                                                    </div>

                                                </a>

                                                <a class="block my-4" href="{{route('recipe_detail',$recipe)}}">
                                                    <h3>{{$recipe->title}}</h3>
                                                </a>

                                                <a class="block hover:no-underline text-black" href="{{route('recipe_detail',$recipe)}}">
                                                    {{ \Illuminate\Support\Str::limit(strip_tags($recipe->description),50)  }}
                                                </a>

                                                <div class="flex items-center justify-center my-4">
                                                    <div class="-ml-2 mr-2 pr-4 border-primary border-r-2 pt-2">
                                                        <p class="flex flex-wrap items-end font-heavy text-primary leading-none justify-center">
                                                            <img decoding="async" alt="Total time" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/q_lossy+to_webp+ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-reduced-cooking-time.svg?id=dc737ade6befaad1eaaf" data-spai="1" width="54" height="26" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="54" data-spai-height="54" loading="lazy"><noscript data-spai="1"><img decoding="async" alt="Total time" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-reduced-cooking-time.svg?id=dc737ade6befaad1eaaf" data-spai-egr="1" width="54" height="26"></noscript>
                                                            <span class="mb-2">
                          {{$recipe->make_time}}
                        </span>
                                                        </p>
                                                    </div>
                                                    <div class="pt-2">
                                                        <p class="flex flex-wrap items-end font-heavy uppercase text-primary leading-none justify-center">
                                                            <img decoding="async" alt="Difficulty" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/q_lossy+to_webp+ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-stove-top.svg?id=5f4b0df898a96e10ac63" data-spai="1" width="54" height="18" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="54" data-spai-height="54" loading="lazy"><noscript data-spai="1"><img decoding="async" alt="Difficulty" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-stove-top.svg?id=5f4b0df898a96e10ac63" data-spai-egr="1" width="54" height="18"></noscript>
                                                            <span class="mb-2">
                          {{$recipe->make_way}}
                        </span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <hr class="wp-block-separator alignwide">

                        <div class="text-center">
                            {{$all_recipes->links()}}
                        </div>
                    </div>
                @else
                    @foreach($recipe_setting as $key => $recipe_settings)

                        <div class="acf-recipe-listing is-style-filtered alignwide pb-8 ">
                            <div class="facetwp-template">
                                <div class="sm:min-h-64 wp-block-media-text alignwide is-style-hero has-background-image is-stacked-on-mobile has-media-on-the-right-mobile">
                                    <img decoding="async" class="wp-block-media-text__background" alt="" data-spai-crop="true" src="{{asset('storage/recipes/'.$recipe_settings->fourth_section_image)}}" data-spai="1" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="720" data-spai-height="601.328" loading="lazy"><noscript data-spai="1"><img decoding="async" class="wp-block-media-text__background" alt="" data-spai-crop="true" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/09/bun-maison-haricots-blancs-a-la-tomate-et-burrata-scaled.jpg" data-spai-egr="1" /></noscript>


                                    <div class="wp-block-media-text__content">
                                        <div class="">

                                            <a class="is-style-cursive block leading-none" href="https://mutti-parma.com/fr/produits/passata-zero-residu-de-pesticides/">
                                                {{$recipe_settings->fourth_section_heading_first}}&nbsp;
                                            </a>

                                            <a class="block my-4" href="https://mutti-parma.com/fr/nos-recettes/buns-maison-haricots-blancs-a-la-tomate-et-burrata/">
                                                <h3 class="h2">{{$recipe_settings->fourth_section_heading_sec}}</h3>
                                            </a>

                                            <a class="block hover:no-underline text-black" href="https://mutti-parma.com/fr/nos-recettes/buns-maison-haricots-blancs-a-la-tomate-et-burrata/">

                                            </a>

                                            <div class="flex items-center justify-center sm:justify-start my-4">
                                                <div class="-ml-2 mr-2 pr-4 border-primary border-r-2 pt-2">
                                                    <p class="flex flex-wrap items-end font-heavy text-primary leading-none justify-center">
                                                        <img decoding="async" alt="Total time" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/q_lossy+to_webp+ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-reduced-cooking-time.svg?id=dc737ade6befaad1eaaf" data-spai="1" width="54" height="26" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="54" data-spai-height="54" loading="lazy"><noscript data-spai="1"><img decoding="async" alt="Total time" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-reduced-cooking-time.svg?id=dc737ade6befaad1eaaf" data-spai-egr="1" width="54" height="26"></noscript>
                                                        <span class="mb-2">
                        {{$recipe_settings->fourth_section_make_time}}
                      </span>
                                                    </p>
                                                </div>
                                                <div class="pt-2">
                                                    <p class="flex flex-wrap items-end font-heavy uppercase text-primary leading-none justify-center">
                                                        <img decoding="async" alt="Difficulty" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/q_lossy+to_webp+ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-stove-top.svg?id=5f4b0df898a96e10ac63" data-spai="1" width="54" height="18" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="54" data-spai-height="54" loading="lazy"><noscript data-spai="1"><img decoding="async" alt="Difficulty" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-stove-top.svg?id=5f4b0df898a96e10ac63" data-spai-egr="1" width="54" height="18"></noscript>
                                                        <span class="mb-2">
                        {{$recipe_settings->fourth_section_make_way}}
                      </span>
                                                    </p>
                                                </div>
                                            </div>

                                            <a class="btn mt-2 hidden sm:block" href="https://mutti-parma.com/fr/nos-recettes/buns-maison-haricots-blancs-a-la-tomate-et-burrata/">{{$recipe_settings->fourth_section_learn_more_text}} </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="alignwide">
                                    <div class="flex -m-2 lg:-m-10 flex-wrap">
                                        @foreach($all_recipes as $recipe)
                                            <div class="w-full sm:w-1/2 md:w-1/3 p-4 lg:p-10">
                                                <div class="teaser--recipe text-center hover:zoom-img">
                                                    <a href="https://mutti-parma.com/fr/nos-recettes/poisson-laque-sur-concasse-de-tomates/" class="relative block mb-4 max-w-96 mx-auto" aria-hidden="true" tabindex="-1">
                                                        <div class="responsive-embed " style="padding-bottom: 108.79765395894%;">
                                                            <img width="1200" height="1600" src="{{asset('storage/recipes/'.$recipe->recipe_image)}}" data-spai="1" class="inline-block" alt="Poisson laqué sur concassé de tomates" decoding="async" data-spai-crop="true" title="Poisson laqué sur concassé de tomates" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="420" data-spai-height="456.938" loading="lazy"><noscript data-spai="1"><img width="1200" height="1600" src="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture.jpeg" data-spai-egr="1" class="inline-block" alt="Poisson laqué sur concassé de tomates" decoding="async" sizes="(max-width: 600px) 72vw, 431px" data-spai-crop="true" title="Poisson laqué sur concassé de tomates" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture.jpeg 1200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture-375x500.jpeg 375w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture-768x1024.jpeg 768w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture-150x200.jpeg 150w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture-600x800.jpeg 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2023/12/lilian-image-couverture-1152x1536.jpeg 1152w" /></noscript>
                                                        </div>

                                                    </a>

                                                    <a class="block my-4" href="https://mutti-parma.com/fr/nos-recettes/poisson-laque-sur-concasse-de-tomates/">
                                                        <h3>{{$recipe->title}}</h3>
                                                    </a>

                                                    <a class="block hover:no-underline text-black" href="https://mutti-parma.com/fr/nos-recettes/poisson-laque-sur-concasse-de-tomates/">
                                                        Réalisez la recette de notre chef ambassadeur&nbsp;Lilian Douchet, à l’aide des Cubes de Tomates Mutti !
                                                    </a>

                                                    <div class="flex items-center justify-center my-4">
                                                        <div class="-ml-2 mr-2 pr-4 border-primary border-r-2 pt-2">
                                                            <p class="flex flex-wrap items-end font-heavy text-primary leading-none justify-center">
                                                                <img decoding="async" alt="Total time" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/q_lossy+to_webp+ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-reduced-cooking-time.svg?id=dc737ade6befaad1eaaf" data-spai="1" width="54" height="26" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="54" data-spai-height="54" loading="lazy"><noscript data-spai="1"><img decoding="async" alt="Total time" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-reduced-cooking-time.svg?id=dc737ade6befaad1eaaf" data-spai-egr="1" width="54" height="26"></noscript>
                                                                <span class="mb-2">
                          {{$recipe->make_time}}
                        </span>
                                                            </p>
                                                        </div>
                                                        <div class="pt-2">
                                                            <p class="flex flex-wrap items-end font-heavy uppercase text-primary leading-none justify-center">
                                                                <img decoding="async" alt="Difficulty" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/q_lossy+to_webp+ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-stove-top.svg?id=5f4b0df898a96e10ac63" data-spai="1" width="54" height="18" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="54" data-spai-height="54" loading="lazy"><noscript data-spai="1"><img decoding="async" alt="Difficulty" class="w-10 md:w-12 mx-3 mb-2" src="https://cdn.shortpixel.ai/spai/ret_img/mutti-parma.com/app/themes/mutti/dist/images/icons/mutti-product-icons-stove-top.svg?id=5f4b0df898a96e10ac63" data-spai-egr="1" width="54" height="18"></noscript>
                                                                <span class="mb-2">
                          {{$recipe->make_way}}
                        </span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach




                                    </div>
                                </div>
                            </div>

                            <hr class="wp-block-separator alignwide">

                            <div class="text-center">
                                <div class="facetwp-pager">
                                    <ul class="list-none flex items-center justify-center h5" role="navigation">
                                        <li class="disabled">
                                            <svg class="svg-inline--fa fa-chevron-left fa-w-10 m-2 opacity-50" title="Précédente " aria-labelledby="svg-inline--fa-title-ebt4OHQtpGqv" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                                                <title id="svg-inline--fa-title-ebt4OHQtpGqv">Précédente </title>
                                                <path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path>
                                            </svg><!-- <i class="fas fa-chevron-left m-2 opacity-50" title="Précédente "></i> Font Awesome fontawesome.com -->
                                        </li>

                                        <li>
                                            <span class="current p-2 text-black">1</span>
                                        </li>

                                        <li>
                                            <a class="facetwp-page p-2" data-page="2">2</a>
                                        </li>
                                        <li>
                                            <a class="facetwp-page p-2" data-page="3">3</a>
                                        </li>

                                        <li class="ellipsis text-black">…</li>
                                        <li>
                                            <a class="facetwp-page p-2" data-page="23">23</a>
                                        </li>

                                        <li class="">
                                            <a class="facetwp-page p-2" data-page="2">
                                                <svg class="svg-inline--fa fa-chevron-right fa-w-10" title="Suivante" aria-labelledby="svg-inline--fa-title-pOh4flMU9bzr" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                                                    <title id="svg-inline--fa-title-pOh4flMU9bzr">Suivante</title>
                                                    <path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path>
                                                </svg><!-- <i class="fas fa-chevron-right" title="Suivante"></i> Font Awesome fontawesome.com -->
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    @endforeach
                @endif

                {{--      <div class="acf-recipe-category-listing aligncenter my-12 text-center">--}}
                {{--        <h3>Que faire avec des tomates en conserve ou une sauce tomate ?</h3>--}}

                {{--        <ul class="list-none flex flex-wrap justify-center">--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/difficulte/difficile/">Difficile</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/course/batch-cooking-avec-des-tomates/">Batch cooking</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-la-passata-zero-residu-de-pesticides/">Recettes avec la Passata Zéro Résidus de Pesticides</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/cuisine/recettes-dhiver-a-la-tomate/">Recettes d'hiver à la tomate</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/course/desserts/">Desserts avec des tomates</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/difficulte/moyen/">Moyen</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/sandwichs/">Sandwichs</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/cuisine/recettes-de-printemps-a-la-tomate/">Recettes de printemps à la tomate</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/course/entrees/">Entrées avec des tomates</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/difficulte/facile/">Facile</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/salades-a-la-tomate/">Salades avec des tomates</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/cuisine/recettes-dete-a-la-tomate/">Recettes d'été à la tomate</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/course/boissons/">Boissons</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-des-tomates-cerises/">Recettes avec des tomates cerises</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/cuisine/recettes-dautomne-a-la-tomate/">Recettes d'automne à la tomate</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/course/petit-dejeuner-brunch/">Petit déjeuner et brunch</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/recettes-aperitives/">Recettes apéritives à la tomate</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/course/plats/">Plats avec des tomates</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-du-ketchup/">Recettes avec du ketchup</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-nos-pestos-de-tomates/">Recettes avec nos pestos de tomates</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/recettes-avec-nos-sauces-tomates-cuisinees/">Recettes avec notre gamme de sauces tomates tunisienne</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/desserts/">Desserts</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/cuisine/italienne/">Cuisine italienne</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/entrees/">Entrées</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/cocktails/">Cocktails</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/sauces/">Sauces</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/poissons/">Recettes de poissons à la tomate</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/viandes/">Viandes à la tomate</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/riz-a-la-tomate/">Riz à la tomate</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/pates-a-la-tomate/">Pâtes à la tomate</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/pizzas/">Recette de Pizzas maison</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/tartes-quiches/">Tartes &amp; Quiches à la tomate</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/vegetarien/">Recettes végétariennes à la tomate</a>--}}
                {{--          </li>--}}
                {{--          <li>--}}
                {{--            <a class="p-2" href="https://mutti-parma.com/fr/type-de-plat/soupes/">Soupes de tomates</a>--}}
                {{--          </li>--}}
                {{--        </ul>--}}
                {{--      </div>--}}

            </div>

        </div>

    </section>
    <!-- our values section start -->

@endsection
@section('js')

    <script src="{{asset('assets/frontend/js/owl.carousel.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
@endsection
