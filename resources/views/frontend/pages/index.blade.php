@extends('frontend.layouts.app')
@section('title','Home')
@section('content')

    <section id="slider_sec" class="slider_sec">
        <div id="carouselExampleFade" class="carousel slide carousel-fade">
            <div class="carousel-inner">
                @if($all_sliders->count() == 0)
                    <div class="carousel-item text-center text-white d-flex align-items-center justify-content-center active">
                        <div>
                            <h2 class="fw-bold">
                                TAKES PRACTICE
                            </h2>
                            <h1 class="fw-bold">
                                SUPERIOR OF EXCELLENCE
                            </h1>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing
                                and typesetting industry. Lorem Ipsum has <br>
                                been the industry's standard dummy text ever
                                since the 1500s,
                            </p>
                            <button
                                class="btn btn-primary px-lg-4 px-3 rounded-pill border-0">
                                CONTACT US
                            </button>
                        </div>
                    </div>
                    <div class="carousel-item text-center text-white d-flex align-items-center justify-content-center ">
                        <div>
                            <h2 class="fw-bold text-uppercase">TAKES
                                PRACTICE</h2>
                            <h1 class="fw-bold text-uppercase">Practice makes
                                perfect</h1>
                            <p>the pinnacle of excellence. Lorem Ipsum remains
                                the industry's standard dummy text, unchanged
                                since the 1500s. </p>
                            <button
                                class="btn btn-primary px-lg-4 px-3 rounded-pill border-0">ABOUT
                                US
                            </button>
                        </div>
                    </div>
                @else
                    @foreach($all_sliders as $key => $slider)
                        <div class="carousel-item text-center text-white d-flex align-items-center justify-content-center @if($key == 0) active @endif"
                             style="background-image: url({{ asset('storage/slider/'.$slider->slider_image)  }})"
                             onerror="this.src='{{asset('assets/frontend/images/slider.png')}}"
                        >
                            <div>
                                <h2 class="fw-bold">
                                    {{$slider->title}}
                                </h2>
                                <h1 class="fw-bold">
                                    {{$slider->heading}}
                                </h1>
                                <p>
                                    {{$slider->description}}
                                </p>
                                <a href="{{$slider->button_link}}"
                                    class="btn btn-primary px-lg-4 px-3 rounded-pill border-0">
                                    {{$slider->button_text}}
                                </a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            <button class="carousel-control-prev" type="button"
                    data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon"
                          aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button"
                    data-bs-target="#carouselExampleFade" data-bs-slide="next">
                    <span class="carousel-control-next-icon"
                          aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>
    <!-- zina house section start -->
    <section id="zina_house">
        <h2 class="text-red fw-bold my-lg-5 my-3 text-center">
            @if($home_page_setting && $home_page_setting->zina_house_title)
                {{$home_page_setting->zina_house_title}}
            @else
                THE ZINA HOUSE
            @endif
        </h2>
        @if($home_page_setting && $home_page_setting->zina_house_image )
            <div class="zina_house_bg d-flex align-items-center justify-content-center"
                 style="background-image: url({{asset('storage/zina_house/'.$home_page_setting->zina_house_image )}})">
                @else
                    <div class="zina_house_bg d-flex align-items-center justify-content-center">
                        @endif
                        <div class="container text-center  ">
                            <div class="row">
                                <div class="col-6"></div>
                                <div class="col-6 d-flex align-items-center ps-lg-5">
                                    <div class="ps-lg-3">
                                        <h1
                                            class="text_italic textAnanda text-musterd mb-3">
                                            @if($home_page_setting && $home_page_setting->zina_house_image_text )
                                                {{$home_page_setting->zina_house_image_text }}
                                            @else
                                                The tomato that speaks to us...
                                            @endif
                                        </h1>

                                        @if($home_page_setting && $home_page_setting->zina_house_button_link  )
                                            <a href="{{$home_page_setting->zina_house_button_link}}"
                                               class="btn btn-success px-lg-4 px-3 rounded-3 py-2 border-0">
                                                @else
                                                    <a href="#"
                                                       class="btn btn-success px-lg-4 px-3 rounded-3 py-2 border-0">
                                                        @endif
                                                        @if($home_page_setting && $home_page_setting->zina_house_button_text  )
                                                            {{$home_page_setting->zina_house_button_text  }}
                                                        @else
                                                            Know more
                                                        @endif
                                                    </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    </section>
    <!-- our product -->
    <section id="ourProduct" class="py-lg-5 py-3">
        <div class="container">
            <p class="text-center fw-bold mb-lg-5">
                @if($home_page_setting && $home_page_setting->our_product_heading)
                    {{$home_page_setting->our_product_heading}}
                @else
                    orem Ipsum is simply
                    dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard
                @endif
            </p>
            <div class="row">
                <div
                    class="col-md-4  d-flex align-items-center justify-content-center text-end">
                    <div>
                        <div class="tree_planting mb-lg-5 mb-3">
                            @if($home_page_setting && $home_page_setting->our_product_point_one_title)
                                <h6 class="fw-bold">{{$home_page_setting->our_product_point_one_title}}</h6>
                            @else
                                <h6 class="fw-bold">Tree Planting </h6>
                            @endif
                            @if($home_page_setting && $home_page_setting->our_product_point_one_desc)
                                <p>
                                    {{$home_page_setting->our_product_point_one_desc}}
                                </p>
                            @else
                                <p>Lorem Ipsum is simply dummy text
                                    of the printing and typesetting
                                    industry. Lorem Ipsum
                                </p>
                            @endif
                        </div>
                        <div class="Rush_Removal position-relative">
                            @if($home_page_setting && $home_page_setting->our_product_point_three_title)
                                <h6 class="fw-bold">{{$home_page_setting->our_product_point_three_title}}</h6>
                            @else
                                <h6 class="fw-bold">Rush Removal </h6>
                            @endif
                            @if($home_page_setting && $home_page_setting->our_product_point_three_desc)
                                <p>
                                    {{$home_page_setting->our_product_point_three_desc}}
                                </p>
                            @else
                                <p>Lorem Ipsum is simply dummy text
                                    of the printing and typesetting
                                    industry. Lorem Ipsum
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
                <div
                    class="col-md-4 d-flex align-items-center justify-content-center mb-lg-0 mb-3">
                    <div>
                        @if($home_page_setting && $home_page_setting->our_product_image)
                            <img src="{{asset('storage/our_product/'.$home_page_setting->our_product_image)}}"
                                 class="img-fluid product_img"
                                 alt="product_zina">
                        @else
                            <img src="{{asset('assets/frontend/images/product_zina.png')}}"
                                 class="img-fluid product_img"
                                 alt="product_zina">
                        @endif
                    </div>
                </div>
                <div
                    class="col-md-4 d-flex align-items-center justify-content-center text-start">
                    <div>
                        <div class="LandScapingDesign mb-lg-5 mb-3">
                            @if($home_page_setting && $home_page_setting->our_product_point_two_title)
                                <h6 class="fw-bold">{{$home_page_setting->our_product_point_two_title}}</h6>
                            @else
                                <h6 class="fw-bold">Land Scaping Design</h6>
                            @endif
                            @if($home_page_setting && $home_page_setting->our_product_point_two_desc)
                                <p>
                                    {{$home_page_setting->our_product_point_two_desc}}
                                </p>
                            @else
                                <p>Lorem Ipsum is simply dummy text
                                    of the printing and typesetting
                                    industry. Lorem Ipsum
                                </p>
                            @endif
                        </div>
                        <div class="Planting_Flower">
                            @if($home_page_setting && $home_page_setting->our_product_point_four_title)
                                <h6 class="fw-bold">{{$home_page_setting->our_product_point_four_title}}</h6>
                            @else
                                <h6 class="fw-bold">Planting Flower </h6>
                            @endif
                            @if($home_page_setting && $home_page_setting->our_product_point_four_desc)
                                <p>
                                    {{$home_page_setting->our_product_point_four_desc}}
                                </p>
                            @else
                                <p>Lorem Ipsum is simply dummy text
                                    of the printing and typesetting
                                    industry. Lorem Ipsum
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- our values section start -->
    <section id="ourValues">
        <h2 class="text-red fw-bold my-lg-5 my-3 text-center">
            @if($home_page_setting && $home_page_setting->our_value_heading)
                {{$home_page_setting->our_value_heading}}
            @else
                Our tomatoes
            @endif
        </h2>
        <div
            class="our-values-bg d-flex align-items-center justify-content-center">

            <div class="container text-center  ">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div
                        class="col-md-8 col-12 d-flex align-items-center justify-content-lg-start justify-content-center">
                        <div class>
                            <h1 class="text_italic fw-bold text-white ">
                                @if($home_page_setting && $home_page_setting->our_value_image_text)
                                    {{$home_page_setting->our_value_image_text}}
                                @else
                                    OUR PEELED TOMATOES WITH
                                    ZERO PESTICIDE RESIDUE
                                @endif
                            </h1>
                            <h2 class="fw-bold mb-3 textAnanda">

                                @if($home_page_setting && $home_page_setting->our_value_image_second_text)
                                    {{$home_page_setting->our_value_image_second_text}}
                                @else
                                    A journey towards excellence
                                @endif
                            </h2>
                            <button
                                class="btn btn-danger px-lg-4 px-3 rounded-3 py-2 border-0">
                                @if($home_page_setting && $home_page_setting->our_value_button_text)
                                    {{$home_page_setting->our_value_button_text}}
                                @else
                                    A journey towards excellence
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- zina products -->
    <section id="zinaProducts" class=" py-lg-5 py-3">
        <div class="container">
            <div class="row">
                <div
                    class="col-lg-6 d-flex align-items-center justify-content-center ">
                    <div class="px-lg-5">
                        <p class="fw-bold text-center mb-0 textAnanda">
                            @if($home_page_setting && $home_page_setting->zina_product_heading)
                                {{$home_page_setting->zina_product_heading}}
                            @else
                                Canned tomatoes and Tunisian tomato sauces
                            @endif
                        </p>
                        <h1 class="text-red text-center fw-bold">
                            @if($home_page_setting && $home_page_setting->zina_product_title)
                                {{$home_page_setting->zina_product_title}}
                            @else
                                ZINA PRODUCTS
                            @endif
                        </h1>
                        <p class="text-success text-center textAnanda">
                            @if($home_page_setting && $home_page_setting->zina_product_text)
                                {{$home_page_setting->zina_product_text}}
                            @else
                                A tradition of quality and passion
                            @endif
                        </p>
                        <p class="text-justify">
                            @if($home_page_setting && $home_page_setting->zina_product_desc)
                                {{$home_page_setting->zina_product_desc}}
                            @else
                                For 120 years, we have devoted all our efforts and passion to creating the best canned Tunisian tomatoes for your kitchen. Discover all Mutti products: tomato pulp, tomato puree, peeled tomatoes and tomato paste and how to choose the ones best suited to your culinary preparations!
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-lg-6 ">
                    @if($home_page_setting && $home_page_setting->zina_product_image)
                        <img src="{{asset('storage/zina_product/'.$home_page_setting->zina_product_image)}}"
                             class="img-fluid" alt="zinaProducts">
                    @else
                        <img src="{{asset('assets/frontend/images/zina3cane.png')}}"
                             class="img-fluid" alt="zinaProducts">
                    @endif

                </div>
            </div>
        </div>
    </section>
    <!-- news section start -->
    <section id="ourNews" class="py-lg-5 py-3">
        <div class="container">
            <div class="row">
                <div class="col-md-6 d-flex align-items-center justify-content-end  mb-lg-0 mb-3">
                    @if($home_page_setting && $home_page_setting->our_news_image)
                        <img src="{{asset('storage/our_news/'.$home_page_setting->our_news_image)}}"
                             class="img-fluid" alt="Calque">
                    @else
                        <img src="{{asset('assets/frontend/images/Calque.png')}}" class="img-fluid"
                             alt="Calque">
                    @endif
                </div>
                <div class="col-md-6">
                    <h5 class="category textAnanda text-red">
                        @if($home_page_setting && $home_page_setting->our_news_title)
                            {{$home_page_setting->our_news_title}}
                        @else
                            News
                        @endif
                    </h5>
                    <h5
                        class="date text-red fw-bold mb-lg-4 ">06/04/2024</h5>
                    <h3 class="title text-red fw-bold mb-3">
                        @if($home_page_setting && $home_page_setting->our_news_heading)
                            {{$home_page_setting->our_news_heading}}
                        @else
                            CELEBRATE THE TOMATO ON APRIL 6
                        @endif
                    </h3>
                    <p class="text-justify long_text">
                        @if($home_page_setting && $home_page_setting->our_news_desc)
                            {{$home_page_setting->our_news_desc}}
                        @else
                            World Tomato Day celebrates one of the most versatile and beloved fruits in the world. A special occasion which allows us to highlight the multiple qualities of the tomato, whether it be its culinary, nutritional or cultural importance. For over 120 years, Mutti has grown and celebrated tomatoes with passion, to offer quality products that capture the very essence of this iconic fruit of Italian cuisine.
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- OUR RECIPES section start -->
    <section id="OUR_RECIPES" class="py-lg-5 py-3 ">
        <div class="container">
            <h2 class="fw-bold text-red text-center  pasta_heading">
                @if($home_page_setting && $home_page_setting->our_recipe_heading)
                    {{$home_page_setting->our_recipe_heading}}
                @else
                    OUR RECIPES WITH 100% TUNISIAN TOMATOES
                @endif
            </h2>
            @if($home_page_setting && $home_page_setting->our_recipe_top_image)
                <img src="{{asset('storage/our_recipe/'.$home_page_setting->our_recipe_top_image)}}" class="w-100 img-fluid"
                     alt="pasta">
            @else
                <img src="{{asset('assets/frontend/images/pasta.png')}}" class="w-100 img-fluid"
                     alt="pasta">
            @endif
            <div class="row my-3">
                <div
                    class="col-md-4 mb-3 d-flex align-items-center justify-content-center">
                    @if($home_page_setting && $home_page_setting->our_recipe_bottom_image_one)
                        <img src="{{asset('storage/our_recipe/'.$home_page_setting->our_recipe_bottom_image_one)}}"
                             class="img-fluid " alt="receipe1">
                    @else
                        <img src="{{asset('assets/frontend/images/receipe1.png')}}"
                             class="img-fluid " alt="receipe1">
                    @endif
                </div>
                <div
                    class="col-md-4 mb-3 d-flex align-items-center justify-content-center">
                    @if($home_page_setting && $home_page_setting->our_recipe_bottom_image_two)
                        <img src="{{asset('storage/our_recipe/'.$home_page_setting->our_recipe_bottom_image_two)}}"
                             class="img-fluid " alt="receipe2">
                    @else
                        <img src="{{asset('assets/frontend/images/receipe2.png')}}"
                             class="img-fluid " alt="receipe2">
                    @endif
                </div>
                <div
                    class="col-md-4 mb-3 d-flex align-items-center justify-content-center">

                    @if($home_page_setting && $home_page_setting->our_recipe_bottom_image_three)
                        <img src="{{asset('storage/our_recipe/'.$home_page_setting->our_recipe_bottom_image_three)}}"
                             class="img-fluid " alt="receipe3">
                    @else
                        <img src="{{asset('assets/frontend/images/receipe3.png')}}"
                             class="img-fluid " alt="receipe3">
                    @endif
                </div>
            </div>
            <h2 class="fw-bold text-red text-center  pasta_heading">
                @if($home_page_setting && $home_page_setting->our_recipe_bottom_text)
                    {{$home_page_setting->our_recipe_bottom_text}}
                @else
                    DISCOVER OUR DELICIOUS RECIPES WITH TOMATOES
                @endif

            </h2>
        </div>
    </section>

@endsection


