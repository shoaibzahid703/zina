@extends('frontend.layouts.app')
@section('title','News')
@section('css')
    <link rel='stylesheet' id='sage/app.css-css' href="{{asset('assets/frontend/css/app.css')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='sage/app.css-css' href="{{asset('assets/frontend/css/reset.css')}}" type='text/css' media='all' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{asset('assets/frontend/css/style.min.css')}}" />
@endsection
@section('content')

    <section id="ourProduct" class="py-lg-5 py-3">
        <div class="container">

            <div class="max-w-content mx-auto entry-content post-8 page type-page status-publish hentry">
                @if($news_page_setting && $news_page_setting->news_bg_image)
                    <div data-spai-bg-prepared="1" class="wp-block-cover alignfull has-black-background-color has-background-dim" style="background-image:url('{{asset('storage/news_setting/'.$news_page_setting->news_bg_image)}}');background-position:54.70059712727865% 47.59843480868602%">
                        @else
                            <div data-spai-bg-prepared="1" class="wp-block-cover alignfull has-black-background-color has-background-dim" style="background-image:url(https://cdn.shortpixel.ai/spai/w_1920+q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/17/2020/05/actualites-mutti-france.jpg);background-position:54.70059712727865% 47.59843480868602%">
                                @endif
                                <div class="wp-block-cover__inner-container is-layout-flow wp-block-cover-is-layout-flow">
                                    <div class="acf-breadcrumb my-4 md:my-8 text-center">
                                        <nav>
                                            <ol class="list-none text-md">
                                                <li class="inline">
                                                    <a class="text-inherit" href="{{route('welcome')}}">
                                                        @if($news_page_setting && $news_page_setting->top_welcome_text)
                                                            {{$news_page_setting->top_welcome_text ?? ''}}
                                                        @else
                                                            Welcome
                                                        @endif

                                                    </a>
                                                    <span aria-hidden="true"> /&nbsp;</span>
                                                </li>
                                                <li class="inline">
                                                    <a class="text-inherit" href="{{route('home-page-news')}}">
                                                        @if($news_page_setting &&  $news_page_setting->top_news_text)
                                                            {{$news_page_setting->top_news_text ?? ''}}
                                                        @else
                                                            News
                                                        @endif
                                                    </a>
                                                </li>
                                            </ol>
                                        </nav>
                                    </div>

                                    <h1 class="has-text-align-center wp-block-heading">
                                        @if($news_page_setting &&  $news_page_setting->top_news_text)
                                            {{$news_page_setting->top_news_text ?? ''}}
                                        @else
                                            NEWS
                                        @endif
                                    </h1>

                                    <p class="has-text-align-center is-style-cursive">
                                        @if($news_page_setting && $news_page_setting->news_heading)
                                            {{$news_page_setting->news_heading ?? ''}}
                                        @else
                                            Discover the latest news from the brand
                                        @endif
                                    </p>
                                </div>
                            </div>


                            <div class="acf-content-listing alignwide">

                                <div class="flex -mx-6 -mt-5 md:-mt-6 flex-wrap">
                                    @foreach($all_news as $news)
                                        <div class="w-1/1 sm:w-1/2 md:w-1/3 px-6 py-5 md:py-6">
                                            <div class="max-w-96 mx-auto">
                                                <a href="{{route('news_detail',$news)}}" class="block">
                                                    <div class="responsive-embed " style="padding-bottom: 67.567567567568%;"><img width="600" height="500" src="{{asset('storage/news_image/'.$news->news_image)}}" data-spai="1" class="inline-block" alt="process planting" decoding="async" data-spai-crop="true" data-spai-loading="lazy" data-spai-lazy-loaded="true" data-spai-width="432" data-spai-height="291.891" loading="lazy"><noscript data-spai="1"><img width="600" height="500" src="{{asset('storage/news_image/'.$news->news_image)}}" data-spai-egr="1" class="inline-block" alt="process planting" decoding="async" data-spai-crop="true" srcset="https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-process-planting.jpg 600w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-process-planting-200x167.jpg 200w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-process-planting-500x417.jpg 500w, https://cdn.shortpixel.ai/spai/q_lossy+ret_img+to_webp/mutti-parma.com/app/uploads/sites/3/2019/09/section-process-planting-150x125.jpg 150w" sizes="(max-width: 600px) 100vw, 600px" /></noscript></div>
                                                </a>

                                                <div class="flex h5">
                                                    <div>
                                                        {{ date('Y-m-d', strtotime($news->created_at)) }}
                                                    </div>
                                                </div>

                                                <a class="block no-underline mb-2 hover:underline" href="{{route('news_detail',$news)}}">
                                                    <h3>{{$news->title}}</h3>
                                                </a>

                                                <a class="block h5 underline" href="{{route('news_detail',$news)}}">
                                                    En savoir plus
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>



                                <hr class="wp-block-separator alignwide">

                                <div class="pager">
                                    {{$all_news->links()}}

                                </div>
                            </div>

                    </div>
            </div>

    </section>
@endsection
@section('js')

    <script src="{{asset('assets/frontend/js/owl.carousel.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
@endsection
