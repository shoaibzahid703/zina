
<nav class="navbar navbar-expand-lg bg-custom mt-4">
    <div class="container-fluid mx-lg-4">
        <div class="collapse navbar-collapse"
             id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link {{active_class('welcome')}}" aria-current="page"
                       href="{{route('welcome')}}">
                        @if($front_navbar && $front_navbar->welcome_menu_text  )
                            {{$front_navbar->welcome_menu_text  }}
                        @else
                            Welcome
                        @endif
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{active_class('zina_house')}}"
                       href="{{route('zina_house')}}">
                        @if($front_navbar && $front_navbar->zina_house_menu_text  )
                            {{$front_navbar->zina_house_menu_text  }}
                        @else
                            ZINA HOUSE
                        @endif
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{active_class('home-page-products')}}"
                       href="{{route('home-page-products')}}">
                        @if($front_navbar && $front_navbar->our_products_menu_text  )
                            {{$front_navbar->our_products_menu_text  }}
                        @else
                            Our Products
                        @endif
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{active_class('our_values')}}" aria-current="page"
                       href="{{route('our_values')}}">

                        @if($front_navbar && $front_navbar->our_values_menu_text  )
                            {{$front_navbar->our_values_menu_text  }}
                        @else
                            Our values
                        @endif
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{active_class('home-page-news')}}" aria-current="page"
                       href="{{route('home-page-news')}}">
                        @if($front_navbar && $front_navbar->news_menu_text  )
                            {{$front_navbar->news_menu_text  }}
                        @else
                            News
                        @endif
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{active_class('home-page-recipes')}}" aria-current="page"
                       href="{{route('home-page-recipes')}}">
                        @if($front_navbar && $front_navbar->recipes_menu_text  )
                            {{$front_navbar->recipes_menu_text  }}
                        @else
                            Recipes
                        @endif
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" aria-current="page"
                       href="{{route('contact')}}">
                        @if($front_navbar && $front_navbar->contact_menu_text  )
                            {{$front_navbar->contact_menu_text  }}
                        @else
                            Contact
                        @endif
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
