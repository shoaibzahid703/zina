<!-- footer -->
<footer id="contact" class>
    <div class="container py-lg-5 py-3 footer_border">

        <div class="row">
            <div class="col-md-3">
                <img src="{{asset('assets/frontend/images/zina-logo.png')}}"
                     class="img-fluid mb-3" alt="zina">

                @if($front_footer_setting && $front_footer_setting->footer_description)
                    <p class="text-justify">{{$front_footer_setting->footer_description}}</p>
                @else
                    <p>
                        Lorem Ipsum is simply dummy
                        text of the printing and
                        type setting industry
                    </p>
                @endif
            </div>
            <div class="col-md-3 my-3">
                @if($front_footer_setting && $front_footer_setting->contact_us_title)
                    <h4 class="fw-bold mb-3 mt-lg-5">{{$front_footer_setting->contact_us_title}}</h4>
                @else
                    <h4 class="fw-bold mb-3 mt-lg-5">Contact Us</h4>
                @endif
                <p class="mb-1">
                    @if($front_footer_setting && $front_footer_setting->address_title)
                        <span class="fw-bold">{{$front_footer_setting->address_title}}:</span>
                    @else
                        <span class="fw-bold">Address:</span>
                    @endif
                    <span class="ms-1">
                         @if($front_footer_setting && $front_footer_setting->address)
                            {{$front_footer_setting->address}}
                        @else
                            Site Industriel à Oued El Hajar, Km 4 Kélibia, 8090 Tunisie
                        @endif
                    </span>
                </p>
                <p class="mb-1">
                    @if($front_footer_setting && $front_footer_setting->phone_title)
                        <span class="fw-bold">{{$front_footer_setting->phone_title}}:</span>
                    @else
                        <span class="fw-bold">Phone:</span>
                    @endif

                    @if($front_footer_setting && $front_footer_setting->phone)
                        <a href="tel:{{$front_footer_setting->phone}}"
                           class="ms-1 text-dark">
                            {{$front_footer_setting->phone}}
                        </a>
                    @else
                        <a href="tel:+216 72 273 123"
                           class="ms-1 text-dark">
                            +216 72 273 123 / 72 273 133
                        </a>
                    @endif
                </p>
                <p>
                    @if($front_footer_setting && $front_footer_setting->email_title)
                        <span class="fw-bold">{{$front_footer_setting->email_title}}:</span>
                    @else
                        <span class="fw-bold">Email:</span>
                    @endif
                    @if($front_footer_setting && $front_footer_setting->email)
                        <a href="mailto:{{$front_footer_setting->email}}"
                           class="ms-1 text-dark">{{$front_footer_setting->email}}
                        </a>
                    @else
                        <a href="mailto:contact@zinagroup.com"
                           class="ms-1 text-dark">contact@zinagroup.com
                        </a>
                    @endif

                </p>
            </div>
            <div class="col-md-3 col-6 my-lg-3">
                @if($front_footer_setting && $front_footer_setting->quick_link_title)
                    <h4 class="fw-bold mb-3 mt-lg-5">{{$front_footer_setting->quick_link_title}}</h4>
                @else
                    <h4 class="fw-bold mb-3 mt-lg-5">Quick Links</h4>
                @endif
                <ul class="list-unstyled p-0">
                    <li>
                        <a class="text-dark" href="{{url('/')}}">HOME</a>
                    </li>
                    <li>
                        <a class="text-dark " href="{{route('contact')}}">CONTACT</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-6 my-lg-3">
                @if($front_footer_setting && $front_footer_setting->social_link_title)
                    <h4 class="fw-bold mb-3 mt-lg-5">{{$front_footer_setting->social_link_title}}</h4>
                @else
                    <h4 class="fw-bold mb-3 mt-lg-5">Social Links</h4>
                @endif
                <ul
                    class="footer_social_media d-flex align-items-center gap-3 p-0 list-unstyled mt-3">
                    <li class="footer_social_media_item footer_social_media_item_fb">
                        <a href="{{ $front_footer_setting->facebook_link ?? '#'}}" target="_blank">
                            <div class="footer_social_media_link flex items-center">
                                <i class="fa-brands fa-facebook-f footer_social_media_link_icone"></i>
                            </div>
                        </a>
                    </li>
                    <li
                        class="footer_social_media_item footer_social_media_item_twt">
                        <a href="{{ $front_footer_setting->youtube_link ?? '#'}}" target="_blank">
                            <div class="footer_social_media_link flex items-center">
                                <i class="fa-brands fa-youtube footer_social_media_link_icone"></i>
                            </div>
                        </a></li>
                    <li
                        class="footer_social_media_item footer_social_media_item_inst">
                        @if($front_footer_setting && $front_footer_setting->instagram_link)
                            <a href="{{$front_footer_setting->instagram_link}}" target="_blank">
                                <div class="footer_social_media_link flex items-center">
                                    <i class="fa-brands fa-instagram footer_social_media_link_icone"></i>
                                </div>
                            </a>
                        @else
                            <a href="#">
                                <div class="footer_social_media_link flex items-center">
                                    <i class="fa-brands fa-instagram footer_social_media_link_icone"></i>
                                </div>
                            </a>
                        @endif

                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container text-end py-3 px-lg-0 px-3">
        @php
            $legal_pages = \App\Models\LegalPage::all();
        @endphp
        @foreach($legal_pages as $key => $legal_page)
            <a href="{{route('page',$legal_page)}}" class="text-dark " @if( $key > 0) style="margin-left: 10px" @endif>{{$legal_page->name}} </a>
        @endforeach
    </div>
</footer>
