<!-- phone navbar -->
<div class="address_info d-lg-block d-none py-lg-3 py-2">
    <div class="container d-flex justify-content-end">
        <div
            class="col-md-4 col-12 d-flex justify-content-lg-end justify-content-between gap-3">
            <div class="phone-box d-flex gap-2 align-items-center">
                <img src="{{asset('assets/frontend/icons/phone.png')}}" class="img-fluid"
                     alt="phone">
                <div>
                    @if($front_footer_setting && $front_footer_setting->phone_title)
                        <p class="mb-0 fw-bold">{{$front_footer_setting->phone_title}}: </p>
                    @else
                        <p class="mb-0 fw-bold">Phone: </p>
                    @endif

                        @if($front_footer_setting && $front_footer_setting->phone)
                            <a href="tel:{{$front_footer_setting->phone}}"
                               class="d-block text-dark">
                                {{$front_footer_setting->phone}}
                            </a>
                        @else
                            <a href="tel:+216 72 273 123"
                               class="d-block text-dark">
                                +216 72 273 123 / 72 273 133
                            </a>
                        @endif
                </div>
            </div>
            <div class="phone-box d-flex gap-2 align-items-center">
                <img src="{{asset('assets/frontend/icons/address.png')}}" class="img-fluid"
                     alt="address">
                <div>

                    @if($front_footer_setting && $front_footer_setting->address_title)
                        <p class="mb-0 fw-bold">{{$front_footer_setting->address_title}}: </p>
                    @else
                        <p class="mb-0 fw-bold">Location: </p>
                    @endif

                        @if($front_footer_setting && $front_footer_setting->address)
                            <a href="#" class="d-block text-dark">
                                {{$front_footer_setting->address}}
                            </a>
                        @else
                            <a href="#" class="d-block text-dark">
                                ur location street here
                            </a>
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
