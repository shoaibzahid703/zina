<!-- red navbar -->
<div class="red_logo_bar bg-red d-lg-block d-none">
    <div class="container">
        <div
            class="red_bar d-flex justify-content-between align-items-center position-relative">
            <!-- <button class="navbar-toggler" type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button> -->
            <i class="fa-solid fa-bars fs-5 text-white"></i>
            <img src="{{asset('assets/frontend/images/zina-logo.png')}}"
                 class="img-fluid center_logo" alt="zina-logo">
            <div class="searchbar text-uppercase text-white d-flex justify-content-end gap-3">
                <form method="post" action="{{route("change_language")}}">
                    @csrf
                    <select class="form-control change_language" name="selected_language" onchange="this.form.submit()">
                        @foreach($front_languages as $language)
                            @if(Session::get('selected_language') != null)
                                <option value="{{$language->id}}" @if(Session::get('selected_language') == $language->id) selected @endif>{{$language->name}}</option>
                            @else
                                <option value="{{$language->id}}" @if($language->is_default == 1) selected @endif>{{$language->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </form>

            </div>
        </div>
    </div>
</div>
