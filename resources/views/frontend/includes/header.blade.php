<header id="nav" class="d-lg-none d-block">
    <div class="contained">
        <div id="logo">
            <h1>
                <a href="#">
                    <img src="{{asset('assets/frontend/images/zina-logo.png')}}" class="img-fluid mobile_logo" alt="zina-logo">
                </a>
            </h1>
        </div>
        <div class="container_nav">
            <div
                id="hamburger"><span></span><span></span><span></span></div>
            <div id="close"><span></span></div>
            <div id="menuWrapper">
                <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link {{active_class('welcome')}}" aria-current="page"
                           href="{{route('welcome')}}">
                            @if($front_navbar && $front_navbar->welcome_menu_text  )
                                {{$front_navbar->welcome_menu_text  }}
                            @else
                                Welcome
                            @endif
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{active_class('zina_house')}}" href="{{route('zina_house')}}">
                            @if($front_navbar && $front_navbar->zina_house_menu_text  )
                                {{$front_navbar->zina_house_menu_text  }}
                            @else
                                ZINA HOUSE
                            @endif
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{active_class('home-page-products')}}" href="{{route('home-page-products')}}">
                            @if($front_navbar && $front_navbar->our_products_menu_text  )
                                {{$front_navbar->our_products_menu_text  }}
                            @else
                                Our Products
                            @endif
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{active_class('our_values')}}" aria-current="page"
                           href="{{route('our_values')}}">
                            @if($front_navbar && $front_navbar->our_values_menu_text  )
                                {{$front_navbar->our_values_menu_text  }}
                            @else
                                Our values
                            @endif
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{active_class('home-page-news')}}" aria-current="page"
                           href="{{route('home-page-news')}}">
                            @if($front_navbar && $front_navbar->news_menu_text  )
                                {{$front_navbar->news_menu_text  }}
                            @else
                                News
                            @endif
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{active_class('home-page-recipes')}}" aria-current="page"
                           href="{{route('home-page-recipes')}}">
                            @if($front_navbar && $front_navbar->recipes_menu_text  )
                                {{$front_navbar->recipes_menu_text  }}
                            @else
                                Recipes
                            @endif
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page"
                           href="{{route('contact')}}">

                            @if($front_navbar && $front_navbar->contact_menu_text  )
                                {{$front_navbar->contact_menu_text  }}
                            @else
                                Contact
                            @endif
                        </a>
                    </li>
                    <li>
                        <!-- phone navbar -->
                        <div class="address_info py-lg-3 py-2">
                            <div
                                class=" text-light-gray d-flex justify-content-end">
                                <div
                                    class="col-md-4 col-12 d-flex justify-content-lg-end justify-content-between gap-3">
                                    <div
                                        class="phone-box d-flex gap-2 align-items-center">
                                        <img
                                            src="{{asset('assets/frontend/icons/phone.png')}}"
                                            class="img-fluid"
                                            alt="phone">
                                        <div>
                                            <p
                                                class="mb-0 fw-bold">Phone:
                                            </p>
                                            <a
                                                href="tel:+88 669 658 6586"
                                                class="d-block text-light-gray">+88
                                                669 658 6586</a>
                                        </div>
                                    </div>
                                    <div
                                        class="phone-box d-flex gap-2 align-items-center">
                                        <img
                                            src="{{asset('assets/frontend/icons/address.png')}}"
                                            class="img-fluid"
                                            alt="address">
                                        <div>
                                            <p
                                                class="mb-0 fw-bold">Location:
                                            </p>
                                            <a href="#"
                                               class="d-block text-light-gray">ur
                                                location
                                                street here</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                </ul>

            </div>
        </div>
    </div>
</header>
