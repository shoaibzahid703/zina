<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    @if(isset($settings->site_description))
        <meta name="description" content="{{$settings->site_description}}">
    @endif
    @if(isset($settings->seo_tags))
        <meta name="keywords" content="{{$settings->seo_tags}}">
    @endif
    <!-- font aweasome -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <link rel="stylesheet" href="{{asset('assets/frontend/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/header.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/style.css')}}">

    <link rel="shortcut icon" href="{{asset('assets/frontend/images/favicon.png')}}" type="image/x-icon">
    @yield('css')
</head>
<body class="bg-body">
@include('frontend.includes.phone_navbar')
@include('frontend.includes.header')
@include('frontend.includes.red_navbar')
@include('frontend.includes.navbar')
@yield('content')

@include('frontend.includes.footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/js/all.min.js"></script>
<script src="{{asset('assets/frontend/js/jquery.js')}}"></script>
<script src="{{asset('assets/frontend/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/header.js')}}"></script>

@yield('js')


</body>
</html>
