<!-- footer @s -->
<div class="nk-footer">
    <div class="container-fluid">
        <div class="nk-footer-wrap">
            <div class="nk-footer-copyright">
                 @if(isset($settings->site_name))
                 © Copyright {{date('Y')}}
                 @endif
            </div>
        </div>
    </div>
</div>
<!-- footer @e -->
