<!-- sidebar @s -->
<div class="nk-sidebar nk-sidebar-fixed is-light " data-content="sidebarMenu">
    <div class="nk-sidebar-element nk-sidebar-head">
        <div class="nk-sidebar-brand">
            <a href="{{url('/')}}" class="logo-link nk-sidebar-logo">
                <img class="logo-dark logo-img" src="{{asset('assets/admin/images/zina-logo.png')}}"  alt="logo-dark">
            </a>
        </div>
        <div class="nk-menu-trigger me-n2">
            <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
            <a href="#" class="nk-nav-compact nk-quick-nav-icon d-none d-xl-inline-flex" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
        </div>
    </div><!-- .nk-sidebar-element -->
    <div class="nk-sidebar-element">
        <div class="nk-sidebar-content">
            <div class="nk-sidebar-menu" data-simplebar>
                <ul class="nk-menu">
                    <li class="nk-menu-heading">
                        <h6 class="overline-title text-primary-alt">Dashboard</h6>
                    </li><!-- .nk-menu-item -->
                    <li class="nk-menu-item">
                        <a href="{{route('admin_dashboard')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-dashboard-fill"></em></span>
                            <span class="nk-menu-text">Dashboard</span>
                        </a>
                    </li>

                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle" data-bs-original-title="" title="">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-home"></em>
                            </span>
                            <span class="nk-menu-text">Home Page Setting</span>
                        </a>
                        <ul class="nk-menu-sub" style="display: none;">
                            <li class="nk-menu-item">
                                <a href="{{route('home-slider')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Slider Section</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('home_page_zina_section')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Zina House Section</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('home_page_our_product')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Our Product Section</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('home_page_our_value')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Our Value Section</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('home_page_zina_products')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Zina Products Section</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('home_page_news')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">News Section</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('home_page_recipes')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Recipes Section</span>
                                </a>
                            </li>

                        </ul><!-- .nk-menu-sub -->
                    </li>
                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle" data-bs-original-title="" title="">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-layout"></em>
                            </span>
                            <span class="nk-menu-text">Zina House</span>
                        </a>
                        <ul class="nk-menu-sub" style="display: none;">
                            <li class="nk-menu-item">
                                <a href="{{route('zina_house_section_one')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Section One</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('zina_house_section_two')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Section Two</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('zina_house_slider')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text"> Section Three</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('zina_house_section_four')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Section Four</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('zina_house_section_five')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Section Five</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('zina_house_section_six')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Section Six</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('zina_house_section_seven')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Section Seven</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('zina_house_section_eight')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Section Eight</span>
                                </a>
                            </li>

                        </ul><!-- .nk-menu-sub -->
                    </li>
                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle" data-bs-original-title="" title="">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-layout"></em>
                            </span>
                            <span class="nk-menu-text">Our Value</span>
                        </a>
                        <ul class="nk-menu-sub" style="display: none;">
                            <li class="nk-menu-item">
                                <a href="{{route('our_value_section_one')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Section One</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('our_value_section_two')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Section Two</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('our_value_section_three')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text"> Section Three</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('our_value_section_four')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Section Four</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('our_value_section_five')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Section Five</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('our_value_section_six')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Section Six</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('our_value_section_seven')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Section Seven</span>
                                </a>
                            </li>

                        </ul><!-- .nk-menu-sub -->
                    </li>



                    <li class="nk-menu-item">
                        <a href="{{route('our-news')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-money"></em></span>
                            <span class="nk-menu-text">Our News</span>
                        </a>
                    </li>

                    <li class="nk-menu-item">
                        <a href="{{route('home-recipes')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-send-alt"></em></span>
                            <span class="nk-menu-text">Recipes</span>
                        </a>
                    </li>
                    <li class="nk-menu-item">
                        <a href="{{route('home-products')}}" class="nk-menu-link">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-back-arrow-fill"></em>
                           </span>
                            <span class="nk-menu-text">Products</span>
                        </a>
                    </li>

                    <li class="nk-menu-item">
                        <a href="{{route('home_page_news_section')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-arrow-down-round"></em></span>
                            <span class="nk-menu-text">News Page Setting</span>
                        </a>
                    </li>



                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle" data-bs-original-title="" title="">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-home"></em>
                            </span>
                            <span class="nk-menu-text">Recipe Page Setting</span>
                        </a>
                        <ul class="nk-menu-sub" style="display: none;">
                            <li class="nk-menu-item">
                                <a href="{{route('recipe_first_section')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">First Section</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('recipe_second_section')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Second Section</span>
                                </a>
                            </li>
                        </ul>
                    </li>


                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle" data-bs-original-title="" title="">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-setting-fill"></em>
                            </span>
                            <span class="nk-menu-text">Product Page Setting</span>
                        </a>
                        <ul class="nk-menu-sub" style="display: none;">
                            <li class="nk-menu-item">
                                <a href="{{route('product_page_first_section')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">First Section</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('product_page_second_section')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Second Section</span>
                                </a>
                            </li>
                        </ul>
                    </li>


                    <li class="nk-menu-item">
                        <a href="{{route('home_page_navbar')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-heading"></em></span>
                            <span class="nk-menu-text">Home Navbar</span>
                        </a>
                    </li>

                    <li class="nk-menu-item">
                        <a href="{{route('footer_setting')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-arrow-down-c"></em></span>
                            <span class="nk-menu-text">Footer Setting</span>
                        </a>
                    </li>

                    <li class="nk-menu-item">
                        <a href="{{route('contact-queries')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-repeat-fill"></em></span>
                            <span class="nk-menu-text">Contact Queries</span>
                        </a>
                    </li>

                    <li class="nk-menu-item">
                        <a href="{{route('legal_pages')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-text"></em></span>
                            <span class="nk-menu-text">Legal Pages</span>
                        </a>
                    </li>

                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle" data-bs-original-title="" title="">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-setting-alt"></em>
                            </span>
                            <span class="nk-menu-text">Settings</span>
                        </a>
                        <ul class="nk-menu-sub" style="display: none;">
                            <li class="nk-menu-item">
                                <a href="{{route('site_settings')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Site Setting</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('languages')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Languages</span>
                                </a>
                            </li>

                        </ul><!-- .nk-menu-sub -->
                    </li>



                </ul><!-- .nk-menu -->
            </div><!-- .nk-sidebar-menu -->
        </div><!-- .nk-sidebar-content -->
    </div><!-- .nk-sidebar-element -->
</div>
<!-- sidebar @e -->
