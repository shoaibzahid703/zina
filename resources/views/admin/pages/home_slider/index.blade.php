@extends('admin.layouts.app')
@section('title','Home Slider')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Home Slider</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">

                    <div class="card-inner">
                        <a href="{{route('home_slider_create')}}"  class="btn btn-primary float-end">
                            <em class="icon ni ni-plus"></em>
                            <span>Add Slider</span>
                        </a>
                        <table class="table datatable-init" >
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Heading</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($all_sliders as $slider)
                                <tr>
                                    <td>
                                        {{$slider->title}}
                                    </td>
                                    <td>
                                        {{$slider->heading}}
                                    </td>
                                    <td>
                                        <img src="{{asset('storage/slider/'.$slider->slider_image)}}" class="img-fluid" width="80" height="80">
                                    </td>


                                    <td>

                                        <a href="{{route("home_slider_edit",$slider)}}" class="btn btn-primary btn-sm"
                                           data-toggle="tooltip" data-placement="top" title="edit language">
                                         <span class="nk-menu-icon text-white">
                                              <em class="icon ni ni-edit"></em>
                                         </span>
                                        </a>

                                        <a href="{{route("home_slider_delete",$slider)}}" class="btn btn-danger btn-sm"
                                           data-toggle="tooltip" data-placement="top" title="delete language">
                                         <span class="nk-menu-icon text-white">
                                              <em class="icon ni ni-trash-empty"></em>
                                         </span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

