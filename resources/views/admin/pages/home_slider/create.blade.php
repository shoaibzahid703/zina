@extends('admin.layouts.app')
@section('title','Add Slider')
@section('content')

    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Add Slider</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('home_slider_store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                               <div class="row">
                                                   <div class="col-md-12">
                                                       <div class="form-group">
                                                           <label for="title">
                                                               Title
                                                               ({{ strtoupper($language->name) }})
                                                               <span class="text-info">*</span>
                                                           </label>
                                                           <input id="title"
                                                                  name="{{ $language->code }}[title]"
                                                                  type="text"
                                                                  class="form-control @error($language->code.'.title') is-invalid @enderror"
                                                                  placeholder="title"
                                                                  value="{{ old($language->code.'.title')  }}"/>
                                                           @error($language->code.'.title')
                                                           <div
                                                               class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                           @enderror
                                                       </div>
                                                   </div>
                                                   <div class="col-md-12">
                                                       <div class="form-group">
                                                           <label for="heading">
                                                               Heading
                                                               ({{ strtoupper($language->name) }})
                                                               <span class="text-info">*</span>
                                                           </label>
                                                           <input id="heading"
                                                                  name="{{ $language->code }}[heading]"
                                                                  type="text"
                                                                  class="form-control @error($language->code.'.heading') is-invalid @enderror"
                                                                  placeholder="heading"
                                                                  value="{{ old($language->code.'.heading')  }}"/>
                                                           @error($language->code.'.heading')
                                                           <div
                                                               class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                           @enderror
                                                       </div>
                                                   </div>
                                                   <div class="col-md-12">
                                                       <div class="form-group">
                                                           <label for="description">
                                                               Description
                                                               ({{ strtoupper($language->name) }})
                                                               <span class="text-info">*</span>
                                                           </label>

                                                           <textarea id="description"
                                                                     name="{{ $language->code }}[description]"
                                                                     class="form-control @error($language->code.'.description') is-invalid @enderror"
                                                                     placeholder="description">{{ old($language->code.'.description')  }}</textarea>

                                                           @error($language->code.'.description')
                                                           <div
                                                               class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                           @enderror
                                                       </div>
                                                   </div>
                                                   <div class="col-md-6">
                                                       <div class="form-group">
                                                           <label for="button_text">
                                                               Button Text
                                                               ({{ strtoupper($language->name) }})
                                                               <span class="text-info">*</span>
                                                           </label>
                                                           <input id="button_text"
                                                                  name="{{ $language->code }}[button_text]"
                                                                  type="text"
                                                                  class="form-control @error($language->code.'.button_text') is-invalid @enderror"
                                                                  placeholder="button_text"
                                                                  value="{{ old($language->code.'.button_text')  }}"/>
                                                           @error($language->code.'.button_text')
                                                           <div
                                                               class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                           @enderror
                                                       </div>
                                                   </div>
                                                   <div class="col-md-6">
                                                       <div class="form-group">
                                                           <label for="button_link">
                                                               Button Link
                                                               ({{ strtoupper($language->name) }})
                                                               <span class="text-info">*</span>
                                                           </label>
                                                           <input id="button_link"
                                                                  name="{{ $language->code }}[button_link]"
                                                                  type="text"
                                                                  class="form-control @error($language->code.'.button_link') is-invalid @enderror"
                                                                  placeholder="button_link"
                                                                  value="{{ old($language->code.'.button_link')  }}"/>
                                                           @error($language->code.'.button_link')
                                                           <div
                                                               class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                           @enderror
                                                       </div>
                                                   </div>
                                               </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="slider_image">Slider Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="slider_image"
                                               class="form-control-file @error('slider_image') is-invalid @enderror"
                                               id="slider_image" accept="image/*">
                                        @error('slider_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary float-right">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')

@endpush
