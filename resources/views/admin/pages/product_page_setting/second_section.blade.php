@extends('admin.layouts.app')
@section('title','Product Settings Second section')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"> Product Settings Second section</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('product_page_second_section_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--zina houser section--}}
                                                
                                                <div class="row mt-2">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="second_section_first_heading">
                                                                First Heading
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="second_section_first_heading"
                                                                   name="{{ $language->code }}[second_section_first_heading]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.second_section_first_heading') is-invalid @enderror"
                                                                   placeholder="First Heading"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.second_section_first_heading') ? old($language->code.'.second_section_first_heading') :  $home_page_setting->translate($language->code)->second_section_first_heading  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.second_section_first_heading')  }}"/>
                                                            @endif

                                                            @error($language->code.'.second_section_first_heading')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="second_section_sec_heading">
                                                                Second Heading
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="second_section_sec_heading"
                                                                   name="{{ $language->code }}[second_section_sec_heading]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.second_section_sec_heading') is-invalid @enderror"
                                                                   placeholder="Second Heading"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.second_section_sec_heading') ? old($language->code.'.second_section_sec_heading') :  $home_page_setting->translate($language->code)->second_section_sec_heading   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.second_section_sec_heading')  }}"/>
                                                            @endif
                                                            @error($language->code.'.second_section_sec_heading')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>


                                                    <div class="col-nd-12">
                                                        <div class="form-group">
                                                            <label for="second_section_paragraph">
                                                                Paragraph
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <textarea id="second_section_paragraph"
                                                                      name="{{ $language->code }}[second_section_paragraph]"
                                                                      class="form-control @error($language->code.'.second_section_paragraph') is-invalid @enderror"
                                                                      placeholder="Paragraph"
                                                            >@if($home_page_setting) {{ old($language->code.'.second_section_paragraph') ? old($language->code.'.second_section_paragraph') :  $home_page_setting->translate($language->code)->second_section_paragraph}}@else{{ old($language->code.'.second_section_paragraph')}}@endif</textarea>

                                                            @error($language->code.'.second_section_paragraph')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                
                                <div class="col-12">
                                    <label class="form-label" for="fourth_section_image">Side Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="second_section_image"
                                               class="form-control-file @error('second_section_image') is-invalid @enderror"
                                               id="second_section_image" accept="image/*">
                                        @error('second_section_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

