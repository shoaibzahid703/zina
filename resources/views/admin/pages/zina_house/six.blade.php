@extends('admin.layouts.app')
@section('title','Zina House Section Six Settings')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Zina House Section Six Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('zina_house_section_six_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                     {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--zina houser section--}}
                                                <div class="row">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_6_text">
                                                                    Section Six Text
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_6_text"
                                                                       name="{{ $language->code }}[section_6_text]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_6_text') is-invalid @enderror"
                                                                       placeholder="section six text"
                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_6_text') ? old($language->code.'.section_6_text') :  $zina_house_setting->translate($language->code)->section_6_text  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_6_text')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_6_text')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_6_heading">
                                                                    Section Six Heading
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_6_heading"
                                                                       name="{{ $language->code }}[section_6_heading]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_6_heading') is-invalid @enderror"
                                                                       placeholder="section six heading"

                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_6_heading') ? old($language->code.'.section_6_heading') :  $zina_house_setting->translate($language->code)->section_6_heading  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_6_heading')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_6_heading')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_6_bottom_text">
                                                                    Section Six Bottom Text
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_6_bottom_text"
                                                                       name="{{ $language->code }}[section_6_bottom_text]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_6_bottom_text') is-invalid @enderror"
                                                                       placeholder="section six bottom text"

                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_6_bottom_text') ? old($language->code.'.section_6_bottom_text') :  $zina_house_setting->translate($language->code)->section_6_bottom_text  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_6_bottom_text')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_6_bottom_text')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_6_results_text">
                                                                    Section Six Result Text
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_6_results_text"
                                                                       name="{{ $language->code }}[section_6_results_text]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_6_results_text') is-invalid @enderror"
                                                                       placeholder="section six results text"

                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_6_results_text') ? old($language->code.'.section_6_results_text') :  $zina_house_setting->translate($language->code)->section_6_results_text  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_6_results_text')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_6_results_text')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_6_drop_heading">
                                                                    Section Six Drop Heading
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_6_drop_heading"
                                                                       name="{{ $language->code }}[section_6_drop_heading]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_6_drop_heading') is-invalid @enderror"
                                                                       placeholder="section six drop heading"

                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_6_drop_heading') ? old($language->code.'.section_6_drop_heading') :  $zina_house_setting->translate($language->code)->section_6_drop_heading  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_6_drop_heading')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_6_drop_heading')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_6_drop_paragraph">
                                                                    Section Six Drop Paragraph
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_6_drop_paragraph"
                                                                       name="{{ $language->code }}[section_6_drop_paragraph]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_6_drop_paragraph') is-invalid @enderror"
                                                                       placeholder="section six drop paragraph"

                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_6_drop_paragraph') ? old($language->code.'.section_6_drop_paragraph') :  $zina_house_setting->translate($language->code)->section_6_drop_paragraph  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_6_drop_paragraph')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_6_drop_paragraph')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_6_co2_heading">
                                                                    Section Six Co2 Heading
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_6_co2_heading"
                                                                       name="{{ $language->code }}[section_6_co2_heading]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_6_co2_heading') is-invalid @enderror"
                                                                       placeholder="section six co2 heading"

                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_6_co2_heading') ? old($language->code.'.section_6_co2_heading') :  $zina_house_setting->translate($language->code)->section_6_co2_heading  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_6_co2_heading')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_6_co2_heading')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_6_co2_paragraph">
                                                                    Section Six Co2 Paragraph
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_6_co2_paragraph"
                                                                       name="{{ $language->code }}[section_6_co2_paragraph]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_6_co2_paragraph') is-invalid @enderror"
                                                                       placeholder="section six co2 paragraph"

                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_6_co2_paragraph') ? old($language->code.'.section_6_co2_paragraph') :  $zina_house_setting->translate($language->code)->section_6_co2_paragraph  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_6_co2_paragraph')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_6_co2_paragraph')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>



                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="section_6_paragraph">
                                                                    Section Six Paragraph
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <textarea id="section_6_paragraph"
                                                                          name="{{ $language->code }}[section_6_paragraph]"
                                                                          class="form-control @error($language->code.'.section_6_paragraph') is-invalid @enderror"
                                                                          placeholder="section six paragraph">@if($zina_house_setting){{ old($language->code.'.section_6_paragraph') ? old($language->code.'.section_6_paragraph') : $zina_house_setting->translate($language->code)->section_6_paragraph  ?? ''}} @else {{ old($language->code.'.section_6_paragraph')  }} @endif</textarea>

                                                                @error($language->code.'.section_6_paragraph')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

