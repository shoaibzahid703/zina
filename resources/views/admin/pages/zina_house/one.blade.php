@extends('admin.layouts.app')
@section('title','Zina House Section One Settings')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Zina House Section One Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('zina_house_section_one_update_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="section_1_image">Section One Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="section_1_image"
                                               class="form-control-file @error('section_1_image') is-invalid @enderror"
                                               id="section_1_image" accept="image/*">
                                        @error('section_1_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

