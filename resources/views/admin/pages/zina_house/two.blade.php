@extends('admin.layouts.app')
@section('title','Zina House Section Two Settings')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Zina House Section Two Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('zina_house_section_two_update_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                     {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--zina houser section--}}
                                                <div class="row">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_2_heading">
                                                                    Section Two Heading
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_2_heading"
                                                                       name="{{ $language->code }}[section_2_heading]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_2_heading') is-invalid @enderror"
                                                                       placeholder="section two heading"

                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_2_heading') ? old($language->code.'.section_2_heading') :  $zina_house_setting->translate($language->code)->section_2_heading  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_2_heading')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_2_heading')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_2_text">
                                                                    Section Two Text
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_2_text"
                                                                       name="{{ $language->code }}[section_2_text]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_2_text') is-invalid @enderror"
                                                                       placeholder="section two text"

                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_2_text') ? old($language->code.'.section_2_text') :  $zina_house_setting->translate($language->code)->section_2_text  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_2_text')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_2_text')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_2_bottom_text">
                                                                    Section Two Bottom Text
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_2_bottom_text"
                                                                       name="{{ $language->code }}[section_2_bottom_text]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_2_bottom_text') is-invalid @enderror"
                                                                       placeholder="section two bottom text"

                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_2_bottom_text') ? old($language->code.'.section_2_bottom_text') :  $zina_house_setting->translate($language->code)->section_2_bottom_text  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_2_bottom_text')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_2_bottom_text')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="section_2_paragraph">
                                                                    Section Two Paragraph
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <textarea id="section_2_paragraph"
                                                                          name="{{ $language->code }}[section_2_paragraph]"
                                                                          class="form-control @error($language->code.'.section_2_paragraph') is-invalid @enderror"
                                                                          placeholder="section two paragraph">@if($zina_house_setting){{ old($language->code.'.section_2_paragraph') ? old($language->code.'.section_2_paragraph') : $zina_house_setting->translate($language->code)->section_2_paragraph  ?? ''}} @else {{ old($language->code.'.section_2_paragraph')  }} @endif</textarea>

                                                                @error($language->code.'.section_2_paragraph')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="section_2_bg_image">Section Two Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="section_2_bg_image"
                                               class="form-control-file @error('section_2_bg_image') is-invalid @enderror"
                                               id="section_2_bg_image" accept="image/*">
                                        @error('section_2_bg_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

