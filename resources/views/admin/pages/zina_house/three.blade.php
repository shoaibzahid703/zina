@extends('admin.layouts.app')
@section('title','Zina House Section Three Settings')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Zina House Section Three Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('zina_house_section_three_update_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                     {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--zina houser section--}}
                                                <div class="row">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="section_3_heading">
                                                                    Section Three Heading
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_3_heading"
                                                                       name="{{ $language->code }}[section_3_heading]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_3_heading') is-invalid @enderror"
                                                                       placeholder="section three heading"

                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_3_heading') ? old($language->code.'.section_3_heading') :  $zina_house_setting->translate($language->code)->section_3_heading  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_3_heading')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_3_heading')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="section_3_paragraph">
                                                                    Section Three Paragraph
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <textarea id="section_3_paragraph"
                                                                          name="{{ $language->code }}[section_3_paragraph]"
                                                                          class="form-control @error($language->code.'.section_3_paragraph') is-invalid @enderror"
                                                                          placeholder="section three paragraph">@if($zina_house_setting){{ old($language->code.'.section_3_paragraph') ? old($language->code.'.section_3_paragraph') : $zina_house_setting->translate($language->code)->section_3_paragraph  ?? ''}} @else {{ old($language->code.'.section_3_paragraph')  }} @endif</textarea>

                                                                @error($language->code.'.section_3_paragraph')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="section_3_image">Section Three Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="section_3_image"
                                               class="form-control-file @error('section_3_image') is-invalid @enderror"
                                               id="section_3_image" accept="image/*">
                                        @error('section_3_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

