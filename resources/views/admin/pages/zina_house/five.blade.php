@extends('admin.layouts.app')
@section('title','Zina House Section Five Settings')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Zina House Section Five Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('zina_house_section_five_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                     {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--zina houser section--}}
                                                <div class="row">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_5_heading">
                                                                    Section Five Top Heading
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_5_heading"
                                                                       name="{{ $language->code }}[section_5_heading]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_5_heading') is-invalid @enderror"
                                                                       placeholder="section five heading"
                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_5_heading') ? old($language->code.'.section_5_heading') :  $zina_house_setting->translate($language->code)->section_5_heading  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_5_heading')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_5_heading')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_5_ceo_name">
                                                                    Section Five Ceo Name
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_5_ceo_name"
                                                                       name="{{ $language->code }}[section_5_ceo_name]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_5_ceo_name') is-invalid @enderror"
                                                                       placeholder="section five ceo name"

                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_5_ceo_name') ? old($language->code.'.section_5_ceo_name') :  $zina_house_setting->translate($language->code)->section_5_ceo_name  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_5_ceo_name')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_5_ceo_name')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_5_bottom_text">
                                                                    Section Five Bottom Text
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_5_bottom_text"
                                                                       name="{{ $language->code }}[section_5_bottom_text]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_5_bottom_text') is-invalid @enderror"
                                                                       placeholder="section five bottom text"

                                                                       @if($zina_house_setting)
                                                                           value="{{ old($language->code.'.section_5_bottom_text') ? old($language->code.'.section_5_bottom_text') :  $zina_house_setting->translate($language->code)->section_5_bottom_text  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_5_bottom_text')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_5_ceo_name')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="section_5_paragraph">
                                                                    Section Five Paragraph
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <textarea id="section_5_paragraph"
                                                                          name="{{ $language->code }}[section_5_paragraph]"
                                                                          class="form-control @error($language->code.'.section_5_paragraph') is-invalid @enderror"
                                                                          placeholder="section five paragraph">@if($zina_house_setting){{ old($language->code.'.section_5_paragraph') ? old($language->code.'.section_5_paragraph') : $zina_house_setting->translate($language->code)->section_5_paragraph  ?? ''}} @else {{ old($language->code.'.section_5_paragraph')  }} @endif</textarea>

                                                                @error($language->code.'.section_5_paragraph')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

