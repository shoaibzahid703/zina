@extends('admin.layouts.app')
@section('title','Edit Slider')
@section('content')

    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Edit Slider</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('zina_house_slider_update',$zina_house_slider)}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="slider_title">
                                                                Slider Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="slider_title"
                                                                   name="{{ $language->code }}[slider_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.slider_title') is-invalid @enderror"
                                                                   placeholder="slider title"
                                                                   @if($zina_house_slider)
                                                                       value="{{ old($language->code.'.slider_title') ? old($language->code.'.slider_title') :  $zina_house_slider->translate($language->code)->slider_title  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.slider_title')  }}"  />
                                                            @endif
                                                            @error($language->code.'.slider_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="slider_description">
                                                                Description
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <textarea id="slider_description"
                                                                      name="{{ $language->code }}[slider_description]"
                                                                      class="form-control @error($language->code.'.slider_description') is-invalid @enderror"
                                                                      placeholder="slider_description">@if($zina_house_slider)  {{ old($language->code.'.slider_description') ? old($language->code.'.slider_description') :  $zina_house_slider->translate($language->code)->slider_description  ?? '' }} @else value="{{ old($language->code.'.slider_title')  }}"  />@endif</textarea>

                                                            @error($language->code.'.slider_description')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="slider_image">Slider Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="slider_image"
                                               class="form-control-file @error('slider_image') is-invalid @enderror"
                                               id="slider_image" accept="image/*">
                                        @error('slider_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>


                                <div class="col-6">
                                    <label class="form-label" for="slider_year">Slider Year</label>
                                    <div class="form-control-wrap">
                                        <input type="text" name="slider_year"
                                               class="form-control @error('slider_year') is-invalid @enderror"
                                               id="slider_year"
                                               placeholder="slider year"
                                               @if($zina_house_slider)
                                                   value="{{$zina_house_slider->slider_year }}"/>
                                        @else
                                            value="{{ old($language->code.'.slider_year')  }}"  />
                                        @endif
                                        @error('slider_year')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary float-right">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')

@endpush
