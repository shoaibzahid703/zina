@extends('admin.layouts.app')
@section('title','Recipes Settings First section')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"> Recipes Settings First section</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('recipe_first_section_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--zina houser section--}}

                                                <div class="row mt-2">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="second_section_top_text_one">
                                                                Top Text Fist
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="second_section_top_text_one"
                                                                   name="{{ $language->code }}[second_section_top_text_one]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.second_section_top_text_one') is-invalid @enderror"
                                                                   placeholder="Top Text Fist"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.second_section_top_text_one') ? old($language->code.'.second_section_top_text_one') :  $home_page_setting->translate($language->code)->second_section_top_text_one  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.second_section_top_text_one')  }}"/>
                                                            @endif

                                                            @error($language->code.'.second_section_top_text_one')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="second_section_top_text_two">
                                                                Top Text second
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="second_section_top_text_two"
                                                                   name="{{ $language->code }}[second_section_top_text_two]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.second_section_top_text_two') is-invalid @enderror"
                                                                   placeholder="Top Text second"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.second_section_top_text_two') ? old($language->code.'.second_section_top_text_two') :  $home_page_setting->translate($language->code)->second_section_top_text_two   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.second_section_top_text_two')  }}"/>
                                                            @endif
                                                            @error($language->code.'.second_section_top_text_two')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-nd-6">
                                                        <div class="form-group">
                                                            <label for="second_section_top_heading">
                                                                Top Heading
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="second_section_top_heading"
                                                                   name="{{ $language->code }}[second_section_top_heading]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.second_section_top_heading') is-invalid @enderror"
                                                                   placeholder="Top Heading"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.second_section_top_heading') ? old($language->code.'.second_section_top_heading') :  $home_page_setting->translate($language->code)->second_section_top_heading   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.second_section_top_heading')  }}"/>
                                                            @endif
                                                            @error($language->code.'.second_section_top_heading')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                   <div class="col-nd-6">
                                                        <div class="form-group">
                                                            <label for="second_section_paragraph">
                                                              Paragraph
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="second_section_paragraph"
                                                                   name="{{ $language->code }}[second_section_paragraph]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.second_section_paragraph') is-invalid @enderror"
                                                                   placeholder="Top Heading"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.second_section_paragraph') ? old($language->code.'.second_section_paragraph') :  $home_page_setting->translate($language->code)->second_section_paragraph   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.second_section_paragraph')  }}"/>
                                                            @endif
                                                            @error($language->code.'.second_section_paragraph')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="second_section_bottom_heading">
                                                                Bottom Heading
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="second_section_bottom_heading"
                                                                   name="{{ $language->code }}[second_section_bottom_heading]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.second_section_bottom_heading') is-invalid @enderror"
                                                                   placeholder="Bottom Heading"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.second_section_bottom_heading') ? old($language->code.'.second_section_bottom_heading') :  $home_page_setting->translate($language->code)->second_section_bottom_heading   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.second_section_bottom_heading')  }}"/>
                                                            @endif
                                                            @error($language->code.'.second_section_bottom_heading')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">

                            	<div class="col-6">
                                    <label class="form-label" for="first_section_top_video">Top Video</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="first_section_top_video"
                                           class="form-control-file @error('first_section_top_video') is-invalid @enderror"
                                          id="first_section_top_video" accept="video/*">
                                        @error('first_section_top_video')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-6">
                                    <label class="form-label" for="second_section_image">Side Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="second_section_image"
                                               class="form-control-file @error('second_section_image') is-invalid @enderror"
                                               id="second_section_image" accept="image/*">
                                        @error('second_section_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>




                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

