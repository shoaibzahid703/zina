@extends('admin.layouts.app')
@section('title','Recipes Settings Second section')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"> Recipes Settings Second section</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('recipe_second_section_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--zina houser section--}}
                                                
                                                <div class="row mt-2">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="third_section_search_heading">
                                                                Search Heading
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="third_section_search_heading"
                                                                   name="{{ $language->code }}[third_section_search_heading]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.third_section_search_heading') is-invalid @enderror"
                                                                   placeholder="Search Heading"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.third_section_search_heading') ? old($language->code.'.third_section_search_heading') :  $home_page_setting->translate($language->code)->third_section_search_heading  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.third_section_search_heading')  }}"/>
                                                            @endif

                                                            @error($language->code.'.third_section_search_heading')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="fourth_section_heading_first">
                                                                Heading First
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="fourth_section_heading_first"
                                                                   name="{{ $language->code }}[fourth_section_heading_first]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.fourth_section_heading_first') is-invalid @enderror"
                                                                   placeholder="Heading First"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.fourth_section_heading_first') ? old($language->code.'.fourth_section_heading_first') :  $home_page_setting->translate($language->code)->fourth_section_heading_first   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.fourth_section_heading_first')  }}"/>
                                                            @endif
                                                            @error($language->code.'.fourth_section_heading_first')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-nd-6">
                                                        <div class="form-group">
                                                            <label for="fourth_section_heading_sec">
                                                                Heading Second
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="fourth_section_heading_sec"
                                                                   name="{{ $language->code }}[fourth_section_heading_sec]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.fourth_section_heading_sec') is-invalid @enderror"
                                                                   placeholder="Heading Second"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.fourth_section_heading_sec') ? old($language->code.'.fourth_section_heading_sec') :  $home_page_setting->translate($language->code)->fourth_section_heading_sec   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.fourth_section_heading_sec')  }}"/>
                                                            @endif
                                                            @error($language->code.'.fourth_section_heading_sec')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="fourth_section_make_time">
                                                                Make Time
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="fourth_section_make_time"
                                                                   name="{{ $language->code }}[fourth_section_make_time]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.fourth_section_make_time') is-invalid @enderror"
                                                                   placeholder="Make Time"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.fourth_section_make_time') ? old($language->code.'.fourth_section_make_time') :  $home_page_setting->translate($language->code)->fourth_section_make_time   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.fourth_section_make_time')  }}"/>
                                                            @endif
                                                            @error($language->code.'.fourth_section_make_time')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="fourth_section_make_way">
                                                                Make Way
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="fourth_section_make_way"
                                                                   name="{{ $language->code }}[fourth_section_make_way]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.fourth_section_make_way') is-invalid @enderror"
                                                                   placeholder="Make Way"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.fourth_section_make_way') ? old($language->code.'.fourth_section_make_way') :  $home_page_setting->translate($language->code)->fourth_section_make_way   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.fourth_section_make_way')  }}"/>
                                                            @endif
                                                            @error($language->code.'.fourth_section_make_way')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="fourth_section_learn_more_text">
                                                                Learn More Botton Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="fourth_section_learn_more_text"
                                                                   name="{{ $language->code }}[fourth_section_learn_more_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.fourth_section_learn_more_text') is-invalid @enderror"
                                                                   placeholder="Make Way"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.fourth_section_learn_more_text') ? old($language->code.'.fourth_section_learn_more_text') :  $home_page_setting->translate($language->code)->fourth_section_learn_more_text   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.fourth_section_learn_more_text')  }}"/>
                                                            @endif
                                                            @error($language->code.'.fourth_section_learn_more_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                
                                <div class="col-12">
                                    <label class="form-label" for="fourth_section_image">Side Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="fourth_section_image"
                                               class="form-control-file @error('fourth_section_image') is-invalid @enderror"
                                               id="fourth_section_image" accept="image/*">
                                        @error('fourth_section_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>


                               
                 
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

