@extends('admin.layouts.app')
@section('title','News Page Settings')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">  News Page Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('home_page_news_section_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--zina houser section--}}
                                                <div class="row">
                                                    <div class="col-nd-6">
                                                        <div class="form-group">
                                                            <label for="top_welcome_text">
                                                                New Top Welcome Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="top_welcome_text"
                                                                   name="{{ $language->code }}[top_welcome_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.top_welcome_text') is-invalid @enderror"
                                                                   placeholder="New Top Welcome Text"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.top_welcome_text') ? old($language->code.'.top_welcome_text') :  $home_page_setting->translate($language->code)->top_welcome_text  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.top_welcome_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.top_welcome_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-nd-6">
                                                        <div class="form-group">
                                                            <label for="top_news_text">
                                                                New Top News Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="top_news_text"
                                                                   name="{{ $language->code }}[top_news_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.top_news_text') is-invalid @enderror"
                                                                   placeholder="New Top News Text"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.top_news_text') ? old($language->code.'.top_news_text') :  $home_page_setting->translate($language->code)->top_news_text  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.top_news_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.top_news_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-nd-12">
                                                        <div class="form-group">
                                                            <label for="news_heading">
                                                                News Heading
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="news_heading"
                                                                   name="{{ $language->code }}[news_heading]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.news_heading') is-invalid @enderror"
                                                                   placeholder="News Heading"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.news_heading  ') ? old($language->code.'.news_heading') :  $home_page_setting->translate($language->code)->news_heading   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.news_heading')  }}"/>
                                                            @endif

                                                            @error($language->code.'.news_heading')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="news_bg_image">News Background Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="news_bg_image"
                                               class="form-control-file @error('news_bg_image') is-invalid @enderror"
                                               id="news_bg_image" accept="image/*">
                                        @error('news_bg_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

