@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Dashboard</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="nk-block">
                    <div class="row g-gs">
                        <div class="col-xxl-3 col-sm-4">
                            <a href="{{route('home-recipes')}}">
                            <div class="card">
                                <div class="nk-ecwg nk-ecwg6">
                                    <div class="card-inner">
                                        <div class="card-title-group">
                                            <div class="card-title">
                                                <h6 class="title">Total Recipes</h6>
                                            </div>
                                        </div>
                                        <div class="data">
                                            <div class="data-group">
                                                <div class="amount">{{$total_recipes}}</div>
                                                <div class="nk-ecwg6-ck">
                                                       <span class="nk-menu-icon">
                                                            <em class="icon ni ni-send-alt text-danger" style="font-size: 50px;"></em>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- .card-inner -->
                                </div><!-- .nk-ecwg -->
                            </div><!-- .card -->
                        </a>
                        </div><!-- .col -->
                        <div class="col-xxl-3 col-sm-4">
                            <a href="{{route('our-news')}}">
                            <div class="card">
                                <div class="nk-ecwg nk-ecwg6">
                                    <div class="card-inner">
                                        <div class="card-title-group">
                                            <div class="card-title">
                                                <h6 class="title">Total News</h6>
                                            </div>
                                        </div>
                                        <div class="data">
                                            <div class="data-group">
                                                <div class="amount">{{$total_news}}</div>
                                                <div class="nk-ecwg6-ck">
                                                       <span class="nk-menu-icon">
                                                            <em class="icon ni icon ni ni-money text-success" style="font-size: 50px;"></em>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- .card-inner -->
                                </div><!-- .nk-ecwg -->
                            </div><!-- .card -->
                        </a>
                        </div><!-- .col -->
                        <div class="col-xxl-3 col-sm-4">
                            <a href="{{route('home-products')}}">
                            <div class="card">
                                <div class="nk-ecwg nk-ecwg6">
                                    <div class="card-inner">
                                        <div class="card-title-group">
                                            <div class="card-title">
                                                <h6 class="title">Total Products</h6>
                                            </div>
                                        </div>
                                        <div class="data">
                                            <div class="data-group">
                                                <div class="amount">{{$total_products}}</div>
                                                <div class="nk-ecwg6-ck">
                                                       <span class="nk-menu-icon">
                                                            <em class="icon  ni ni-back-arrow-fill text-primary" style="font-size: 50px;"></em>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- .card-inner -->
                                </div><!-- .nk-ecwg -->
                            </div><!-- .card -->
                        </a>
                        </div><!-- .col -->

                    </div><!-- .row -->
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
@endpush

