@extends('admin.layouts.app')
@section('title','Languages')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Languages</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">

                    <div class="card-inner">
                        <a href="{{route('language_create')}}"  class="btn btn-primary float-end">
                            <em class="icon ni ni-plus"></em>
                            <span>Add Language</span>
                        </a>
                        <table class="table datatable-init" >
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Status</th>
                                <th>Default</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($languages as $language)
                                <tr>
                                    <td>
                                        {{$language->name}}
                                    </td>
                                    <td>
                                        {{$language->code}}
                                    </td>
                                    <td>
                                        @if ($language->status == \App\Models\Language::ENABLE)
                                            <span class="badge  bg-primary">Active</span>
                                        @else
                                            <span class="badge   bg-secondary">In-Active</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($language->is_default == \App\Models\Language::ENABLE)
                                            <span class="badge  bg-primary">Yes</span>
                                        @else
                                            <span class="badge  bg-danger">No</span>
                                        @endif
                                    </td>

                                    <td>

                                        <a href="{{route("language_edit",$language)}}" class="btn btn-primary btn-sm"
                                           data-toggle="tooltip" data-placement="top" title="edit language">
                                         <span class="nk-menu-icon text-white">
                                              <em class="icon ni ni-edit"></em>
                                         </span>
                                        </a>

                                        <a href="{{route("language_delete",$language)}}" class="btn btn-danger btn-sm"
                                           data-toggle="tooltip" data-placement="top" title="delete language">
                                         <span class="nk-menu-icon text-white">
                                              <em class="icon ni ni-trash-empty"></em>
                                         </span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@push("js")

@endpush
