@extends('admin.layouts.app')
@section('title','Edit Language')
@section('content')

    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Edit Language</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('language_update',$language)}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="name">Name</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control @error('name') is-invalid @enderror"
                                                   id="name"
                                                   name="name" placeholder="language name"
                                                   value="{{ old('name') ? old('name') : $language->name }}">
                                            @error('name')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="code">Code</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control @error('code') is-invalid @enderror"
                                                   id="code"
                                                   name="code" placeholder="language code"
                                                   value="{{old('code') ? old('code') : $language->code }}">
                                            @error('code')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="preview-block">
                                        <span class="preview-title overline-title">Status</span>
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" name="status" class="custom-control-input"   id="status" @if($language->status == 1) checked @endif value="1">
                                            <label class="custom-control-label" for="status">Enable</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="preview-block">
                                        <span class="preview-title overline-title">Default</span>
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" name="is_default" class="custom-control-input"  id="default"  @if($language->is_default == 1) checked @endif  value="1">
                                            <label class="custom-control-label" for="default">Enable</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary float-right">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')

@endpush
