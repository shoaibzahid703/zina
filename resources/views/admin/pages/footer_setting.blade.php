@extends('admin.layouts.app')
@section('title','Footer Setting')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"> Footer Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('footer_setting_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="footer_description">
                                                                Footer Description
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="footer_description"
                                                                   name="{{ $language->code }}[footer_description]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.footer_description') is-invalid @enderror"
                                                                   placeholder="footer description"

                                                                   @if($footer_setting)
                                                                       value="{{ old($language->code.'.footer_description') ? old($language->code.'.footer_description') :  $footer_setting->translate($language->code)->footer_description ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.footer_description')  }}"/>
                                                            @endif

                                                            @error($language->code.'.footer_description')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="contact_us_title">
                                                                Contact Us Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="contact_us_title"
                                                                   name="{{ $language->code }}[contact_us_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.contact_us_title') is-invalid @enderror"
                                                                   placeholder="contact us title"
                                                                   @if($footer_setting)
                                                                       value="{{ old($language->code.'.contact_us_title') ? old($language->code.'.contact_us_title') :  $footer_setting->translate($language->code)->contact_us_title ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.contact_us_title')  }}"/>
                                                            @endif
                                                            @error($language->code.'.contact_us_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address_title">
                                                                Address Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="address_title"
                                                                   name="{{ $language->code }}[address_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.address_title') is-invalid @enderror"
                                                                   placeholder="address title"
                                                                   @if($footer_setting)
                                                                       value="{{ old($language->code.'.address_title') ? old($language->code.'.address_title') : $footer_setting->translate($language->code)->address_title ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.address_title')  }}"/>

                                                            @endif
                                                            @error($language->code.'.address_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="phone_title">
                                                                Phone Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="phone_title"
                                                                   name="{{ $language->code }}[phone_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.phone_title') is-invalid @enderror"
                                                                   placeholder="phone title"
                                                                   @if($footer_setting)
                                                                       value="{{ old($language->code.'.phone_title') ? old($language->code.'.phone_title') : $footer_setting->translate($language->code)->phone_title ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.phone_title') }}"/>
                                                            @endif
                                                            @error($language->code.'.phone_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email_title">
                                                                Email Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="email_title"
                                                                   name="{{ $language->code }}[email_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.email_title') is-invalid @enderror"
                                                                   placeholder="email title"
                                                                   @if($footer_setting)
                                                                       value="{{old($language->code.'.email_title') ? old($language->code.'.email_title') : $footer_setting->translate($language->code)->email_title ?? '' }}"/>
                                                            @else
                                                                value="{{old($language->code.'.email_title')  }}"/>
                                                            @endif
                                                            @error($language->code.'.email_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="quick_link_title">
                                                                Quick Link Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="quick_link_title"
                                                                   name="{{ $language->code }}[quick_link_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.quick_link_title') is-invalid @enderror"
                                                                   placeholder="quick link title"
                                                                   @if($footer_setting)
                                                                       value="{{old($language->code.'.quick_link_title') ? old($language->code.'.quick_link_title') : $footer_setting->translate($language->code)->quick_link_title ?? '' }}"/>

                                                            @else
                                                                value="{{old($language->code.'.quick_link_title')  }}"/>

                                                            @endif
                                                            @error($language->code.'.quick_link_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="social_link_title">
                                                                Social Link Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="social_link_title"
                                                                   name="{{ $language->code }}[social_link_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.social_link_title') is-invalid @enderror"
                                                                   placeholder="quick link title"
                                                                   @if($footer_setting)
                                                                       value="{{old($language->code.'.social_link_title') ? old($language->code.'.social_link_title') : $footer_setting->translate($language->code)->social_link_title ?? '' }}"/>

                                                            @else
                                                                value="{{old($language->code.'.social_link_title') }}"/>

                                                            @endif
                                                            @error($language->code.'.social_link_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">

                                <div class="col-6">
                                    <label class="form-label" for="facebook_link">Facebook Link</label>
                                    <div class="form-control-wrap">
                                        <input type="text" name="facebook_link"
                                               class="form-control @error('facebook_link') is-invalid @enderror"
                                               id="facebook_link" placeholder="facebook link"
                                               value="{{old('facebook_link') ? old('facebook_link') : $footer_setting->facebook_link ?? ''}}">
                                        @error('facebook_link')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="youtube_link">Youtube Link</label>
                                    <div class="form-control-wrap">
                                        <input type="text" name="youtube_link"
                                               class="form-control @error('youtube_link') is-invalid @enderror"
                                               id="youtube_link" placeholder="youtube link"
                                               value="{{old('youtube_link') ? old('youtube_link') : $footer_setting->youtube_link ?? ''}}">
                                        @error('youtube_link')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="instagram_link">Instagram Link</label>
                                    <div class="form-control-wrap">
                                        <input type="text" name="instagram_link"
                                               class="form-control @error('instagram_link') is-invalid @enderror"
                                               id="instagram_link" placeholder="instagram link"
                                               value="{{old('instagram_link') ? old('instagram_link') : $footer_setting->instagram_link ?? ''}}">
                                        @error('instagram_link')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="address">Address</label>
                                    <div class="form-control-wrap">
                                        <input type="text" name="address"
                                               class="form-control @error('address') is-invalid @enderror"
                                               id="address" placeholder="address"
                                               value="{{old('address') ? old('address') : $footer_setting->address ?? ''}}">
                                        @error('address')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="phone">Phone</label>
                                    <div class="form-control-wrap">
                                        <input type="text" name="phone"
                                               class="form-control @error('phone') is-invalid @enderror"
                                               id="phone" placeholder="phone"
                                               value="{{old('phone') ? old('phone') : $footer_setting->phone ?? ''}}">
                                        @error('phone')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="email">Email</label>
                                    <div class="form-control-wrap">
                                        <input type="email" name="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               id="email" placeholder="email"
                                               value="{{old('email') ? old('email') : $footer_setting->email ?? ''}}">
                                        @error('email')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="footer_logo">Footer Logo</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="footer_logo"
                                               class="form-control-file @error('footer_logo') is-invalid @enderror"
                                               id="footer_logo" accept="image/*">
                                        @error('footer_logo')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')

@endpush
