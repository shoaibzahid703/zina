@extends('admin.layouts.app')
@section('title','Edit Product')
@section('content')

    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Edit Product</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('products_update',$product)}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span
                                                        class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="title">
                                                                Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="title"
                                                                   name="{{ $language->code }}[title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.title') is-invalid @enderror"
                                                                   placeholder="title"
                                                                   @if($product)
                                                                       value="{{ old($language->code.'.title') ? old($language->code.'.title') :  $product->translate($language->code)->title  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.title')  }}"  />
                                                            @endif
                                                            @error($language->code.'.title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="description">
                                                                Description
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>

                                                            <textarea id="description"
                                                                      name="{{ $language->code }}[description]"
                                                                      class="form-control @error($language->code.'.description') is-invalid @enderror description"
                                                                      placeholder="description"> @if($product) {{ old($language->code.'.description') ? old($language->code.'.description') :  $product->translate($language->code)->description  ?? '' }} @else {{ old($language->code.'.description')  }} @endif</textarea>

                                                            @error($language->code.'.description')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="product_image">Product Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="product_image"
                                               class="form-control-file @error('product_image') is-invalid @enderror"
                                               id="product_image" accept="image/*">
                                        @error('product_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary float-right">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
<script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    var descriptions = document.getElementsByClassName('description');
    for (var i = 0; i < descriptions.length; i++) {
        CKEDITOR.replace(descriptions[i], {
            filebrowserUploadMethod: 'form'
        });
    }
</script>
@endpush
