@extends('admin.layouts.app')
@section('title','Our Products')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Products</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">

                    <div class="card-inner">
                        <a href="{{route('products_create')}}" class="btn btn-primary float-end">
                            <em class="icon ni ni-plus"></em>
                            <span>Add Products</span>
                        </a>
                        <table class="table datatable-init" >
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($all_products as $product)
                                <tr>
                                    <td>
                                        {{$product->title}}
                                    </td>
                                    <td>
                                        {{ \Illuminate\Support\Str::limit(strip_tags($product->description),50)  }}
                                    </td>
                                    <td>
                                        <img src="{{asset('storage/products/'.$product->product_image)}}" class="img-fluid" width="80" height="80">
                                    </td>
                                    <td>

                                        <a href="{{route('products_edit', $product)}}" class="btn btn-primary btn-sm"
                                           data-toggle="tooltip" data-placement="top" title="edit">
                                         <span class="nk-menu-icon text-white">
                                              <em class="icon ni ni-edit"></em>
                                         </span>
                                        </a>

                                        <a href="{{route('products_delete',$product)}}" class="btn btn-danger btn-sm"
                                           data-toggle="tooltip" data-placement="top" title="delete">
                                         <span class="nk-menu-icon text-white">
                                              <em class="icon ni ni-trash-empty"></em>
                                         </span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

