@extends('admin.layouts.app')
@section('title','Home Page News Settings')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"> Home Page News Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('news_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--zina houser section--}}
                                                <div class="row">
                                                    <div class="col-nd-12">
                                                        <div class="form-group">
                                                            <label for="our_news_title">
                                                                Our News Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_news_title"
                                                                   name="{{ $language->code }}[our_news_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_news_title') is-invalid @enderror"
                                                                   placeholder="our news title"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_news_title') ? old($language->code.'.our_news_title') :  $home_page_setting->translate($language->code)->our_news_title  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_news_title')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_news_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-nd-12">
                                                        <div class="form-group">
                                                            <label for="our_news_heading">
                                                                Our News Heading
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_news_heading"
                                                                   name="{{ $language->code }}[our_news_heading]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_news_heading') is-invalid @enderror"
                                                                   placeholder="our news heading"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_news_heading') ? old($language->code.'.our_news_heading') :  $home_page_setting->translate($language->code)->our_news_heading   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_news_heading')  }}"/>
                                                            @endif
                                                            @error($language->code.'.our_news_heading')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-nd-12">
                                                        <div class="form-group">
                                                            <label for="our_news_desc">
                                                                Our News Desc
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <textarea id="our_news_desc"
                                                                      name="{{ $language->code }}[our_news_desc]"
                                                                      class="form-control @error($language->code.'.our_news_desc') is-invalid @enderror"
                                                                      placeholder="zina product desc"
                                                            >@if($home_page_setting) {{ old($language->code.'.our_news_desc') ? old($language->code.'.our_news_desc') :  $home_page_setting->translate($language->code)->our_news_desc}}@else{{ old($language->code.'.our_news_desc')}}@endif</textarea>

                                                            @error($language->code.'.our_news_desc')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="our_news_image">News Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="our_news_image"
                                               class="form-control-file @error('our_news_image') is-invalid @enderror"
                                               id="our_news_image" accept="image/*">
                                        @error('our_news_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

