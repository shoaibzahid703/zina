@extends('admin.layouts.app')
@section('title','Home Page Zina Product Settings')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"> Home Page Zina Product Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('zina_products_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">

                                                {{--our value section--}}
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="zina_product_heading">
                                                                Zina Product Heading
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="zina_product_heading"
                                                                   name="{{ $language->code }}[zina_product_heading]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.zina_product_heading') is-invalid @enderror"
                                                                   placeholder="zina product heading"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.zina_product_heading') ? old($language->code.'.zina_product_heading') :  $home_page_setting->translate($language->code)->zina_product_heading  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.zina_product_heading')  }}"/>
                                                            @endif

                                                            @error($language->code.'.zina_product_heading')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="zina_product_title">
                                                                Zina Product Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="zina_product_title"
                                                                   name="{{ $language->code }}[zina_product_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.zina_product_title') is-invalid @enderror"
                                                                   placeholder="zina product title"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.zina_product_title') ? old($language->code.'.zina_product_title') :  $home_page_setting->translate($language->code)->zina_product_title   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.zina_product_title')  }}"/>
                                                            @endif

                                                            @error($language->code.'.zina_product_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="zina_product_text">
                                                                Zina Product Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="zina_product_text"
                                                                   name="{{ $language->code }}[zina_product_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.zina_product_text') is-invalid @enderror"
                                                                   placeholder="zina product text"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.zina_product_text') ? old($language->code.'.zina_product_text') :  $home_page_setting->translate($language->code)->zina_product_text   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.zina_product_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.zina_product_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="zina_product_desc">
                                                                Zina Product Desc
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <textarea id="zina_product_desc"
                                                                      name="{{ $language->code }}[zina_product_desc]"
                                                                      class="form-control @error($language->code.'.zina_product_desc') is-invalid @enderror"
                                                                      placeholder="zina product desc"
                                                            >@if($home_page_setting) {{ old($language->code.'.zina_product_desc') ? old($language->code.'.zina_product_desc') :  $home_page_setting->translate($language->code)->zina_product_desc   ?? '' }}@else{{ old($language->code.'.zina_product_desc')  }}@endif</textarea>

                                                            @error($language->code.'.zina_product_desc')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="zina_product_image">Zina Product Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="zina_product_image"
                                               class="form-control-file @error('zina_product_image') is-invalid @enderror"
                                               id="zina_product_image" accept="image/*">
                                        @error('zina_product_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

