@extends('admin.layouts.app')
@section('title','Home Page Our Product Setting')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Home Page Our Product Setting</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('our_product_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--our product section--}}
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="our_product_heading">
                                                                Our Product Heading
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_product_heading"
                                                                   name="{{ $language->code }}[our_product_heading]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_product_heading') is-invalid @enderror"
                                                                   placeholder="our product heading"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_product_heading') ? old($language->code.'.our_product_heading') :  $home_page_setting->translate($language->code)->our_product_heading  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_product_heading')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_product_heading')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_product_point_one_title">
                                                                Our Product Point One Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_product_point_one_title"
                                                                   name="{{ $language->code }}[our_product_point_one_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_product_point_one_title') is-invalid @enderror"
                                                                   placeholder="our product point one title"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_product_point_one_title') ? old($language->code.'.our_product_point_one_title') :  $home_page_setting->translate($language->code)->our_product_point_one_title   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_product_point_one_title')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_product_point_one_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_product_point_one_desc">
                                                                Our Product Point One Desc
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_product_point_one_desc"
                                                                   name="{{ $language->code }}[our_product_point_one_desc]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_product_point_one_desc') is-invalid @enderror"
                                                                   placeholder="our product point one desc"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_product_point_one_desc') ? old($language->code.'.our_product_point_one_desc') :  $home_page_setting->translate($language->code)->our_product_point_one_desc   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_product_point_one_desc')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_product_point_one_desc')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_product_point_two_title">
                                                                Our Product Point Two Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_product_point_two_title"
                                                                   name="{{ $language->code }}[our_product_point_two_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_product_point_two_title') is-invalid @enderror"
                                                                   placeholder="our product point two title"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_product_point_two_title') ? old($language->code.'.our_product_point_two_title') :  $home_page_setting->translate($language->code)->our_product_point_two_title   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_product_point_two_title')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_product_point_two_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_product_point_two_desc">
                                                                Our Product Point Two Desc
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_product_point_two_desc"
                                                                   name="{{ $language->code }}[our_product_point_two_desc]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_product_point_two_desc') is-invalid @enderror"
                                                                   placeholder="our product point two desc"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_product_point_two_desc') ? old($language->code.'.our_product_point_two_desc') :  $home_page_setting->translate($language->code)->our_product_point_two_desc   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_product_point_two_desc')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_product_point_two_desc')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_product_point_three_title">
                                                                Our Product Point Three Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_product_point_three_title"
                                                                   name="{{ $language->code }}[our_product_point_three_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_product_point_three_title') is-invalid @enderror"
                                                                   placeholder="our product point three title"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_product_point_three_title') ? old($language->code.'.our_product_point_three_title') :  $home_page_setting->translate($language->code)->our_product_point_three_title   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_product_point_three_title')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_product_point_three_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_product_point_three_desc">
                                                                Our Product Point Three Desc
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_product_point_three_desc"
                                                                   name="{{ $language->code }}[our_product_point_three_desc]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_product_point_three_desc') is-invalid @enderror"
                                                                   placeholder="our product point three desc"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_product_point_three_desc') ? old($language->code.'.our_product_point_three_desc') :  $home_page_setting->translate($language->code)->our_product_point_three_desc   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_product_point_three_desc')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_product_point_three_desc')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_product_point_four_title">
                                                                Our Product Point Four Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_product_point_four_title"
                                                                   name="{{ $language->code }}[our_product_point_four_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_product_point_four_title') is-invalid @enderror"
                                                                   placeholder="our product point four title"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_product_point_four_title') ? old($language->code.'.our_product_point_four_title') :  $home_page_setting->translate($language->code)->our_product_point_four_title   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_product_point_four_title')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_product_point_four_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_product_point_four_desc">
                                                                Our Product Point Four Desc
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_product_point_four_desc"
                                                                   name="{{ $language->code }}[our_product_point_four_desc]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_product_point_four_desc') is-invalid @enderror"
                                                                   placeholder="our product point four desc"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_product_point_four_desc') ? old($language->code.'.our_product_point_four_desc') :  $home_page_setting->translate($language->code)->our_product_point_four_desc   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_product_point_four_desc')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_product_point_four_desc')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="our_product_image">Our Product Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="our_product_image"
                                               class="form-control-file @error('our_product_image') is-invalid @enderror"
                                               id="our_product_image" accept="image/*">
                                        @error('our_product_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
