@extends('admin.layouts.app')
@section('title','Home Page Zina House Settings')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"> Home Page Zina House Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('home_page_zina_section_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--zina houser section--}}
                                                <div class="row">
                                                    <div class="col-nd-12">
                                                        <div class="form-group">
                                                            <label for="zina_house_title">
                                                                Zina House Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="zina_house_title"
                                                                   name="{{ $language->code }}[zina_house_title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.zina_house_title') is-invalid @enderror"
                                                                   placeholder="zina house title"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.zina_house_title') ? old($language->code.'.zina_house_title') :  $home_page_setting->translate($language->code)->zina_house_title  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.zina_house_title')  }}"/>
                                                            @endif

                                                            @error($language->code.'.zina_house_title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-nd-12">
                                                        <div class="form-group">
                                                            <label for="zina_house_image_text">
                                                                Zina House Image Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="zina_house_image_text"
                                                                   name="{{ $language->code }}[zina_house_image_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.zina_house_image_text') is-invalid @enderror"
                                                                   placeholder="zina house image text"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.zina_house_image_text  ') ? old($language->code.'.zina_house_image_text') :  $home_page_setting->translate($language->code)->zina_house_image_text   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.zina_house_image_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.zina_house_image_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-nd-12">
                                                        <div class="form-group">
                                                            <label for="zina_house_button_text">
                                                                Zina House Button Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="zina_house_button_text"
                                                                   name="{{ $language->code }}[zina_house_button_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.zina_house_button_text') is-invalid @enderror"
                                                                   placeholder="zina house button text"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.zina_house_button_text  ') ? old($language->code.'.zina_house_button_text') :  $home_page_setting->translate($language->code)->zina_house_button_text   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.zina_house_button_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.zina_house_button_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="zina_house_image">Zina House Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="zina_house_image"
                                               class="form-control-file @error('zina_house_image') is-invalid @enderror"
                                               id="zina_house_image" accept="image/*">
                                        @error('zina_house_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="zina_house_button_link">Zina House Button Link</label>
                                    <div class="form-control-wrap">
                                        <input type="text" name="zina_house_button_link"
                                               class="form-control @error('zina_house_button_link') is-invalid @enderror"
                                               id="zina_house_button_link" placeholder="zina house button link"
                                               value="{{old('zina_house_button_link') ? old('zina_house_button_link') : $home_page_setting->zina_house_button_link ?? ''}}">
                                        @error('zina_house_button_link')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

