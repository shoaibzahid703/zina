@extends('admin.layouts.app')
@section('title','Home Navbar')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"> Home Page Navbar</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('navbar_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--zina houser section--}}
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="welcome_menu_text">
                                                                Welcome Menu Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="welcome_menu_text"
                                                                   name="{{ $language->code }}[welcome_menu_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.welcome_menu_text') is-invalid @enderror"
                                                                   placeholder="welcome menu text"
                                                                   @if($home_navbar_setting)
                                                                       value="{{ old($language->code.'.welcome_menu_text') ? old($language->code.'.welcome_menu_text') :  $home_navbar_setting->translate($language->code)->welcome_menu_text  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.welcome_menu_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.welcome_menu_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="zina_house_menu_text">
                                                                Zina House Menu Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="zina_house_menu_text"
                                                                   name="{{ $language->code }}[zina_house_menu_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.zina_house_menu_text') is-invalid @enderror"
                                                                   placeholder="zina house menu text"

                                                                   @if($home_navbar_setting)
                                                                       value="{{ old($language->code.'.zina_house_menu_text') ? old($language->code.'.zina_house_menu_text') :  $home_navbar_setting->translate($language->code)->zina_house_menu_text  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.zina_house_menu_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.zina_house_menu_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_products_menu_text">
                                                                Our Products Menu Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_products_menu_text"
                                                                   name="{{ $language->code }}[our_products_menu_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_products_menu_text') is-invalid @enderror"
                                                                   placeholder="our products menu text"

                                                                   @if($home_navbar_setting)
                                                                       value="{{ old($language->code.'.our_products_menu_text') ? old($language->code.'.our_products_menu_text') :  $home_navbar_setting->translate($language->code)->our_products_menu_text  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_products_menu_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_products_menu_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_values_menu_text">
                                                                Our Values Menu Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_values_menu_text"
                                                                   name="{{ $language->code }}[our_values_menu_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_values_menu_text') is-invalid @enderror"
                                                                   placeholder="our products menu text"

                                                                   @if($home_navbar_setting)
                                                                       value="{{ old($language->code.'.our_values_menu_text') ? old($language->code.'.our_values_menu_text') :  $home_navbar_setting->translate($language->code)->our_values_menu_text  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_values_menu_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_values_menu_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="news_menu_text">
                                                                News Menu Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="news_menu_text"
                                                                   name="{{ $language->code }}[news_menu_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.news_menu_text') is-invalid @enderror"
                                                                   placeholder="news menu text"

                                                                   @if($home_navbar_setting)
                                                                       value="{{ old($language->code.'.news_menu_text') ? old($language->code.'.news_menu_text') :  $home_navbar_setting->translate($language->code)->news_menu_text  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.news_menu_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.news_menu_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="recipes_menu_text">
                                                                Recipes Menu Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="recipes_menu_text"
                                                                   name="{{ $language->code }}[recipes_menu_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.recipes_menu_text') is-invalid @enderror"
                                                                   placeholder="recipes menu text"

                                                                   @if($home_navbar_setting)
                                                                       value="{{ old($language->code.'.recipes_menu_text') ? old($language->code.'.recipes_menu_text') :  $home_navbar_setting->translate($language->code)->recipes_menu_text  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.recipes_menu_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.recipes_menu_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="contact_menu_text">
                                                                Contact Menu Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="contact_menu_text"
                                                                   name="{{ $language->code }}[contact_menu_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.contact_menu_text') is-invalid @enderror"
                                                                   placeholder="contact menu text"

                                                                   @if($home_navbar_setting)
                                                                       value="{{ old($language->code.'.contact_menu_text') ? old($language->code.'.contact_menu_text') :  $home_navbar_setting->translate($language->code)->contact_menu_text  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.contact_menu_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.contact_menu_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

