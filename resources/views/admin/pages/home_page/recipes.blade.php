@extends('admin.layouts.app')
@section('title','Home Page Recipes Settings')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"> Home Page Recipes Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('recipes_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--zina houser section--}}
                                                <div class="row">
                                                    <div class="col-nd-12">
                                                        <div class="form-group">
                                                            <label for="our_recipe_heading">
                                                                Our Recipe Heading
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_recipe_heading"
                                                                   name="{{ $language->code }}[our_recipe_heading]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_recipe_heading') is-invalid @enderror"
                                                                   placeholder="our recipe heading"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_recipe_heading') ? old($language->code.'.our_recipe_heading') :  $home_page_setting->translate($language->code)->our_recipe_heading  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_recipe_heading')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_recipe_heading')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-nd-12">
                                                        <div class="form-group">
                                                            <label for="our_recipe_bottom_text">
                                                                Our Recipe Bottom Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_recipe_bottom_text"
                                                                   name="{{ $language->code }}[our_recipe_bottom_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_recipe_bottom_text') is-invalid @enderror"
                                                                   placeholder="our recipe bottom text"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_recipe_bottom_text') ? old($language->code.'.our_recipe_bottom_text') :  $home_page_setting->translate($language->code)->our_recipe_bottom_text   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_recipe_bottom_text')  }}"/>
                                                            @endif
                                                            @error($language->code.'.our_recipe_bottom_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="our_recipe_top_image">Our Recipe Top Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="our_recipe_top_image"
                                               class="form-control-file @error('our_recipe_top_image') is-invalid @enderror"
                                               id="our_recipe_top_image" accept="image/*">
                                        @error('our_recipe_top_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="our_recipe_bottom_image_one">Our Recipe Bottom Image One</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="our_recipe_bottom_image_one"
                                               class="form-control-file @error('our_recipe_bottom_image_one') is-invalid @enderror"
                                               id="our_recipe_bottom_image_one" accept="image/*">
                                        @error('our_recipe_bottom_image_one')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="our_recipe_bottom_image_two">Our Recipe Bottom Image Two</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="our_recipe_bottom_image_two"
                                               class="form-control-file @error('our_recipe_bottom_image_two') is-invalid @enderror"
                                               id="our_recipe_bottom_image_two" accept="image/*">
                                        @error('our_recipe_bottom_image_two')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="our_recipe_bottom_image_three">Our Recipe Bottom Image Three</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="our_recipe_bottom_image_three"
                                               class="form-control-file @error('our_recipe_bottom_image_three') is-invalid @enderror"
                                               id="our_recipe_bottom_image_three" accept="image/*">
                                        @error('our_recipe_bottom_image_three')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

