@extends('admin.layouts.app')
@section('title','Home Page Our Value Settings')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"> Home Page Our Value Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('our_value_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">

                                                {{--our value section--}}
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_value_heading">
                                                                Our Value Heading
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_value_heading"
                                                                   name="{{ $language->code }}[our_value_heading]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_value_heading') is-invalid @enderror"
                                                                   placeholder="our value heading"
                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_value_heading') ? old($language->code.'.our_value_heading') :  $home_page_setting->translate($language->code)->our_value_heading  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_value_heading')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_value_heading')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_value_image_text">
                                                                Our Value Image Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_value_image_text"
                                                                   name="{{ $language->code }}[our_value_image_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_value_image_text') is-invalid @enderror"
                                                                   placeholder="our value image text"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_value_image_text') ? old($language->code.'.our_value_image_text') :  $home_page_setting->translate($language->code)->our_value_image_text   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_value_image_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_value_image_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_value_image_second_text">
                                                                Our Value Image Second Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_value_image_second_text"
                                                                   name="{{ $language->code }}[our_value_image_second_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_value_image_second_text') is-invalid @enderror"
                                                                   placeholder="our value image second text"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_value_image_second_text') ? old($language->code.'.our_value_image_second_text') :  $home_page_setting->translate($language->code)->our_value_image_second_text   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_value_image_second_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_value_image_second_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="our_value_button_text">
                                                                Our Value button Text
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="our_value_button_text"
                                                                   name="{{ $language->code }}[our_value_button_text]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.our_value_button_text') is-invalid @enderror"
                                                                   placeholder="our value button text"

                                                                   @if($home_page_setting)
                                                                       value="{{ old($language->code.'.our_value_button_text') ? old($language->code.'.our_value_button_text') :  $home_page_setting->translate($language->code)->our_value_button_text   ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.our_value_button_text')  }}"/>
                                                            @endif

                                                            @error($language->code.'.our_value_button_text')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="our_value_image">Our Value Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="our_value_image"
                                               class="form-control-file @error('our_value_image') is-invalid @enderror"
                                               id="our_value_image" accept="image/*">
                                        @error('our_value_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-6">
                                    <label class="form-label" for="our_value_button_link">Our Value Button Link</label>
                                    <div class="form-control-wrap">
                                        <input type="text" name="our_value_button_link"
                                               class="form-control @error('our_value_button_link') is-invalid @enderror"
                                               id="our_value_button_link" placeholder="zina house button link"
                                               value="{{old('our_value_button_link') ? old('our_value_button_link') : $home_page_setting->our_value_button_link ?? ''}}">
                                        @error('our_value_button_link')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

