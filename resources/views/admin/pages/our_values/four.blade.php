@extends('admin.layouts.app')
@section('title','Our Value Section Four Settings')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Our Value Section Four Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('our_value_section_four_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                     {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                {{--zina houser section--}}
                                                <div class="row">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_4_heading">
                                                                    Section Four Top Heading
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_4_heading"
                                                                       name="{{ $language->code }}[section_4_heading]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_4_heading') is-invalid @enderror"
                                                                       placeholder="section four heading"
                                                                       @if($our_value_setting)
                                                                           value="{{ old($language->code.'.section_4_heading') ? old($language->code.'.section_4_heading') :  $our_value_setting->translate($language->code)->section_4_heading  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_4_heading')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_4_heading')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_4_ceo_name">
                                                                    Section Four Ceo Name
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_4_ceo_name"
                                                                       name="{{ $language->code }}[section_4_ceo_name]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_4_ceo_name') is-invalid @enderror"
                                                                       placeholder="section four ceo name"

                                                                       @if($our_value_setting)
                                                                           value="{{ old($language->code.'.section_4_ceo_name') ? old($language->code.'.section_4_ceo_name') :  $our_value_setting->translate($language->code)->section_4_ceo_name  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_4_ceo_name')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_4_ceo_name')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="section_4_bottom_text">
                                                                    Section Four Bottom Text
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <input id="section_4_bottom_text"
                                                                       name="{{ $language->code }}[section_4_bottom_text]"
                                                                       type="text"
                                                                       class="form-control @error($language->code.'.section_4_bottom_text') is-invalid @enderror"
                                                                       placeholder="section four bottom text"

                                                                       @if($our_value_setting)
                                                                           value="{{ old($language->code.'.section_4_bottom_text') ? old($language->code.'.section_4_bottom_text') :  $our_value_setting->translate($language->code)->section_4_bottom_text  ?? '' }}"/>
                                                                @else
                                                                    value="{{ old($language->code.'.section_4_bottom_text')  }}"/>
                                                                @endif

                                                                @error($language->code.'.section_4_ceo_name')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="section_4_paragraph">
                                                                    Section Four Paragraph
                                                                    ({{ strtoupper($language->name) }})
                                                                    <span class="text-info">*</span>
                                                                </label>
                                                                <textarea id="section_4_paragraph"
                                                                          name="{{ $language->code }}[section_4_paragraph]"
                                                                          class="form-control @error($language->code.'.section_4_paragraph') is-invalid @enderror"
                                                                          placeholder="section four paragraph">@if($our_value_setting){{ old($language->code.'.section_4_paragraph') ? old($language->code.'.section_4_paragraph') : $our_value_setting->translate($language->code)->section_4_paragraph  ?? ''}} @else {{ old($language->code.'.section_4_paragraph')  }} @endif</textarea>

                                                                @error($language->code.'.section_4_paragraph')
                                                                <div
                                                                    class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

