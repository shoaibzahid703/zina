@extends('admin.layouts.app')
@section('title','Site Setting')
@section('content')
    <style>
        .tagify{
            padding: 0.4375rem 1rem;
            color: #3c4d62;
            border: 1px solid #dbdfea !important;
        }
    </style>
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"> Site Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('site_settings.update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                                <div class="col-md-12 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="site_description">Site Description</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control @error('site_description') is-invalid @enderror" id="site_description" name="site_description" placeholder="site description"
                                                   value="{{old('site_description') ? old('site_description') : $setting->site_description}}">
                                            @error('site_description')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="seo_tags">Seo Tags</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="w-100 @error('seo_tags') is-invalid @enderror" id="seo_tags" name="seo_tags" placeholder="seo tags"
                                                   value="{{old('seo_tags') ? old('seo_tags') : $setting->seo_tags}}" >
                                            @error('seo_tags')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary float-right">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <link href="https://cdn.jsdelivr.net/npm/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
    <script src="https://cdn.jsdelivr.net/npm/@yaireo/tagify"></script>
    <script src="https://cdn.jsdelivr.net/npm/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
    <script>
        var inputElm = document.querySelector('input[name=seo_tags]');
        var tagify = new Tagify(inputElm,{
            originalInputValueFormat: valuesArr => valuesArr.map(item => item.value).join(',')
        })
    </script>
@endpush
