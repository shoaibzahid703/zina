@extends('admin.layouts.app')
@section('title','Edit Recipe')
@section('content')

    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Edit Recipe</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('home_recipes_update',$recipes)}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="nav nav-tabs mt-n3" role="tablist">
                                        @foreach($all_languages as $key => $language)
                                            <li class="nav-item">
                                                <a class="nav-link @if($loop->iteration === 1) active @endif"
                                                   data-bs-toggle="tab"
                                                   href="#tab_{{ $key }}"
                                                >
                                                    <span
                                                        class="nav-text @error($language->code.'.*') text-danger @enderror">
                                                {{ $language->name }}
                                                  </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_languages as $key => $language)
                                            <div
                                                class="tab-pane fade  @if($loop->iteration === 1) show active @endif"
                                                id="tab_{{ $key }}" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="title">
                                                                Title
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="title"
                                                                   name="{{ $language->code }}[title]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.title') is-invalid @enderror"
                                                                   placeholder="title"
                                                                   @if($recipes)
                                                                       value="{{ old($language->code.'.title') ? old($language->code.'.title') :  $recipes->translate($language->code)->title  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.title')  }}"  />
                                                            @endif
                                                            @error($language->code.'.title')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="description">
                                                                Description
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>

                                                            <textarea id="description"
                                                                      name="{{ $language->code }}[description]"
                                                                      class="form-control @error($language->code.'.description') is-invalid @enderror description"
                                                                      placeholder="description"> @if($recipes) {{ old($language->code.'.description') ? old($language->code.'.description') :  $recipes->translate($language->code)->description  ?? '' }} @else {{ old($language->code.'.description')  }} @endif</textarea>

                                                            @error($language->code.'.description')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="make_time">
                                                                Make Time
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="make_time"
                                                                   name="{{ $language->code }}[make_time]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.make_time') is-invalid @enderror"
                                                                   placeholder="make_time"
                                                                   @if($recipes)
                                                                       value="{{ old($language->code.'.make_time') ? old($language->code.'.make_time') :  $recipes->translate($language->code)->make_time  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.make_time')  }}"  />
                                                            @endif
                                                            @error($language->code.'.make_time')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="make_way">
                                                                Make Way
                                                                ({{ strtoupper($language->name) }})
                                                                <span class="text-info">*</span>
                                                            </label>
                                                            <input id="make_way"
                                                                   name="{{ $language->code }}[make_way]"
                                                                   type="text"
                                                                   class="form-control @error($language->code.'.make_way') is-invalid @enderror"
                                                                   placeholder="make_way"
                                                                   @if($recipes)
                                                                       value="{{ old($language->code.'.make_way') ? old($language->code.'.make_way') :  $recipes->translate($language->code)->make_way  ?? '' }}"/>
                                                            @else
                                                                value="{{ old($language->code.'.make_way')  }}"  />
                                                            @endif
                                                            @error($language->code.'.make_way')
                                                            <div
                                                                class="invalid-feedback">{{ str_replace('.',' ',$message) }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label class="form-label" for="recipe_image">Recipe Image</label>
                                    <div class="form-control-wrap">
                                        <input type="file" name="recipe_image"
                                               class="form-control-file @error('recipe_image') is-invalid @enderror"
                                               id="recipe_image" accept="image/*">
                                        @error('recipe_image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary float-right">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
<script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    var descriptions = document.getElementsByClassName('description');
    for (var i = 0; i < descriptions.length; i++) {
        CKEDITOR.replace(descriptions[i], {
            filebrowserUploadMethod: 'form'
        });
    }
</script>
@endpush
