@extends('admin.layouts.app')
@section('title','Recipes')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Recipes</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">

                    <div class="card-inner">
                        <a href="{{route('recipes_create')}}" class="btn btn-primary float-end">
                            <em class="icon ni ni-plus"></em>
                            <span>Add Recipe</span>
                        </a>
                        <table class="table datatable-init" >
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Make Time</th>
                                <th>Make Way</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($all_recipes as $recipe)
                                <tr>
                                    <td>
                                        {{$recipe->title}}
                                    </td>
                                    <td>
                                        {{ \Illuminate\Support\Str::limit(strip_tags($recipe->description),50)  }}
                                    </td>
                                    <td>
                                        {{$recipe->make_time}}
                                    </td>
                                    <td>
                                        {{$recipe->make_way}}
                                    </td>
                                    <td>
                                        <img src="{{asset('storage/recipes/'.$recipe->recipe_image)}}" class="img-fluid" width="80" height="80">
                                    </td>


                                    <td>

                                        <a href="{{route('recipes_edit', $recipe)}}" class="btn btn-primary btn-sm"
                                           data-toggle="tooltip" data-placement="top" title="edit">
                                         <span class="nk-menu-icon text-white">
                                              <em class="icon ni ni-edit"></em>
                                         </span>
                                        </a>

                                        <a href="{{route('recipes_delete', $recipe)}}" class="btn btn-danger btn-sm"
                                           data-toggle="tooltip" data-placement="top" title="delete">
                                         <span class="nk-menu-icon text-white">
                                              <em class="icon ni ni-trash-empty"></em>
                                         </span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

