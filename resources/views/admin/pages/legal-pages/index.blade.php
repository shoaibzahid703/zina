@extends('admin.layouts.app')
@section('title','Legal pages')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Legal Pages</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">

                    <div class="card-inner">
                        <a href="{{route('legal_page_create')}}" class="btn btn-primary float-end">
                            <em class="icon ni ni-plus"></em>
                            <span>Add Legal Page</span>
                        </a>
                        <table class="table datatable-init" >
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($all_legal_pages as $legal_page)
                                <tr>
                                    <td>
                                        {{$legal_page->name}}
                                    </td>
                                    <td>
                                        {{ \Illuminate\Support\Str::limit(strip_tags($legal_page->description),50)  }}
                                    </td>
                                    <td>
                                        <img src="{{asset('storage/pages/'.$legal_page->page_image)}}" class="img-fluid" width="80" height="80">
                                    </td>
                                    <td>

                                        <a href="{{route('legal_page_edit', $legal_page)}}" class="btn btn-primary btn-sm"
                                           data-toggle="tooltip" data-placement="top" title="edit">
                                         <span class="nk-menu-icon text-white">
                                              <em class="icon ni ni-edit"></em>
                                         </span>
                                        </a>

                                        <a href="{{route('legal_page_destroy',$legal_page)}}" class="btn btn-danger btn-sm"
                                           data-toggle="tooltip" data-placement="top" title="delete">
                                         <span class="nk-menu-icon text-white">
                                              <em class="icon ni ni-trash-empty"></em>
                                         </span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

