<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    @if(isset($settings->site_description))
        <meta name="description" content="{{$settings->site_description}}">
    @endif
    @if(isset($settings->seo_tags))
        <meta name="keywords" content="{{$settings->seo_tags}}">
    @endif
    <!-- Page Title  -->
    <title>@yield('title')</title>
    <link rel="shortcut icon" type="image/png" href="{{asset('assets/auth/images/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/dashboard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/sweetalert.css')}}">
    @stack('css')
</head>

<body class="nk-body bg-lighter npc-default has-sidebar ">
<div class="nk-app-root">
    <!-- main @s -->
    <div class="nk-main ">
        @include('admin.includes.sidebar')
        <!-- wrap @s -->
        <div class="nk-wrap ">
            @include('admin.includes.header')
            <!-- content @s -->
            <div class="nk-content ">
                <br>
                @yield('content')
            </div>
            <!-- content @e -->
            @include('admin.includes.footer')

        </div>
        <!-- wrap @e -->
    </div>
    <!-- main @e -->
</div>
<!-- JavaScript -->
<script src="{{asset('assets/admin/js/bundle.js')}}"></script>
<script src="{{asset('assets/admin/js/scripts.js')}}"></script>
<script src="{{asset('assets/admin/js/sweetalert.js')}}"></script>


@include('admin.includes.messages')

@stack('js')
</body>

</html>
