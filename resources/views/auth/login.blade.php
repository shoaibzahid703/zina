@extends('layouts.app')
@section('title','Login')
@section('content')
    <style>
        .togglePassword{
            cursor: pointer;
            position: absolute;
            top: 66%;
            right: 5%;
        }
    </style>
    <div class="container-fluid s_login">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a href="{{route('welcome')}}">
                                    <img src="{{asset('assets/auth/images/zina-logo.png')}}" class="img-fluid" alt="logo" />
                                    <h2 class="card-title">Get Started</h2>
                                    <p class="card-text">Please Login To Your Account.</p>
                                </a>
                            </div>
                            <div class="col-md-12 text-center">
                                @if(session('info'))
                                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                                        <style type="text/css">
                                            .togglePassword{
                                                cursor: pointer;
                                                position: absolute;
                                                top: 66%;
                                                right: 5%;
                                                margin-top: 30px;
                                            }
                                        </style>
                                        <strong> {{ session('info') }}</strong>
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-12 mt-3 ">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" name="email" id="email" placeholder="Enter Your Email" class="form-control @error('email') is-invalid @enderror"/>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" placeholder="Enter Your Password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" />
                                        <i class="fa fa-eye togglePassword togglePassworrd" id="togglePassword"></i>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="s_remember">
                                        <label>Remember Me
                                            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <span class="s_checkbox"></span>
                                        </label>
                                    </div>
                                    <div class="s_jump_page text-center">
                                        <button type="submit" class="ss_btn submitForm">login</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('js')
    <script>
        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#password');

        togglePassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });
    </script>
@endpush
