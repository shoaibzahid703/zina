
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    @if(isset($settings->site_description))
        <meta name="description" content="{{$settings->site_description}}">
    @endif
    @if(isset($settings->seo_tags))
        <meta name="keywords" content="{{$settings->seo_tags}}">
    @endif
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="MobileOptimized" content="320" />
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{asset('assets/auth/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/auth/css/fonts.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/auth/css/fontawesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/auth/css/magnific-popup.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/auth/css/swiper.min.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/auth/css/animate.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/auth/css/style.css')}}" />
    <!--=== Favicon Link ===-->
    <link rel="shortcut icon" type="image/png" href="{{asset('assets/auth/images/favicon.png')}}">
    @stack('css')

</head>
<body class="ss_login_bg">

@yield('content')

<!--===script start===-->
<script src="{{asset('assets/auth/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/auth/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/auth/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/auth/js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('assets/auth/js/swiper.min.js')}}"></script>
<script src="{{asset('assets/auth/js/wow.min.js')}}"></script>
<script src="{{asset('assets/auth/js/script.js')}}"></script>

@stack('js')

</body>
</html>

