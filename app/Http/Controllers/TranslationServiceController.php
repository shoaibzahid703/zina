<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class TranslationServiceController extends Controller
{
    public function change_language(Request $request){
        Session::put('selected_language', $request->selected_language) ;
        return redirect()->back();
    }

    public function selected_language(){
        if(Session::has('selected_language')){
            return Language::find(Session::get('selected_language'));
        } else{
            return Language::all()->where("is_default", "=", 1)->first();
        }

    }

    public function all_languages(){
        return Language::where('status',1)->get();
    }
}
