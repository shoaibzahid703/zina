<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TranslationServiceController;
use App\Models\OurValue;
use App\Traits\imageUploadTrait;
use Illuminate\Http\Request;

class OurValueController extends Controller
{
    use imageUploadTrait;
    public $selected_language ;
    public $all_languages ;
    public function __construct(){
        $translation = new TranslationServiceController();
        $this->selected_language = $translation->selected_language();
        $this->all_languages = $translation->all_languages();
    }
    public function our_value_section_one()
    {
        $our_value_setting = OurValue::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.our_values.one',compact('our_value_setting','selected_language','all_languages'));
    }
    public function our_value_section_one_update(Request $request)
    {
        $our_value = OurValue::first();
        if ($our_value){
            $data = $request->validate($this->getSectionOneRules());
            if ($request->file('section_1_image')){
                $section_1_image = self::uploadFile($request,'section_1_image','our_value');
                $data['section_1_image'] = $section_1_image;
            }
            $our_value->update($data);
            return redirect()->back()->with('success','Our Value Section One Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionOneRules());
            if ($request->file('section_1_image')){
                $section_1_image = self::uploadFile($request,'section_1_image','our_value');
                $data['section_1_image'] = $section_1_image;
            }
            OurValue::create($data);
            return redirect()->back()->with('success','Our Value Section One Added Successfully');
        }
    }
    private function getSectionOneRules(){
        return [
            'section_1_image' => 'sometimes|image',
        ];
    }
    public function our_value_section_two()
    {
        $our_value_setting = OurValue::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.our_values.two',compact('our_value_setting','selected_language','all_languages'));
    }
    public function our_value_section_two_update(Request $request)
    {
        $our_value = OurValue::first();
        if ($our_value){
            $data = $request->validate($this->getSectionTwoRules());
            if ($request->file('section_2_bg_image')){
                $section_2_bg_image = self::uploadFile($request,'section_2_bg_image','our_value');
                $data['section_2_bg_image'] = $section_2_bg_image;
            }
            $our_value->update($data);
            return redirect()->back()->with('success','Our Value Section Two Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionTwoRules());
            if ($request->file('section_2_bg_image')){
                $section_2_bg_image = self::uploadFile($request,'section_2_bg_image','our_value');
                $data['section_2_bg_image'] = $section_2_bg_image;
            }
            OurValue::create($data);
            return redirect()->back()->with('success','Our Value Section Two Added Successfully');
        }
    }
    private function getSectionTwoRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.section_2_heading'] = 'required|max:255';
            $rules[$value->code.'.section_2_text'] = 'required|max:255';
            $rules[$value->code.'.section_2_paragraph'] = 'required|max:500';
            $rules[$value->code.'.section_2_bottom_text'] = 'required|max:255';
        }
        $other_rules = [
            'section_2_bg_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }
    public function our_value_section_three()
    {
        $our_value_setting = OurValue::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.our_values.three',compact('our_value_setting','selected_language','all_languages'));
    }
    public function our_value_section_three_update(Request $request)
    {
        $our_value = OurValue::first();
        if ($our_value){
            $data = $request->validate($this->getSectionThreeRules());
            if ($request->file('section_3_side_image')){
                $section_3_side_image = self::uploadFile($request,'section_3_side_image','our_value');
                $data['section_3_side_image'] = $section_3_side_image;
            }
            $our_value->update($data);
            return redirect()->back()->with('success','Our Value Section Three Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionThreeRules());
            if ($request->file('section_3_side_image')){
                $section_3_side_image = self::uploadFile($request,'section_3_side_image','our_value');
                $data['section_3_side_image'] = $section_3_side_image;
            }
            OurValue::create($data);
            return redirect()->back()->with('success','Our Value Section Three Added Successfully');
        }
    }
    private function getSectionThreeRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.section_3_top_heading'] = 'required|max:200';
            $rules[$value->code.'.section_3_text'] = 'required|max:200';
            $rules[$value->code.'.section_3_paragraph'] = 'required|max:500';
            $rules[$value->code.'.section_3_bottom_heading'] = 'required|max:200';
            $rules[$value->code.'.section_3_last_text'] = 'required|max:200';
        }
        $other_rules = [
            'section_3_side_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }
    public function our_value_section_four()
    {
        $our_value_setting = OurValue::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.our_values.four',compact('our_value_setting','selected_language','all_languages'));
    }
    public function our_value_section_four_update(Request $request)
    {
        $our_value = OurValue::first();
        if ($our_value){
            $data = $request->validate($this->getSectionFourRules());
            $our_value->update($data);
            return redirect()->back()->with('success','Our Value Section Four Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionFourRules());
            OurValue::create($data);
            return redirect()->back()->with('success','Our Value Section Four Added Successfully');
        }
    }
    private function getSectionFourRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.section_4_heading'] = 'required|max:200';
            $rules[$value->code.'.section_4_paragraph'] = 'required|max:500';
            $rules[$value->code.'.section_4_ceo_name'] = 'required|max:200';
            $rules[$value->code.'.section_4_bottom_text'] = 'required|max:200';
        }
        return $rules;
    }
    public function our_value_section_five()
    {
        $our_value_setting = OurValue::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.our_values.five',compact('our_value_setting','selected_language','all_languages'));
    }
    public function our_value_section_five_update(Request $request)
    {
        $our_value = OurValue::first();
        if ($our_value){
            $data = $request->validate($this->getSectionFiveRules());
            $our_value->update($data);
            return redirect()->back()->with('success','Our Value Section Five Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionFiveRules());
            OurValue::create($data);
            return redirect()->back()->with('success','Our Value Section Five Added Successfully');
        }
    }

    private function getSectionFiveRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.section_5_text'] = 'required|max:200';
            $rules[$value->code.'.section_5_heading'] = 'required|max:200';
            $rules[$value->code.'.section_5_bottom_text'] = 'required|max:200';
            $rules[$value->code.'.section_5_paragraph'] = 'required|max:500';
            $rules[$value->code.'.section_5_results_text'] = 'required|max:200';
            $rules[$value->code.'.section_5_drop_heading'] = 'required|max:200';
            $rules[$value->code.'.section_5_drop_paragraph'] = 'required|max:200';
            $rules[$value->code.'.section_5_co2_heading'] = 'required|max:200';
            $rules[$value->code.'.section_5_co2_paragraph'] = 'required|max:200';
        }
        return $rules;
    }

    public function our_value_section_six()
    {
        $our_value_setting = OurValue::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.our_values.six',compact('our_value_setting','selected_language','all_languages'));
    }
    public function our_value_section_six_update(Request $request)
    {
        $our_value = OurValue::first();
        if ($our_value){
            $data = $request->validate($this->getSectionSixRules());
            if ($request->file('section_6_side_image')){
                $section_6_side_image = self::uploadFile($request,'section_6_side_image','our_value');
                $data['section_6_side_image'] = $section_6_side_image;
            }
            $our_value->update($data);
            return redirect()->back()->with('success','Our Value Section Six Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionSixRules());
            if ($request->file('section_6_side_image')){
                $section_6_side_image = self::uploadFile($request,'section_6_side_image','our_value');
                $data['section_6_side_image'] = $section_6_side_image;
            }
            OurValue::create($data);
            return redirect()->back()->with('success','Our Value Section Six Added Successfully');
        }
    }

    private function getSectionSixRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.section_6_top_text'] = 'required|max:200';
            $rules[$value->code.'.section_6_heading'] = 'required|max:200';
            $rules[$value->code.'.section_6_learn_more'] = 'required|max:200';
            $rules[$value->code.'.section_6_paragraph'] = 'required|max:500';
        }
        $other_rules = [
            'section_6_side_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }

    public function our_value_section_seven()
    {
        $our_value_setting = OurValue::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.our_values.seven',compact('our_value_setting','selected_language','all_languages'));
    }
    public function our_value_section_seven_update(Request $request)
    {
        $our_value = OurValue::first();
        if ($our_value){
            $data = $request->validate($this->getSectionSevenRules());
            if ($request->file('section_7_image')){
                $section_7_image = self::uploadFile($request,'section_7_image','our_value');
                $data['section_7_image'] = $section_7_image;
            }
            $our_value->update($data);
            return redirect()->back()->with('success','Our Value Section Seven Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionSevenRules());
            if ($request->file('section_7_image')){
                $section_7_image = self::uploadFile($request,'section_7_image','our_value');
                $data['section_7_image'] = $section_7_image;
            }
            OurValue::create($data);
            return redirect()->back()->with('success','Our Value Section Seven Added Successfully');
        }
    }

    private function getSectionSevenRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.section_7_top_text'] = 'required|max:200';
            $rules[$value->code.'.section_7_heading'] = 'required|max:200';
            $rules[$value->code.'.section_7_paragraph'] = 'required|max:500';
        }
        $other_rules = [
            'section_7_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }
}
