<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\imageUploadTrait;
use App\Http\Controllers\TranslationServiceController;
use App\Models\Recipe;

class HomePageRecipeController extends Controller
{

    use imageUploadTrait;

    public function __construct(){
        $translation = new TranslationServiceController();
        $this->selected_language = $translation->selected_language();
        $this->all_languages = $translation->all_languages();
    }


    public function index()
    {
        $all_recipes= Recipe::orderBy('id','desc')->get();
        return view('admin.pages.recipes.index', compact('all_recipes'));
    }


    public function create()
    {
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.recipes.create',compact('selected_language','all_languages'));
    }

    public function store(Request $request){

        $data = $request->validate($this->getRules());

        if ($request->hasFile('recipe_image')) {
            $recipe_image = self::uploadFile($request, fileName: 'recipe_image', folderName: 'recipes');
            $data['recipe_image'] = $recipe_image;
        }

        Recipe::create($data);

        return redirect()->route('home-recipes')->with('success', 'Recipe Data Added Successfully');
    }


    private function getRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.title'] = 'required|max:255';
            $rules[$value->code.'.description'] = 'required|max:500';
            $rules[$value->code.'.make_time'] = 'required|max:500';
            $rules[$value->code.'.make_way'] = 'required|max:500';
        }
        $other_rules = [
            'recipe_image' => 'required|image',
        ];
        return array_merge($rules,$other_rules);
    }


    public function edit(Recipe $recipes)
    {
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.recipes.edit',compact('selected_language','all_languages','recipes'));
    }


    public function update(Request $request,Recipe $recipes)
    {
        $data = $request->validate($this->updateRules());
        if ($request->file('recipe_image')){
            $recipe_image = self::uploadFile($request,'recipe_image','recipes');
            $data['recipe_image'] = $recipe_image;
        }
        $recipes->update($data);
        return redirect()->route('home-recipes')->with('success','Recipe Data Update Successfully');
    }

    private function updateRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.title'] = 'required|max:255';
            $rules[$value->code.'.description'] = 'required|max:500';

            $rules[$value->code.'.make_time'] = 'required|max:500';
            $rules[$value->code.'.make_way'] = 'required|max:500';

        }
        $other_rules = [
            'recipe_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }

    public function delete(Recipe $recipes)
    {
        Recipe::destroy($recipes->id);
        return redirect()->back()->with('success','Recipe Data Deleted Successfully');
    }
}
