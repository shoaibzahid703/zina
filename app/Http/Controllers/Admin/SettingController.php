<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{

    public function site_setting(){
        $setting = Setting::first();
        return view('admin.pages.setting',compact('setting'));
    }
    public function site_setting_update(Request $request)
    {
        $request->validate([
            'site_name' => 'required|max:100',
            'site_description' => 'nullable|max:200',
            'lead_price' => 'required',
        ]);
        $setting = Setting::first();

        $setting->update([
            'site_name' => $request->site_name,
            'seo_tags' => $request->seo_tags,
            'site_description' => $request->site_description,
            'lead_price' => $request->lead_price,
            'admin_email' => $request->admin_email,
        ]);
        return redirect()->back()->with('success','setting updated successfully');

    }

    public function leadSetting(){
        $setting = Setting::first();
        return view('admin.pages.lead-setting',compact('setting'));
    }

    public function leadUserSettings(Request $request){
        $setting = Setting::first();
        $setting->update([
            'residential_lead_option_1'=> $request->residential_lead_option_1,
            'residential_lead_option_2'=> $request->residential_lead_option_2,
            'residential_lead_option_3'=> $request->residential_lead_option_3,
            'residential_lead_option_4'=> $request->residential_lead_option_4,
            'residential_lead_option_5'=> $request->residential_lead_option_5,
            'commercial_lead_option_1'=>  $request->commercial_lead_option_1,
            'commercial_lead_option_2'=>  $request->commercial_lead_option_2,
            'commercial_lead_option_3'=>  $request->commercial_lead_option_3,
            'commercial_lead_option_4'=>  $request->commercial_lead_option_4,
        ]);
        return redirect()->back()->with('success','Record added successfully');
    }


}
