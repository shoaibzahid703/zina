<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OurNew;
use App\Models\Product;
use App\Models\Recipe;
use App\Models\User;
use App\Models\Transaction;
use App\Models\PurchaseRequest;
use App\Models\RefundRequest;
use App\Models\ContactQuery;

class DashboardController extends Controller
{
    public function index()
    {
        $total_news = OurNew::count();
        $total_recipes = Recipe::count();
        $total_products = Product::count();
        return view('admin.pages.dashboard',compact('total_news','total_recipes','total_products'));
    }
}
