<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TranslationServiceController;
use App\Models\FooterSetting;
use App\Traits\imageUploadTrait;
use Illuminate\Http\Request;

class FooterController extends Controller
{
    use imageUploadTrait;
    public $selected_language ;
    public $all_languages ;
    public function __construct(){
        $translation = new TranslationServiceController();
        $this->selected_language = $translation->selected_language();
        $this->all_languages = $translation->all_languages();
    }
    public function index()
    {
        $footer_setting = FooterSetting::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.footer_setting',compact('footer_setting','selected_language','all_languages'));
    }
    public function update(Request $request)
    {
        $footer_setting = FooterSetting::first();
        if ($footer_setting){
            $data = $request->validate($this->getRules());
            if ($request->file('footer_logo')){
                $footer_logo = self::uploadFile($request,'footer_logo','footer_logo');
                $data['footer_logo'] = $footer_logo;
            }
            $footer_setting->update($data);
            return redirect()->back()->with('success','Footer Setting Updated Successfully');
        }else{
            $data = $request->validate($this->getRules());
            if ($request->file('footer_logo')){
                $footer_logo = self::uploadFile($request,'footer_logo','footer_logo');
                $data['footer_logo'] = $footer_logo;
            }
            FooterSetting::create($data);
            return redirect()->back()->with('success','Footer Setting Added Successfully');
        }
    }

    private function getRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.footer_description'] = 'required|max:255';
            $rules[$value->code.'.contact_us_title'] = 'required|max:25';
            $rules[$value->code.'.address_title'] = 'required|max:25';
            $rules[$value->code.'.phone_title'] = 'required|max:25';
            $rules[$value->code.'.email_title'] = 'required|max:25';
            $rules[$value->code.'.quick_link_title'] = 'required|max:25';
            $rules[$value->code.'.social_link_title'] = 'required|max:25';
        }
        $other_rules = [
            'footer_logo' => 'sometimes|image',
            'facebook_link' => 'required',
            'youtube_link' => 'required',
            'instagram_link' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        ];
        return array_merge($rules,$other_rules);
    }
}
