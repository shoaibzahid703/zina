<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TranslationServiceController;
use App\Models\HomeNavbar;
use App\Models\HomeSetting;
use App\Models\NewsSetting;
use App\Models\ProductSetting;
use App\Models\RecipeHomePage;
use App\Traits\imageUploadTrait;
use Illuminate\Http\Request;

class HomePageSettingController extends Controller
{
    use imageUploadTrait;
    public $selected_language ;
    public $all_languages ;
    public function __construct(){
        $translation = new TranslationServiceController();
        $this->selected_language = $translation->selected_language();
        $this->all_languages = $translation->all_languages();
    }

    public function home_slider_section()
    {
        $home_page_setting = HomeSetting::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.home_page.slider',compact('home_page_setting','selected_language','all_languages'));
    }

    public function home_page_zina_section()
    {
        $home_page_setting = HomeSetting::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.home_page.zina_section',compact('home_page_setting','selected_language','all_languages'));
    }

    public function zina_section_update(Request $request)
    {
        $home_setting = HomeSetting::first();
        if ($home_setting){
            $data = $request->validate($this->getZinaHouseRules());
            if ($request->file('zina_house_image')){
                $zina_house_image = self::uploadFile($request,'zina_house_image','zina_house');
                $data['zina_house_image'] = $zina_house_image;
            }
            $home_setting->update($data);
            return redirect()->back()->with('success','Home Zina House Section Setting Updated Successfully');
        }
        else{
            $data = $request->validate($this->getZinaHouseRules());
            if ($request->file('zina_house_image')){
                $zina_house_image = self::uploadFile($request,'zina_house_image','zina_house');
                $data['zina_house_image'] = $zina_house_image;
            }
            HomeSetting::create($data);
            return redirect()->back()->with('success','Home Zina House Section Setting Added Successfully');
        }
    }

    private function getZinaHouseRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.zina_house_title'] = 'required|max:255';
            $rules[$value->code.'.zina_house_image_text'] = 'required|max:100';
            $rules[$value->code.'.zina_house_button_text'] = 'required|max:25';
        }
        $other_rules = [
            'zina_house_image' => 'sometimes|image',
            'zina_house_button_link' => 'required',
        ];
        return array_merge($rules,$other_rules);
    }

    public function home_page_our_product()
    {
        $home_page_setting = HomeSetting::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.home_page.our_product',compact('home_page_setting','selected_language','all_languages'));
    }

    public function our_product_update(Request $request)
    {
        $home_setting = HomeSetting::first();
        if ($home_setting){
            $data = $request->validate($this->getOurProductRules());
            if ($request->file('our_product_image')){
                $our_product_image = self::uploadFile($request,'our_product_image','our_product');
                $data['our_product_image'] = $our_product_image;
            }
            $home_setting->update($data);
            return redirect()->back()->with('success','Home Our Product Section Setting Updated Successfully');
        }
        else{
            $data = $request->validate($this->getOurProductRules());
            if ($request->file('our_product_image')){
                $our_product_image = self::uploadFile($request,'our_product_image','our_product');
                $data['our_product_image'] = $our_product_image;
            }
            HomeSetting::create($data);
            return redirect()->back()->with('success','Home Our Product Section Setting Added Successfully');
        }
    }

    private function getOurProductRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.our_product_heading'] = 'required|max:200';
            $rules[$value->code.'.our_product_point_one_title'] = 'required|max:200';
            $rules[$value->code.'.our_product_point_one_desc'] = 'required|max:200';
            $rules[$value->code.'.our_product_point_two_title'] = 'required|max:200';
            $rules[$value->code.'.our_product_point_two_desc'] = 'required|max:200';
            $rules[$value->code.'.our_product_point_three_title'] = 'required|max:200';
            $rules[$value->code.'.our_product_point_three_desc'] = 'required|max:200';
            $rules[$value->code.'.our_product_point_four_title'] = 'required|max:200';
            $rules[$value->code.'.our_product_point_four_desc'] = 'required|max:200';
        }
        $other_rules = [
            'our_product_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }

    public function home_page_our_value()
    {
        $home_page_setting = HomeSetting::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.home_page.our_value',compact('home_page_setting','selected_language','all_languages'));
    }

    public function our_value_update(Request $request)
    {
        $home_setting = HomeSetting::first();
        if ($home_setting){
            $data = $request->validate($this->getOurValueRules());
            if ($request->file('our_value_image')){
                $our_value_image = self::uploadFile($request,'our_value_image','our_value');
                $data['our_value_image'] = $our_value_image;
            }
            $home_setting->update($data);
            return redirect()->back()->with('success','Home Page Our Value Section Setting Updated Successfully');
        }
        else{
            $data = $request->validate($this->getOurValueRules());
            if ($request->file('our_value_image')){
                $our_value_image = self::uploadFile($request,'our_value_image','our_value');
                $data['our_value_image'] = $our_value_image;
            }
            HomeSetting::create($data);
            return redirect()->back()->with('success','Home Page Our Value Section Setting Added Successfully');
        }
    }

    private function getOurValueRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.our_value_heading'] = 'required|max:200';
            $rules[$value->code.'.our_value_image_text'] = 'required|max:200';
            $rules[$value->code.'.our_value_image_second_text'] = 'required|max:200';
            $rules[$value->code.'.our_value_button_text'] = 'required|max:200';
        }
        $other_rules = [
            'our_value_image' => 'sometimes|image',
            'our_value_button_link' => 'required',
        ];
        return array_merge($rules,$other_rules);
    }

    public function home_page_zina_products()
    {
        $home_page_setting = HomeSetting::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.home_page.zina_products',compact('home_page_setting','selected_language','all_languages'));
    }

    public function zina_products_update(Request $request)
    {
        $home_setting = HomeSetting::first();
        if ($home_setting){
            $data = $request->validate($this->getZinaProductRules());
            if ($request->file('zina_product_image')){
                $zina_product_image= self::uploadFile($request,'zina_product_image','zina_product');
                $data['zina_product_image'] = $zina_product_image;
            }
            $home_setting->update($data);
            return redirect()->back()->with('success','Home Page Zina Product Section Setting Updated Successfully');
        }
        else{
            $data = $request->validate($this->getZinaProductRules());
            if ($request->file('zina_product_image')){
                $zina_product_image= self::uploadFile($request,'zina_product_image','zina_product');
                $data['zina_product_image'] = $zina_product_image;
            }
            HomeSetting::create($data);
            return redirect()->back()->with('success','Home Page Zina Product Section Setting Added Successfully');
        }
    }

    private function getZinaProductRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.zina_product_heading'] = 'required|max:200';
            $rules[$value->code.'.zina_product_title'] = 'required|max:200';
            $rules[$value->code.'.zina_product_text'] = 'required|max:200';
            $rules[$value->code.'.zina_product_desc'] = 'required|max:500';
        }
        $other_rules = [
            'zina_product_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }

    public function home_page_news()
    {
        $home_page_setting = HomeSetting::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.home_page.news',compact('home_page_setting','selected_language','all_languages'));
    }

    public function news_update(Request $request)
    {
        $home_setting = HomeSetting::first();
        if ($home_setting){
            $data = $request->validate($this->getNewsRules());
            if ($request->file('our_news_image')){
                $our_news_image = self::uploadFile($request,'our_news_image','our_news');
                $data['our_news_image'] = $our_news_image;
            }
            $home_setting->update($data);
            return redirect()->back()->with('success','Home Page News Section Setting Updated Successfully');
        } else{
            $data = $request->validate($this->getNewsRules());
            if ($request->file('our_news_image')){
                $our_news_image = self::uploadFile($request,'our_news_image','our_news');
                $data['our_news_image'] = $our_news_image;
            }
            HomeSetting::create($data);
            return redirect()->back()->with('success','Home Page News Section Setting Added Successfully');
        }
    }

    private function getNewsRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.our_news_title'] = 'required|max:200';
            $rules[$value->code.'.our_news_heading'] = 'required|max:200';
            $rules[$value->code.'.our_news_desc'] = 'required|max:500';
        }
        $other_rules = [
            'our_news_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }

    public function home_page_recipes()
    {
        $home_page_setting = HomeSetting::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.home_page.recipes',compact('home_page_setting','selected_language','all_languages'));
    }

    public function recipes_update(Request $request)
    {
        $home_setting = HomeSetting::first();
        if ($home_setting){
            $data = $request->validate($this->getRecipesRules());
            if ($request->file('our_recipe_top_image')){
                $our_recipe_top_image = self::uploadFile($request,'our_recipe_top_image','our_recipe');
                $data['our_recipe_top_image'] = $our_recipe_top_image;
            }
            if ($request->file('our_recipe_bottom_image_one')){
                $our_recipe_bottom_image_one = self::uploadFile($request,'our_recipe_bottom_image_one','our_recipe');
                $data['our_recipe_bottom_image_one'] = $our_recipe_bottom_image_one;
            }
            if ($request->file('our_recipe_bottom_image_two')){
                $our_recipe_bottom_image_two = self::uploadFile($request,'our_recipe_bottom_image_two','our_recipe');
                $data['our_recipe_bottom_image_two'] = $our_recipe_bottom_image_two;
            }
            if ($request->file('our_recipe_bottom_image_three')){
                $our_recipe_bottom_image_three = self::uploadFile($request,'our_recipe_bottom_image_three','our_recipe');
                $data['our_recipe_bottom_image_three'] = $our_recipe_bottom_image_three;
            }
            $home_setting->update($data);
            return redirect()->back()->with('success','Home Page Recipes Section Setting Updated Successfully');
        } else{
            $data = $request->validate($this->getRecipesRules());
            if ($request->file('our_recipe_top_image')){
                $our_recipe_top_image = self::uploadFile($request,'our_recipe_top_image','our_recipe');
                $data['our_recipe_top_image'] = $our_recipe_top_image;
            }
            if ($request->file('our_recipe_bottom_image_one')){
                $our_recipe_bottom_image_one = self::uploadFile($request,'our_recipe_bottom_image_one','our_recipe');
                $data['our_recipe_bottom_image_one'] = $our_recipe_bottom_image_one;
            }
            if ($request->file('our_recipe_bottom_image_two')){
                $our_recipe_bottom_image_two = self::uploadFile($request,'our_recipe_bottom_image_two','our_recipe');
                $data['our_recipe_bottom_image_two'] = $our_recipe_bottom_image_two;
            }
            if ($request->file('our_recipe_bottom_image_three')){
                $our_recipe_bottom_image_three = self::uploadFile($request,'our_recipe_bottom_image_three','our_recipe');
                $data['our_recipe_bottom_image_three'] = $our_recipe_bottom_image_three;
            }
            HomeSetting::create($data);
            return redirect()->back()->with('success','Home Page Recipes Section Setting Added Successfully');
        }
    }
    private function getRecipesRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.our_recipe_heading'] = 'required|max:200';
            $rules[$value->code.'.our_recipe_bottom_text'] = 'required|max:200';
        }
        $other_rules = [
            'our_recipe_top_image' => 'sometimes|image',
            'our_recipe_bottom_image_one' => 'sometimes|image',
            'our_recipe_bottom_image_two' => 'sometimes|image',
            'our_recipe_bottom_image_three' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }
    public function home_page_navbar()
    {
        $home_navbar_setting = HomeNavbar::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.home_page.navbar',compact('home_navbar_setting','selected_language','all_languages'));
    }

    public function navbar_update(Request $request)
    {
        $home_navbar = HomeNavbar::first();
        if ($home_navbar){
            $data = $request->validate($this->getNavbarRules());
            $home_navbar->update($data);
            return redirect()->back()->with('success','Home Navbar Translation Updated Successfully');
        } else{
            $data = $request->validate($this->getNavbarRules());
            HomeNavbar::create($data);
            return redirect()->back()->with('success','Home Navbar Translation Added Successfully');
        }
    }

    private function getNavbarRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.welcome_menu_text'] = 'required|max:200';
            $rules[$value->code.'.zina_house_menu_text'] = 'required|max:200';
            $rules[$value->code.'.our_products_menu_text'] = 'required|max:200';
            $rules[$value->code.'.our_values_menu_text'] = 'required|max:200';
            $rules[$value->code.'.news_menu_text'] = 'required|max:200';
            $rules[$value->code.'.recipes_menu_text'] = 'required|max:200';
            $rules[$value->code.'.contact_menu_text'] = 'required|max:200';
        }

        return  $rules ;
    }


    // news home page setting

    public function home_page_news_section()
    {
        $home_page_setting = NewsSetting::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.news_page_setting.news_section',compact('home_page_setting','selected_language','all_languages'));
    }


    public function news_section_update(Request $request)
    {
        $home_setting = NewsSetting::first();
        if ($home_setting){
            $data = $request->validate($this->getNewsPageRules());
            if ($request->file('news_bg_image')){
                $news_bg_image = self::uploadFile($request,'news_bg_image','news_setting');
                $data['news_bg_image'] = $news_bg_image;
            }
            $home_setting->update($data);
            return redirect()->back()->with('success','News page Setting Updated Successfully');
        }
        else{
            $data = $request->validate($this->getNewsPageRules());
            if ($request->file('news_bg_image')){
                $news_bg_image = self::uploadFile($request,'news_bg_image','news_setting');
                $data['news_bg_image'] = $news_bg_image;
            }
            NewsSetting::create($data);
            return redirect()->back()->with('success','Home Zina House Section Setting Added Successfully');
        }
    }

    private function getNewsPageRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.top_welcome_text'] = 'required|max:255';
            $rules[$value->code.'.top_news_text'] = 'required|max:255';
            $rules[$value->code.'.news_heading'] = 'required|max:100';

        }
        $other_rules = [
            'news_bg_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }



    // recipe home page setting

    public function recipe_first_section()
    {
        $home_page_setting = RecipeHomePage::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.recipe_page_setting.first_section',compact('home_page_setting','selected_language','all_languages'));
    }


    public function recipe_first_section_update(Request $request)
    {
        $home_setting = RecipeHomePage::first();
        if ($home_setting){
            $data = $request->validate($this->getRecipePageRules());
            if ($request->file('first_section_top_video')){
                $first_section_top_video = self::uploadFile($request,'first_section_top_video','recipe_video');
                $data['first_section_top_video'] = $first_section_top_video;
            }

            if ($request->file('second_section_image')){
                $second_section_image = self::uploadFile($request,'second_section_image','recipes');
                $data['second_section_image'] = $second_section_image;
            }
            $home_setting->update($data);
            return redirect()->back()->with('success','Pecipe page Setting Updated Successfully');
        }
        else{
            $data = $request->validate($this->getRecipePageRules());
            if ($request->file('first_section_top_video')){
                $first_section_top_video = self::uploadFile($request,'first_section_top_video','recipe_video');
                $data['first_section_top_video'] = $first_section_top_video;
            }
            if ($request->file('second_section_image')){
                $second_section_image = self::uploadFile($request,'second_section_image','recipes');
                $data['second_section_image'] = $second_section_image;
            }
            RecipeHomePage::create($data);
        }

    }

    private function getRecipePageRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.second_section_top_text_one'] = 'required|max:255';
            $rules[$value->code.'.second_section_top_text_two'] = 'required|max:255';
            $rules[$value->code.'.second_section_top_heading'] = 'required|max:100';

            $rules[$value->code.'.second_section_paragraph'] = 'required';

            $rules[$value->code.'.second_section_bottom_heading'] = 'required|max:100';

        }
        $other_rules = [
            'first_section_top_video' => 'mimes:mp4,mov,ogg,qt | max:20000',
            'second_section_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }

    // second section

    public function recipe_second_section()
    {
        $home_page_setting = RecipeHomePage::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.recipe_page_setting.second_section',compact('home_page_setting','selected_language','all_languages'));
    }


    public function recipe_second_section_update(Request $request)
    {
        $home_setting = RecipeHomePage::first();
        if ($home_setting){
            $data = $request->validate($this->getRecipeSecRules());
            if ($request->file('fourth_section_image')){
                $fourth_section_image = self::uploadFile($request,'fourth_section_image','recipes');
                $data['fourth_section_image'] = $fourth_section_image;
            }
            $home_setting->update($data);
            return redirect()->back()->with('success','Pecipe page Setting Updated Successfully');
        }
        else{
            $data = $request->validate($this->getRecipeSecRules());
            if ($request->file('fourth_section_image')){
                $fourth_section_image = self::uploadFile($request,'fourth_section_image','recipes');
                $data['fourth_section_image'] = $fourth_section_image;
            }
            RecipeHomePage::create($data);
            return redirect()->back()->with('success','Pecipe page Section Setting Added Successfully');
        }
    }

    private function getRecipeSecRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.third_section_search_heading'] = 'required|max:255';
            $rules[$value->code.'.fourth_section_heading_first'] = 'required|max:255';
            $rules[$value->code.'.fourth_section_heading_sec'] = 'required|max:100';

            $rules[$value->code.'.fourth_section_make_way'] = 'required|max:100';
            $rules[$value->code.'.fourth_section_make_time'] = 'required|max:100';
            $rules[$value->code.'.fourth_section_learn_more_text'] = 'required|max:100';

        }
        $other_rules = [
            'fourth_section_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }

    //product page setting

    public function product_page_first_section()
    {
        $home_page_setting = ProductSetting::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.product_page_setting.first_section',compact('home_page_setting','selected_language','all_languages'));
    }



    public function product_page_first_section_update(Request $request)
    {
        $home_setting = ProductSetting::first();
        if ($home_setting){
            $data = $request->validate($this->getProductPageRules());
            if ($request->file('first_section_top_video')){
                $first_section_top_video = self::uploadFile($request,'first_section_top_video','product_video');
                $data['first_section_top_video'] = $first_section_top_video;
            }

            if ($request->file('first_section_bg_image')){
                $first_section_bg_image = self::uploadFile($request,'first_section_bg_image','products');
                $data['first_section_bg_image'] = $first_section_bg_image;
            }
            $home_setting->update($data);
            return redirect()->back()->with('success','Product page Setting Updated Successfully');
        }
        else{
            $data = $request->validate($this->getProductPageRules());
            if ($request->file('first_section_top_video')){
                $first_section_top_video = self::uploadFile($request,'first_section_top_video','product_video');
                $data['first_section_top_video'] = $first_section_top_video;
            }
            if ($request->file('first_section_bg_image')){
                $first_section_bg_image = self::uploadFile($request,'first_section_bg_image','products');
                $data['first_section_bg_image'] = $first_section_bg_image;
            }
            ProductSetting::create($data);
        }

    }

    private function getProductPageRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.first_section_top_text'] = 'required|max:255';
            $rules[$value->code.'.first_section_top_heading'] = 'required|max:255';
            $rules[$value->code.'.first_section_text_two'] = 'required|max:100';

            $rules[$value->code.'.first_section_paragraph'] = 'required';

            $rules[$value->code.'.first_section_bottom_heading'] = 'required|max:100';

        }
        $other_rules = [
            'first_section_top_video' => 'mimes:mp4,mov,ogg,qt | max:20000',
            'first_section_bg_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }




    public function product_page_second_section()
    {
        $home_page_setting = ProductSetting::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.product_page_setting.second_section',compact('home_page_setting','selected_language','all_languages'));
    }



    public function product_page_second_section_update(Request $request)
    {
        $home_setting = ProductSetting::first();
        if ($home_setting){
            $data = $request->validate($this->getProductSecRules());
            if ($request->file('fourth_section_image')){
                $fourth_section_image = self::uploadFile($request,'fourth_section_image','products');
                $data['fourth_section_image'] = $fourth_section_image;
            }
            $home_setting->update($data);
            return redirect()->back()->with('success','Product page Setting Updated Successfully');
        }
        else{
            $data = $request->validate($this->getProductSecRules());
            if ($request->file('fourth_section_image')){
                $fourth_section_image = self::uploadFile($request,'fourth_section_image','products');
                $data['fourth_section_image'] = $fourth_section_image;
            }
            ProductSetting::create($data);
            return redirect()->back()->with('success','Product page Section Setting Added Successfully');
        }
    }

    private function getProductSecRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.second_section_first_heading'] = 'required|max:255';
            $rules[$value->code.'.second_section_sec_heading'] = 'required|max:255';
            $rules[$value->code.'.second_section_paragraph'] = 'required';

        }
        $other_rules = [
            'second_section_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }



}
