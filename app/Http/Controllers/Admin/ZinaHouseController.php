<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TranslationServiceController;
use App\Models\ZinaHouse;
use App\Traits\imageUploadTrait;
use Illuminate\Http\Request;


class ZinaHouseController extends Controller
{
    use imageUploadTrait;
    public $selected_language ;
    public $all_languages ;
    public function __construct(){
        $translation = new TranslationServiceController();
        $this->selected_language = $translation->selected_language();
        $this->all_languages = $translation->all_languages();
    }
    public function zina_house_section_one()
    {
        $zina_house_setting = ZinaHouse::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.zina_house.one',compact('zina_house_setting','selected_language','all_languages'));
    }
    public function zina_house_section_one_update(Request $request)
    {
        $zina_house = ZinaHouse::first();
        if ($zina_house){
            $data = $request->validate($this->getSectionOneRules());
            if ($request->file('section_1_image')){
                $section_1_image = self::uploadFile($request,'section_1_image','zina_house');
                $data['section_1_image'] = $section_1_image;
            }
            $zina_house->update($data);
            return redirect()->back()->with('success','Zina House Section One Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionOneRules());
            if ($request->file('section_1_image')){
                $section_1_image = self::uploadFile($request,'section_1_image','zina_house');
                $data['section_1_image'] = $section_1_image;
            }
            ZinaHouse::create($data);
            return redirect()->back()->with('success','Zina House Section One Added Successfully');
        }
    }

    private function getSectionOneRules(){
        return [
            'section_1_image' => 'sometimes|image',
        ];
    }
    public function zina_house_section_two()
    {
        $zina_house_setting = ZinaHouse::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.zina_house.two',compact('zina_house_setting','selected_language','all_languages'));
    }
    public function zina_house_section_two_update(Request $request)
    {
        $zina_house = ZinaHouse::first();
        if ($zina_house){
            $data = $request->validate($this->getSectionTwoRules());
            if ($request->file('section_2_bg_image')){
                $section_2_bg_image = self::uploadFile($request,'section_2_bg_image','zina_house');
                $data['section_2_bg_image'] = $section_2_bg_image;
            }
            $zina_house->update($data);
            return redirect()->back()->with('success','Zina House Section Two Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionTwoRules());
            if ($request->file('section_2_bg_image')){
                $section_2_bg_image = self::uploadFile($request,'section_2_bg_image','zina_house');
                $data['section_2_bg_image'] = $section_2_bg_image;
            }
            ZinaHouse::create($data);
            return redirect()->back()->with('success','Zina House Section Two Added Successfully');
        }
    }

    private function getSectionTwoRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.section_2_heading'] = 'required|max:255';
            $rules[$value->code.'.section_2_text'] = 'required|max:255';
            $rules[$value->code.'.section_2_paragraph'] = 'required|max:500';
            $rules[$value->code.'.section_2_bottom_text'] = 'required|max:255';
        }
        $other_rules = [
            'section_2_bg_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }

    public function zina_house_section_four()
    {
        $zina_house_setting = ZinaHouse::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.zina_house.four',compact('zina_house_setting','selected_language','all_languages'));
    }

    public function zina_house_section_four_update(Request $request)
    {
        $zina_house = ZinaHouse::first();
        if ($zina_house){
            $data = $request->validate($this->getSectionFourRules());
            if ($request->file('section_4_side_image')){
                $section_4_side_image = self::uploadFile($request,'section_4_side_image','zina_house');
                $data['section_4_side_image'] = $section_4_side_image;
            }
            $zina_house->update($data);
            return redirect()->back()->with('success','Zina House Section Four Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionFourRules());
            if ($request->file('section_4_side_image')){
                $section_4_side_image = self::uploadFile($request,'section_4_side_image','zina_house');
                $data['section_4_side_image'] = $section_4_side_image;
            }
            ZinaHouse::create($data);
            return redirect()->back()->with('success','Zina House Section Four Added Successfully');
        }
    }

    private function getSectionFourRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.section_4_top_heading'] = 'required|max:200';
            $rules[$value->code.'.section_4_text'] = 'required|max:200';
            $rules[$value->code.'.section_4_paragraph'] = 'required|max:500';
            $rules[$value->code.'.section_4_bottom_heading'] = 'required|max:200';
            $rules[$value->code.'.section_4_last_text'] = 'required|max:200';
        }
        $other_rules = [
            'section_4_side_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }

    public function zina_house_section_five()
    {
        $zina_house_setting = ZinaHouse::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.zina_house.five',compact('zina_house_setting','selected_language','all_languages'));
    }
    public function zina_house_section_five_update(Request $request)
    {
        $zina_house = ZinaHouse::first();
        if ($zina_house){
            $data = $request->validate($this->getSectionFiveRules());
            $zina_house->update($data);
            return redirect()->back()->with('success','Zina House Section Five Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionFiveRules());
            ZinaHouse::create($data);
            return redirect()->back()->with('success','Zina House Section Five Added Successfully');
        }
    }

    private function getSectionFiveRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.section_5_heading'] = 'required|max:200';
            $rules[$value->code.'.section_5_paragraph'] = 'required|max:500';
            $rules[$value->code.'.section_5_ceo_name'] = 'required|max:200';
            $rules[$value->code.'.section_5_bottom_text'] = 'required|max:200';
        }
        return $rules;
    }
    public function zina_house_section_six()
    {
        $zina_house_setting = ZinaHouse::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.zina_house.six',compact('zina_house_setting','selected_language','all_languages'));
    }
    public function zina_house_section_six_update(Request $request)
    {
        $zina_house = ZinaHouse::first();
        if ($zina_house){
            $data = $request->validate($this->getSectionSixRules());
            $zina_house->update($data);
            return redirect()->back()->with('success','Zina House Section Six Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionSixRules());
            ZinaHouse::create($data);
            return redirect()->back()->with('success','Zina House Section Six Added Successfully');
        }
    }
    private function getSectionSixRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.section_6_text'] = 'required|max:200';
            $rules[$value->code.'.section_6_heading'] = 'required|max:200';
            $rules[$value->code.'.section_6_bottom_text'] = 'required|max:200';
            $rules[$value->code.'.section_6_paragraph'] = 'required|max:500';
            $rules[$value->code.'.section_6_results_text'] = 'required|max:200';
            $rules[$value->code.'.section_6_drop_heading'] = 'required|max:200';
            $rules[$value->code.'.section_6_drop_paragraph'] = 'required|max:200';
            $rules[$value->code.'.section_6_co2_heading'] = 'required|max:200';
            $rules[$value->code.'.section_6_co2_paragraph'] = 'required|max:200';
        }
        return $rules;
    }
    public function zina_house_section_seven()
    {
        $zina_house_setting = ZinaHouse::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.zina_house.seven',compact('zina_house_setting','selected_language','all_languages'));
    }
    public function zina_house_section_seven_update(Request $request)
    {
        $zina_house = ZinaHouse::first();
        if ($zina_house){
            $data = $request->validate($this->getSectionSevenRules());
            if ($request->file('section_7_side_image')){
                $section_7_side_image = self::uploadFile($request,'section_7_side_image','zina_house');
                $data['section_7_side_image'] = $section_7_side_image;
            }
            $zina_house->update($data);
            return redirect()->back()->with('success','Zina House Section Seven Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionSevenRules());
            if ($request->file('section_7_side_image')){
                $section_7_side_image = self::uploadFile($request,'section_7_side_image','zina_house');
                $data['section_7_side_image'] = $section_7_side_image;
            }
            ZinaHouse::create($data);
            return redirect()->back()->with('success','Zina House Section Seven Added Successfully');
        }
    }

    private function getSectionSevenRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.section_7_top_text'] = 'required|max:200';
            $rules[$value->code.'.section_7_heading'] = 'required|max:200';
            $rules[$value->code.'.section_7_learn_more'] = 'required|max:200';
            $rules[$value->code.'.section_7_paragraph'] = 'required|max:500';
        }
        $other_rules = [
            'section_7_side_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }
    public function zina_house_section_eight()
    {
        $zina_house_setting = ZinaHouse::first();
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.zina_house.eight',compact('zina_house_setting','selected_language','all_languages'));
    }
    public function zina_house_section_eight_update(Request $request)
    {
        $zina_house = ZinaHouse::first();
        if ($zina_house){
            $data = $request->validate($this->getSectionEightRules());
            if ($request->file('section_8_image')){
                $section_8_image = self::uploadFile($request,'section_8_image','zina_house');
                $data['section_8_image'] = $section_8_image;
            }
            $zina_house->update($data);
            return redirect()->back()->with('success','Zina House Section Eight Updated Successfully');
        }
        else{
            $data = $request->validate($this->getSectionEightRules());
            if ($request->file('section_8_image')){
                $section_8_image = self::uploadFile($request,'section_8_image','zina_house');
                $data['section_8_image'] = $section_8_image;
            }
            ZinaHouse::create($data);
            return redirect()->back()->with('success','Zina House Section Eight Added Successfully');
        }
    }
    private function getSectionEightRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.section_8_top_text'] = 'required|max:200';
            $rules[$value->code.'.section_8_heading'] = 'required|max:200';
            $rules[$value->code.'.section_8_paragraph'] = 'required|max:500';
        }
        $other_rules = [
            'section_8_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }
}
