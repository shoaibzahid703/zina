<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TranslationServiceController;
use App\Models\HomeSlider;
use App\Traits\imageUploadTrait;
use Illuminate\Http\Request;

class HomeSliderController extends Controller
{
    use imageUploadTrait;
    public $selected_language ;
    public $all_languages ;
    public function __construct(){
        $translation = new TranslationServiceController();
        $this->selected_language = $translation->selected_language();
        $this->all_languages = $translation->all_languages();
    }
    public function index()
    {
        $all_sliders= HomeSlider::orderBy('id','desc')->get();
        return view('admin.pages.home_slider.index',compact('all_sliders'));
    }
    public function create()
    {
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.home_slider.create',compact('selected_language','all_languages'));
    }
    public function store(Request $request)
    {
        $data = $request->validate($this->getRules());
        if ($request->file('slider_image')){
            $slider_image = self::uploadFile($request,'slider_image','slider');
            $data['slider_image'] = $slider_image;
        }
        HomeSlider::create($data);
        return redirect()->route('home-slider')->with('success','Slider Data Added Successfully');
    }
    private function getRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.title'] = 'required|max:255';
            $rules[$value->code.'.heading'] = 'required|max:255';
            $rules[$value->code.'.description'] = 'required|max:500';
            $rules[$value->code.'.button_text'] = 'required|max:50';
            $rules[$value->code.'.button_link'] = 'required|max:100';
        }
        $other_rules = [
            'slider_image' => 'required|image',
        ];
        return array_merge($rules,$other_rules);
    }
    public function edit(HomeSlider $home_slider)
    {
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.home_slider.edit',compact('selected_language','all_languages','home_slider'));
    }
    public function update(Request $request,HomeSlider $home_slider)
    {
        $data = $request->validate($this->updateRules());
        if ($request->file('slider_image')){
            $slider_image = self::uploadFile($request,'slider_image','slider');
            $data['slider_image'] = $slider_image;
        }
        $home_slider->update($data);
        return redirect()->route('home-slider')->with('success','Slider Data Update Successfully');
    }

    private function updateRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.title'] = 'required|max:255';
            $rules[$value->code.'.heading'] = 'required|max:255';
            $rules[$value->code.'.description'] = 'required|max:500';
            $rules[$value->code.'.button_text'] = 'required|max:50';
            $rules[$value->code.'.button_link'] = 'required|max:100';
        }
        $other_rules = [
            'slider_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }
    public function delete(HomeSlider $home_slider)
    {
        HomeSlider::destroy($home_slider->id);
        return redirect()->route('home-slider')->with('success','Slider Data Deleted Successfully');
    }
}
