<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\imageUploadTrait;
use App\Http\Controllers\TranslationServiceController;
use App\Models\Product;

class HomePageProductController extends Controller
{

    use imageUploadTrait;

    public function __construct(){
        $translation = new TranslationServiceController();
        $this->selected_language = $translation->selected_language();
        $this->all_languages = $translation->all_languages();
    }

    public function index()
    {
        $all_products= Product::orderBy('id','desc')->get();
        return view('admin.pages.products.index', compact('all_products'));
    }

    public function create()
    {
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.products.create',compact('selected_language','all_languages'));
    }

    public function store(Request $request){

        $data = $request->validate($this->getRules());

        if ($request->hasFile('product_image')) {
            $product_image = self::uploadFile($request, fileName: 'product_image', folderName: 'products');
            $data['product_image'] = $product_image;
        }

        Product::create($data);

        return redirect()->route('home-products')->with('success', 'Product Data Added Successfully');
    }


    private function getRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.title'] = 'required|max:255';
            $rules[$value->code.'.description'] = 'required|max:500';
        }
        $other_rules = [
            'product_image' => 'required|image',
        ];
        return array_merge($rules,$other_rules);
    }


    public function edit(Product $product)
    {
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.products.edit',compact('selected_language','all_languages','product'));
    }



    public function update(Request $request,Product $product)
    {
        $data = $request->validate($this->updateRules());
        if ($request->file('product_image')){
            $product_image = self::uploadFile($request,'product_image','products');
            $data['product_image'] = $product_image;
        }
        $product->update($data);
        return redirect()->route('home-products')->with('success','Product Data Update Successfully');
    }

    private function updateRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.title'] = 'required|max:255';
            $rules[$value->code.'.description'] = 'required|max:500';

        }
        $other_rules = [
            'product_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }

    public function delete(Product $product)
    {
        Product::destroy($product->id);
        return redirect()->back()->with('success','Product Data Deleted Successfully');
    }

}
