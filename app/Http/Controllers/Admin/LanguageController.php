<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function index()
    {
        $languages = Language::orderBy('id','desc')->get();
        return view('admin.pages.language.index',compact('languages'));
    }
    public function create()
    {
        return view('admin.pages.language.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required',
        ]);
        if (isset($request->is_default)){
           $all_languages =  Language::all();
           foreach ($all_languages as $language){
               $language->update([
                  'is_default' => 0
               ]);
           }
        }

        Language::create([
            'name' => $request->name,
            'code' => $request->code,
            'status' => $request->status ?? 0,
            'is_default' => $request->is_default ?? 0,
        ]);

        return redirect()->route('languages')->with('success','Language Added Successfully');
    }
    public function edit(Language $language)
    {
        return view('admin.pages.language.edit',compact('language'));
    }
    public function update(Request $request,Language $language)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required',
        ]);
        if (isset($request->is_default)){
            $all_languages =  Language::all();
            foreach ($all_languages as $language){
                $language->update([
                    'is_default' => 0
                ]);
            }
        }

        $language->update([
            'name' => $request->name,
            'code' => $request->code,
            'status' => $request->status ?? 0,
            'is_default' => $request->is_default ?? 0,
        ]);

        return redirect()->route('languages')->with('success','Language Added Successfully');
    }
    public function delete(Language $language)
    {
         Language::destroy($language->id);
        return redirect()->route('languages')->with('success','Language Deleted Successfully');

    }
}
