<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TranslationServiceController;
use App\Models\LegalPage;
use App\Traits\imageUploadTrait;
use Illuminate\Http\Request;

class LegalPageController extends Controller
{
    use imageUploadTrait;

    public function __construct(){
        $translation = new TranslationServiceController();
        $this->selected_language = $translation->selected_language();
        $this->all_languages = $translation->all_languages();
    }
    public function index(Request $request)
    {
        $all_legal_pages = LegalPage::orderBy('id','desc')->get();
        return view('admin.pages.legal-pages.index',compact('all_legal_pages'));
    }

    public function create()
    {
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.legal-pages.create',compact('selected_language','all_languages'));
    }

    public function save(Request $request)
    {
        $data = $request->validate($this->getRules());
        if ($request->hasFile('page_image')) {
            $page_image = self::uploadFile($request, fileName: 'page_image', folderName: 'pages');
            $data['page_image'] = $page_image;
        }
        $data['slug'] =  $this->slugify($request->en['name']);
        LegalPage::create($data);
        return redirect()->route('legal_pages')->with('success', 'Page Added Successfully');
    }

    private function getRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.name'] = 'required|max:255';
            $rules[$value->code.'.description'] = 'required';
        }
        $other_rules = [
            'page_image' => 'required|image',
        ];
        return array_merge($rules,$other_rules);
    }

    public function edit(LegalPage $page)
    {
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.legal-pages.edit',compact('page','selected_language','all_languages'));
    }

    public function update(Request $request,LegalPage $page)
    {
        $data = $request->validate($this->updateRules());
        if ($request->hasFile('page_image')) {
            $page_image = self::uploadFile($request, fileName: 'page_image', folderName: 'pages');
            $data['page_image'] = $page_image;
        }
        if ($request->en['name'] != $page->name){
            $data['slug'] =  $this->slugify($request->en['name']);
        }
        $page->update($data);
        return redirect()->route('legal_pages')->with('success','Product Data Update Successfully');
    }
    private function updateRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.name'] = 'required|max:255';
            $rules[$value->code.'.description'] = 'required';

        }
        $other_rules = [
            'page_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }

    public function delete(LegalPage $page)
    {
        LegalPage::destroy($page->id);
        return redirect()->route('legal_pages')->with('success','Legal Page Deleted Successfully');
    }


    function slugify($str, $delimiter = '-')
    {
        return strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
    }
}
