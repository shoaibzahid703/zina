<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\imageUploadTrait;
use App\Http\Controllers\TranslationServiceController;
use App\Models\OurNew;

class HomePageNewsController extends Controller
{
    use imageUploadTrait;


    public function __construct(){
        $translation = new TranslationServiceController();
        $this->selected_language = $translation->selected_language();
        $this->all_languages = $translation->all_languages();
    }

    public function index()
    {
        $all_news= OurNew::orderBy('id','desc')->get();
        return view('admin.pages.home_page_news.index', compact('all_news'));
    }

    public function create()
    {
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.home_page_news.create',compact('selected_language','all_languages'));
    }

    public function store(Request $request){

        $data = $request->validate($this->getRules());

        if ($request->hasFile('news_image')) {
            $news_image = self::uploadFile($request, fileName: 'news_image', folderName: 'news_image');
            $data['news_image'] = $news_image;
        }
        OurNew::create($data);
        return redirect()->route('our-news')->with('success', 'News Data Added Successfully');
    }


    private function getRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.title'] = 'required|max:255';
            $rules[$value->code.'.description'] = 'required|max:500';
        }
        $other_rules = [
            'news_image' => 'required|image',
        ];
        return array_merge($rules,$other_rules);
    }


    public function edit(OurNew $our_news)
    {
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.home_page_news.edit',compact('selected_language','all_languages','our_news'));
    }



    public function update(Request $request,OurNew $our_news)
    {
        $data = $request->validate($this->updateRules());
        if ($request->file('news_image')){
            $news_image = self::uploadFile($request,'news_image','news_image');
            $data['news_image'] = $news_image;
        }
        $our_news->update($data);
        return redirect()->route('our-news')->with('success','News Data Update Successfully');
    }

    private function updateRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.title'] = 'required|max:255';
            $rules[$value->code.'.description'] = 'required|max:500';

        }
        $other_rules = [
            'news_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }

    public function delete(OurNew $our_news)
    {
        OurNew::destroy($our_news->id);
        return redirect()->route('our-news')->with('success','News Data Deleted Successfully');
    }
}
