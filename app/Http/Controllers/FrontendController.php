<?php

namespace App\Http\Controllers;

use App\Models\ContactQuery;
use App\Models\HomeNavbar;
use App\Models\HomeSetting;
use App\Models\HomeSlider;
use App\Models\LegalPage;
use App\Models\NewsSetting;
use App\Models\OurNew;
use App\Models\OurValue;
use App\Models\Product;
use App\Models\ProductSetting;
use App\Models\Recipe;
use App\Models\RecipeHomePage;
use App\Models\ZinaHouse;
use App\Models\ZinaHouseSlider;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        $home_page_setting = HomeSetting::first();
        $all_sliders = HomeSlider::all();
        return view('frontend.pages.index',compact('home_page_setting','all_sliders'));
    }

    public function zina_house()
    {
        $zina_house_setting = ZinaHouse::first();
        $zina_house_sliders = ZinaHouseSlider::all();
        $zina_house_sliders_years = ZinaHouseSlider::pluck('slider_year');
        $products = Product::limit(6)->get();
        return view('frontend.pages.zina_house',compact('products','zina_house_setting','zina_house_sliders','zina_house_sliders_years'));
    }

    public function our_values()
    {
        $our_values_setting = OurValue::first();
        $zina_house_sliders = ZinaHouseSlider::all();
        $zina_house_sliders_years = ZinaHouseSlider::pluck('slider_year');
        return view('frontend.pages.our_values',compact('our_values_setting','zina_house_sliders','zina_house_sliders_years'));
    }

    public function products()
    {
        $home_page_setting = HomeSetting::first();
        $all_products = Product::all();
        $product_page_setting = ProductSetting::first();
        return view('frontend.pages.products',compact('home_page_setting','all_products', 'product_page_setting'));
    }

    public function product_detail(Product $product)
    {
        return view('frontend.pages.product_detail', compact('product'));
    }

    public function news()
    {
        $home_page_setting = HomeSetting::first();
        $all_news = OurNew::orderBy('id','desc')->paginate(10);
        $news_page_setting = NewsSetting::first();

        return view('frontend.pages.news', compact('all_news', 'home_page_setting', 'news_page_setting'));
    }

    public function news_detail(OurNew $news)
    {
        return view('frontend.pages.news_detail', compact('news'));
    }

    public function recipes()
    {
        $home_page_setting = HomeSetting::first();
        $all_recipes = Recipe::orderBy('id','desc')->paginate(10);
        $recipe_setting = RecipeHomePage::all();
        return view('frontend.pages.recipes', compact('all_recipes', 'home_page_setting', 'recipe_setting'));
    }

    public function recipe_detail(Recipe $recipe)
    {
        return view('frontend.pages.recipes_detail', compact('recipe'));
    }

    public function contact()
    {
        return view('frontend.pages.contact');
    }
    public function contact_save(Request $request)
    {
        $request->validate([
            'name' => 'required|max:50',
            'email' => 'required|email',
            'phone_no' => 'required|max:50',
            'message' => 'required|max:500',
        ]);

         ContactQuery::create([
            'name' => $request->name,
            'email' => $request->email ,
            'phone_no' => $request->phone_no ,
            'message' => $request->message ,
        ]);

        return redirect()->back()->with('success','Contact Query Sent Successfully');
    }

    public function page(LegalPage $legal_page)
    {
        return view('frontend.pages.legal-page',compact('legal_page'));
    }
}
