<?php

namespace App\Http\Controllers;

use App\Models\HomeSlider;
use App\Models\ZinaHouseSlider;
use App\Traits\imageUploadTrait;
use Illuminate\Http\Request;

class ZinaHouseSliderController extends Controller
{
    use imageUploadTrait;
    public $selected_language ;
    public $all_languages ;
    public function __construct(){
        $translation = new TranslationServiceController();
        $this->selected_language = $translation->selected_language();
        $this->all_languages = $translation->all_languages();
    }
    public function index()
    {
        $all_sliders = ZinaHouseSlider::orderBy('id','desc')->get();
        return view('admin.pages.zina_house_slider.index',compact('all_sliders'));
    }
    public function create()
    {
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.zina_house_slider.create',compact('selected_language','all_languages'));
    }
    public function store(Request $request)
    {
        $data = $request->validate($this->getRules());
        if ($request->file('slider_image')){
            $slider_image = self::uploadFile($request,'slider_image','zina_house_slider');
            $data['slider_image'] = $slider_image;
        }
        ZinaHouseSlider::create($data);
        return redirect()->route('zina_house_slider')->with('success','Slider Data Added Successfully');
    }

    private function getRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.slider_title'] = 'required|max:255';
            $rules[$value->code.'.slider_description'] = 'required|max:500';
        }
        $other_rules = [
            'slider_image' => 'required|image',
            'slider_year' => 'required|max:50',
        ];
        return array_merge($rules,$other_rules);
    }

    public function edit(ZinaHouseSlider $zina_house_slider)
    {
        $selected_language = $this->selected_language ;
        $all_languages =  $this->all_languages;
        return view('admin.pages.zina_house_slider.edit',compact('selected_language','all_languages','zina_house_slider'));
    }
    public function update(Request $request,ZinaHouseSlider $zina_house_slider)
    {
        $data = $request->validate($this->updateRules());
        if ($request->file('slider_image')){
            $slider_image = self::uploadFile($request,'slider_image','zina_house_slider');
            $data['slider_image'] = $slider_image;
        }
        $zina_house_slider->update($data);
        return redirect()->route('zina_house_slider')->with('success','Slider Data Updated Successfully');
    }
    public function delete(ZinaHouseSlider $zina_house_slider)
    {
        ZinaHouseSlider::destroy($zina_house_slider->id);
        return redirect()->route('zina_house_slider')->with('success','Slider Data Deleted Successfully');

    }

    private function updateRules(){
        $all_languages = $this->all_languages;
        $rules = array();
        foreach ($all_languages as  $value){
            $rules[$value->code.'.slider_title'] = 'required|max:255';
            $rules[$value->code.'.slider_description'] = 'required|max:500';
        }
        $other_rules = [
            'slider_image' => 'sometimes|image',
        ];
        return array_merge($rules,$other_rules);
    }
}
