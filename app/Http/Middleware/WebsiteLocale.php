<?php

namespace App\Http\Middleware;

use App\Models\Language;
use App\Models\Translation;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class WebsiteLocale
{

    public function handle(Request $request, Closure $next)
    {
        if(Session::has('selected_language')){
            $translation =  Language::find(Session::get('selected_language'));
            App::setLocale($translation->code);
        } else{
            $translation =  Language::all()->where("is_default", "=", 1)->first();
            App::setLocale($translation->code);
        }
        return $next($request);
    }
}
