<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeNavbar extends Model  implements TranslatableContract
{
    use HasFactory,Translatable;

    protected $guarded = ['id'];

    public array $translatedAttributes = [
        'welcome_menu_text',
        'zina_house_menu_text',
        'our_products_menu_text',
        'our_values_menu_text',
        'news_menu_text',
        'recipes_menu_text',
        'contact_menu_text',
    ];
}
