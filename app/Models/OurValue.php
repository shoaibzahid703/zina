<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OurValue extends Model implements TranslatableContract
{
    use HasFactory,Translatable;

    protected $guarded = ['id'];


    public array $translatedAttributes = [
        'section_2_heading',
        'section_2_text',
        'section_2_paragraph',
        'section_2_bottom_text',
        'section_3_top_heading',
        'section_3_text',
        'section_3_paragraph',
        'section_3_bottom_heading',
        'section_3_last_text',
        'section_4_heading',
        'section_4_paragraph',
        'section_4_ceo_name',
        'section_4_bottom_text',
        'section_5_text',
        'section_5_heading',
        'section_5_bottom_text',
        'section_5_paragraph',
        'section_5_results_text',
        'section_5_drop_heading',
        'section_5_drop_paragraph',
        'section_5_co2_heading',
        'section_5_co2_paragraph',
        'section_6_top_text',
        'section_6_heading',
        'section_6_paragraph',
        'section_6_learn_more',
        'section_7_top_text',
        'section_7_paragraph',
        'section_7_heading',
    ];


}
