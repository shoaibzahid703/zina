<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeSetting extends Model implements TranslatableContract
{
    use HasFactory,Translatable;

    protected $guarded = ['id'];

    public array $translatedAttributes = [
        'zina_house_title',
        'zina_house_image_text',
        'zina_house_button_text',
        'our_product_heading',
        'our_product_point_one_title',
        'our_product_point_one_desc',
        'our_product_point_two_title',
        'our_product_point_two_desc',
        'our_product_point_three_title',
        'our_product_point_three_desc',
        'our_product_point_four_title',
        'our_product_point_four_desc',
        'our_value_heading',
        'our_value_image_text',
        'our_value_image_second_text',
        'our_value_button_text',
        'zina_product_heading',
        'zina_product_title',
        'zina_product_text',
        'zina_product_desc',
        'our_news_title',
        'our_news_heading',
        'our_news_desc',
        'our_recipe_heading',
        'our_recipe_bottom_text',
    ];
}
