<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecipeHomePage extends Model implements TranslatableContract
{
    use HasFactory,Translatable;

    protected $guarded = ['id'];


    public array $translatedAttributes = [
        'second_section_top_text_one',
        'second_section_top_text_two',
        'second_section_top_heading',

        'second_section_paragraph',
        'second_section_bottom_heading',

        'third_section_search_heading',

        'fourth_section_heading_first',
        'fourth_section_heading_sec',
        'fourth_section_make_way',
        'fourth_section_learn_more_text',
    ];
}
