<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class FooterSetting extends Model implements TranslatableContract
{
    use HasFactory,Translatable;

    protected $guarded = ['id'];

    public array $translatedAttributes = [
        'footer_description',
        'contact_us_title',
        'address_title',
        'phone_title',
        'email_title',
        'quick_link_title',
        'social_link_title',
    ];
}
