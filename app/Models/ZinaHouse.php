<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZinaHouse extends Model  implements TranslatableContract
{
    use HasFactory,Translatable;

    protected $guarded = ['id'];


    public array $translatedAttributes = [

        'section_2_heading',
        'section_2_text',
        'section_2_paragraph',
        'section_2_bottom_text',
        'section_3_heading',
        'section_3_paragraph',
        'section_4_top_heading',
        'section_4_text',
        'section_4_paragraph',
        'section_4_bottom_heading',
        'section_4_last_text',
        'section_5_heading',
        'section_5_paragraph',
        'section_5_ceo_name',
        'section_5_bottom_text',
        'section_6_text',
        'section_6_heading',
        'section_6_bottom_text',
        'section_6_paragraph',
        'section_6_results_text',
        'section_6_drop_heading',
        'section_6_drop_paragraph',
        'section_6_co2_heading',
        'section_6_co2_paragraph',
        'section_7_top_text',
        'section_7_head',
        'section_7_paragraph',
        'section_7_learn_more',
        'section_8_top_text',
        'section_8_paragraph',
        'section_8_heading',
    ];
}
