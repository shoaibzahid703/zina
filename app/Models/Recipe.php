<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model implements TranslatableContract
{
    use HasFactory,Translatable;

    protected $guarded = [];

    public array $translatedAttributes = [
        'title',
        'description',
        'make_time',
        'make_way',
    ];
}
