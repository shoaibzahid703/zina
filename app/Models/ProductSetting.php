<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductSetting extends Model implements TranslatableContract
{
    use HasFactory,Translatable;

    protected $guarded = ['id'];


    public array $translatedAttributes = [
        'first_section_top_text',
        'first_section_top_heading',
        'first_section_text_two',
        'first_section_paragraph',
        'first_section_bottom_heading',


        'second_section_first_heading',
        'second_section_sec_heading',
        'second_section_paragraph',
    ];
}
