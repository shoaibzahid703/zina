<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeNavbarTranslation extends Model
{
    use HasFactory;

    use HasFactory;

    protected $guarded = ['id'];
    public $timestamps = false;
}
