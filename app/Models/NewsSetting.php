<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsSetting extends Model implements TranslatableContract
{
    use HasFactory,Translatable;

    protected $guarded = ['id'];


    public array $translatedAttributes = [
        'top_welcome_text',
        'top_news_text',
        'news_heading',
    ];
}
