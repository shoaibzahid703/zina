<?php

use App\Models\Admin;
use App\Models\User;
use Laravolt\Avatar\Avatar;

function active_class( $routes ): string
{
    $routesArray = explode( ',', $routes );
    if ( in_array( \Request::route()->getName(), $routesArray ) ) {
        return 'active';
    } else {
        return '';
    }

}


function get_first_letters($str){
    $ret = '';
    foreach (explode(' ', $str) as $word)
        $ret .= strtoupper($word[0]);
    return $ret;
}


