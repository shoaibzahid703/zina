<?php

namespace App\Providers;

use App\Models\FooterSetting;
use App\Models\HomeNavbar;
use App\Models\Language;
use App\Models\Setting;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $settings = Setting::first();
        $front_footer_setting =  FooterSetting::first();
        $front_navbar =  HomeNavbar::first();
        $front_languages =  Language::where('status',1)->get();

        view()->share('front_footer_setting', $front_footer_setting);
        view()->share('front_languages', $front_languages);
        view()->share('settings', $settings);
        view()->share('front_navbar', $front_navbar);
    }
}
