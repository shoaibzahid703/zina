<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_setting_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_setting_id')
                ->constrained('product_settings')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->string('locale')->index();

            $table->string('first_section_top_text')->nullable();
            $table->string('first_section_top_heading')->nullable();

            $table->string('first_section_text_two')->nullable();
            $table->longText('first_section_paragraph')->nullable();

            $table->string('first_section_bottom_heading')->nullable();

            $table->string('second_section_first_heading')->nullable();
            $table->string('second_section_sec_heading')->nullable();
            $table->longText('second_section_paragraph')->nullable();
            
            $table->unique(['product_setting_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_setting_translations');
    }
};
