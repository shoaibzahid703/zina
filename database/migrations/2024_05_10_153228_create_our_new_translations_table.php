<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('our_new_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('our_new_id')
                ->constrained('our_news')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->string('locale')->index();
            $table->string('title');
            $table->longText('description');

            $table->unique(['our_new_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('our_new_translations');
    }
};
