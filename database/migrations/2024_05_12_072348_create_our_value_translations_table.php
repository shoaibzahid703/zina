<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('our_value_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('our_value_id')
                ->constrained('our_values')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->string('locale')->index();

            $table->string('section_2_heading')->nullable();
            $table->string('section_2_text')->nullable();
            $table->longText('section_2_paragraph')->nullable();
            $table->string('section_2_bottom_text')->nullable();

            $table->string('section_3_top_heading')->nullable();
            $table->string('section_3_text')->nullable();
            $table->longText('section_3_paragraph')->nullable();
            $table->string('section_3_bottom_heading')->nullable();
            $table->string('section_3_last_text')->nullable();

            $table->string('section_4_heading')->nullable();
            $table->longText('section_4_paragraph')->nullable();
            $table->string('section_4_ceo_name')->nullable();
            $table->string('section_4_bottom_text')->nullable();

            $table->string('section_5_text')->nullable();
            $table->string('section_5_heading')->nullable();
            $table->string('section_5_bottom_text')->nullable();
            $table->longText('section_5_paragraph')->nullable();
            $table->string('section_5_results_text')->nullable();
            $table->string('section_5_drop_heading')->nullable();
            $table->longText('section_5_drop_paragraph')->nullable();
            $table->string('section_5_co2_heading')->nullable();
            $table->longText('section_5_co2_paragraph')->nullable();

            $table->string('section_6_top_text')->nullable();
            $table->string('section_6_heading')->nullable();
            $table->longText('section_6_paragraph')->nullable();
            $table->string('section_6_learn_more')->nullable();

            $table->string('section_7_top_text')->nullable();
            $table->longText('section_7_paragraph')->nullable();
            $table->string('section_7_heading')->nullable();

            $table->unique(['our_value_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('our_value_translations');
    }
};
