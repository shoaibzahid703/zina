<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zina_house_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('zina_house_id')
                ->constrained('zina_houses')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->string('locale')->index();

            $table->string('section_2_heading')->nullable();
            $table->string('section_2_text')->nullable();
            $table->longText('section_2_paragraph')->nullable();
            $table->string('section_2_bottom_text')->nullable();


            $table->string('section_4_top_heading')->nullable();
            $table->string('section_4_text')->nullable();
            $table->longText('section_4_paragraph')->nullable();
            $table->string('section_4_bottom_heading')->nullable();
            $table->string('section_4_last_text')->nullable();

            $table->string('section_5_heading')->nullable();
            $table->longText('section_5_paragraph')->nullable();
            $table->string('section_5_ceo_name')->nullable();
            $table->string('section_5_bottom_text')->nullable();

            $table->string('section_6_text')->nullable();
            $table->string('section_6_heading')->nullable();
            $table->string('section_6_bottom_text')->nullable();
            $table->longText('section_6_paragraph')->nullable();
            $table->string('section_6_results_text')->nullable();
            $table->string('section_6_drop_heading')->nullable();
            $table->longText('section_6_drop_paragraph')->nullable();
            $table->string('section_6_co2_heading')->nullable();
            $table->longText('section_6_co2_paragraph')->nullable();

            $table->string('section_7_top_text')->nullable();
            $table->string('section_7_heading')->nullable();
            $table->longText('section_7_paragraph')->nullable();
            $table->string('section_7_learn_more')->nullable();

            $table->string('section_8_top_text')->nullable();
            $table->longText('section_8_paragraph')->nullable();
            $table->string('section_8_heading')->nullable();

            $table->unique(['zina_house_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zina_house_translations');
    }
};
