<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zina_houses', function (Blueprint $table) {
            $table->id();
            $table->string('section_1_image')->nullable();
            $table->string('section_2_bg_image')->nullable();
            $table->string('section_3_image')->nullable();
            $table->string('section_4_side_image')->nullable();
            $table->string('section_7_side_image')->nullable();
            $table->string('section_8_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zina_houses');
    }
};
