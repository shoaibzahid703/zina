<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_home_page_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('recipe_home_page_id')
                ->constrained('recipe_home_pages')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->string('locale')->index();

            $table->string('second_section_top_text_one')->nullable();
            $table->string('second_section_top_text_two')->nullable();
            $table->string('second_section_top_heading')->nullable();
            $table->longText('second_section_paragraph')->nullable();
            $table->string('second_section_bottom_heading')->nullable();


            $table->string('third_section_search_heading')->nullable();

            $table->string('fourth_section_heading_first')->nullable();
            $table->string('fourth_section_heading_sec')->nullable();
            $table->string('fourth_section_make_way')->nullable();
            $table->string('fourth_section_make_time')->nullable();
            $table->string('fourth_section_learn_more_text')->nullable();
           
            $table->unique(['recipe_home_page_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_home_page_translations');
    }
};
