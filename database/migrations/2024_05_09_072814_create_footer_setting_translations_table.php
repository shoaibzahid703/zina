<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('footer_setting_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('footer_setting_id')
                ->constrained('footer_settings')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('locale')->index();

            $table->string('footer_description');
            $table->string('contact_us_title');
            $table->string('address_title');
            $table->string('phone_title');
            $table->string('email_title');
            $table->string('quick_link_title');
            $table->string('social_link_title');

            $table->unique(['footer_setting_id', 'locale']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('footer_setting_translations');
    }
};
