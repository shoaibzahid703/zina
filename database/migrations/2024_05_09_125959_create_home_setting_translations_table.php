<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_setting_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('home_setting_id')
                ->constrained('home_settings')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->string('locale')->index();

            $table->string('zina_house_title')->nullable();
            $table->string('zina_house_image_text')->nullable();
            $table->string('zina_house_button_text')->nullable();

            $table->string('our_product_heading')->nullable();
            $table->string('our_product_point_one_title')->nullable();
            $table->string('our_product_point_one_desc')->nullable();
            $table->string('our_product_point_two_title')->nullable();
            $table->string('our_product_point_two_desc')->nullable();
            $table->string('our_product_point_three_title')->nullable();
            $table->string('our_product_point_three_desc')->nullable();
            $table->string('our_product_point_four_title')->nullable();
            $table->string('our_product_point_four_desc')->nullable();

            $table->string('our_value_heading')->nullable();
            $table->string('our_value_image_text')->nullable();
            $table->string('our_value_image_second_text')->nullable();
            $table->string('our_value_button_text')->nullable();

            $table->string('zina_product_heading')->nullable();
            $table->string('zina_product_title')->nullable();
            $table->string('zina_product_text')->nullable();
            $table->longText('zina_product_desc')->nullable();

            $table->string('our_news_title')->nullable();
            $table->string('our_news_heading')->nullable();
            $table->longText('our_news_desc')->nullable();

            $table->string('our_recipe_heading')->nullable();
            $table->string('our_recipe_bottom_text')->nullable();

            $table->unique(['home_setting_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_setting_translations');
    }
};
