<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_setting_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('news_setting_id')
                ->constrained('news_settings')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->string('locale')->index();

            $table->string('top_welcome_text');
            $table->string('top_news_text');
            $table->longText('news_heading');
           
            $table->unique(['news_setting_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_setting_translations');
    }
};
