<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('our_values', function (Blueprint $table) {
            $table->id();
            $table->string('section_1_image')->nullable();
            $table->string('section_2_bg_image')->nullable();
            $table->string('section_3_side_image')->nullable();
            $table->string('section_6_side_image')->nullable();
            $table->string('section_7_side_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('our_values');
    }
};
