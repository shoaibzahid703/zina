<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_settings', function (Blueprint $table) {
            $table->id();
            $table->string('zina_house_image')->nullable();
            $table->string('our_product_image')->nullable();
            $table->string('our_value_image')->nullable();
            $table->string('zina_product_image')->nullable();
            $table->string('our_news_image')->nullable();

            $table->string('our_recipe_top_image')->nullable();
            $table->string('our_recipe_bottom_image_one')->nullable();
            $table->string('our_recipe_bottom_image_two')->nullable();
            $table->string('our_recipe_bottom_image_three')->nullable();

            $table->string('zina_house_button_link')->nullable();
            $table->string('our_value_button_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_settings');
    }
};
