<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zina_house_slider_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('zina_house_slider_id')
                ->constrained('zina_house_sliders')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->string('locale')->index();

            $table->string('slider_title');
            $table->longText('slider_description');

            $table->unique(['locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zina_house_slider_translations');
    }
};
