<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_home_pages', function (Blueprint $table) {
            $table->id();
            $table->longText('first_section_top_video')->nullable();
            $table->longText('second_section_image')->nullable();
            $table->longText('fourth_section_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_home_pages');
    }
};
