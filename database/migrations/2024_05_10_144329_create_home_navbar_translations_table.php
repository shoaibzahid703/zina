<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_navbar_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('home_navbar_id')
                ->constrained('home_navbars')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->string('locale')->index();

            $table->string('welcome_menu_text');
            $table->string('zina_house_menu_text');
            $table->string('our_products_menu_text');
            $table->string('our_values_menu_text');
            $table->string('news_menu_text');
            $table->string('recipes_menu_text');
            $table->string('contact_menu_text');


            $table->unique(['home_navbar_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_navbar_translations');
    }
};
