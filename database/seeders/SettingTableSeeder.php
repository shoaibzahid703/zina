<?php

namespace Database\Seeders;

use App\Models\Setting;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new Setting();
        $setting->seo_tags = 'DELICIOUS RECIPES,TOMATOES';
        $setting->site_description = 'DISCOVER OUR DELICIOUS RECIPES WITH TOMATOES';
        $setting->save();
    }
}
