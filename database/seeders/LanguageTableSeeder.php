<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Language();
        $admin->name = 'English';
        $admin->code = 'en';
        $admin->status = '1';
        $admin->is_default = '1';
        $admin->save();


        $admin = new Language();
        $admin->name = 'French';
        $admin->code = 'fr';
        $admin->status = '1';
        $admin->is_default = '0';
        $admin->save();
    }
}
