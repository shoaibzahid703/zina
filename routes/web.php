<?php

use App\Http\Controllers\FrontendController;
use App\Http\Controllers\TranslationServiceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [FrontendController::class, 'index'])->name('welcome');
Route::get('zina-house', [FrontendController::class, 'zina_house'])->name('zina_house');
Route::get('our-values', [FrontendController::class, 'our_values'])->name('our_values');
Route::get('products', [FrontendController::class, 'products'])->name('home-page-products');
Route::get('product/detail/{product}', [FrontendController::class, 'product_detail'])->name('product_detail');
Route::get('news', [FrontendController::class, 'news'])->name('home-page-news');
Route::get('news/detail/{news}', [FrontendController::class, 'news_detail'])->name('news_detail');
Route::get('recipes', [FrontendController::class, 'recipes'])->name('home-page-recipes');
Route::get('recipes/detail/{recipe}', [FrontendController::class, 'recipe_detail'])->name('recipe_detail');
Route::get('contact', [FrontendController::class, 'contact'])->name('contact');
Route::get('contact', [FrontendController::class, 'contact'])->name('contact');
Route::post('contact-save', [FrontendController::class, 'contact_save'])->name('contact_save');

Auth::routes(['register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::post('change/language', [TranslationServiceController::class, 'change_language'])->name('change_language');
Route::get('/{legal_page:slug}', [FrontendController::class, 'page'])->name('page');

