<?php


use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\FooterController;
use App\Http\Controllers\Admin\HomePageNewsController;
use App\Http\Controllers\Admin\HomePageProductController;
use App\Http\Controllers\Admin\HomePageRecipeController;
use App\Http\Controllers\Admin\HomePageSettingController;
use App\Http\Controllers\Admin\HomeSliderController;
use App\Http\Controllers\Admin\LanguageController;
use App\Http\Controllers\Admin\LegalPageController;
use App\Http\Controllers\Admin\OurValueController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\ZinaHouseController;
use App\Http\Controllers\ZinaHouseSliderController;


Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin_dashboard');


Route::get( 'site-settings', [SettingController::class, 'site_setting'] )->name( 'site_settings');
Route::post( 'site-settings-update', [SettingController::class, 'site_setting_update'] )->name( 'site_settings.update');

Route::get( 'profile', [ProfileController::class, 'index'] )->name( 'adminProfile' );
Route::post( 'profile/update', [ProfileController::class, 'update'] )->name( 'adminProfileUpdate' );


Route::get( '/languages', [LanguageController::class, 'index'] )->name( 'languages' );
Route::get( '/language/add', [LanguageController::class, 'create'] )->name( 'language_create' );
Route::post( '/language/store', [LanguageController::class, 'store'] )->name( 'language_store' );
Route::get( '/language/{language}', [LanguageController::class, 'edit'] )->name( 'language_edit' );
Route::post( '/language/update/{language}', [LanguageController::class, 'update'] )->name( 'language_update' );
Route::get( '/language/delete/{language}', [LanguageController::class, 'delete'] )->name( 'language_delete' );


Route::get( 'footer-setting', [FooterController::class, 'index'] )->name('footer_setting');
Route::post( 'footer-setting-update', [FooterController::class, 'update'] )->name( 'footer_setting_update');


Route::get( '/home-slider', [HomeSliderController::class, 'index'] )->name( 'home-slider' );
Route::get( '/home_slider/add', [HomeSliderController::class, 'create'] )->name( 'home_slider_create' );
Route::post( '/home_slider/store', [HomeSliderController::class, 'store'] )->name( 'home_slider_store' );
Route::get( '/home_slider/{home_slider}', [HomeSliderController::class, 'edit'] )->name( 'home_slider_edit' );
Route::post( '/home_slider/update/{home_slider}', [HomeSliderController::class, 'update'] )->name( 'home_slider_update' );
Route::get( '/home_slider/delete/{home_slider}', [HomeSliderController::class, 'delete'] )->name( 'home_slider_delete' );


// news
Route::get( '/our-news', [HomePageNewsController::class, 'index'] )->name( 'our-news' );
Route::get( '/our_news/add', [HomePageNewsController::class, 'create'] )->name( 'our_news_create' );
Route::post( '/our_news/store', [HomePageNewsController::class, 'store'] )->name( 'our_news_store' );
Route::get( '/our_news/{our_news}', [HomePageNewsController::class, 'edit'] )->name( 'our_news_edit' );
Route::post( '/our_news/update/{our_news}', [HomePageNewsController::class, 'update'] )->name( 'our_news_update' );
Route::get( '/our_news/delete/{our_news}', [HomePageNewsController::class, 'delete'] )->name( 'our_news_delete' );


// recipes

Route::get( '/recipes', [HomePageRecipeController::class, 'index'] )->name( 'home-recipes' );
Route::get( '/recipes/add', [HomePageRecipeController::class, 'create'] )->name( 'recipes_create' );
Route::post( '/recipes/store', [HomePageRecipeController::class, 'store'] )->name( 'recipes_store' );
Route::get( '/recipes/{recipes}', [HomePageRecipeController::class, 'edit'] )->name( 'recipes_edit' );
Route::post( '/recipes/update/{recipes}', [HomePageRecipeController::class, 'update'] )->name( 'home_recipes_update' );
Route::get( '/recipes/delete/{recipes}', [HomePageRecipeController::class, 'delete'] )->name( 'recipes_delete' );

// our products

Route::get( '/products', [HomePageProductController::class, 'index'] )->name( 'home-products' );
Route::get( '/products/add', [HomePageProductController::class, 'create'] )->name( 'products_create' );
Route::post( '/products/store', [HomePageProductController::class, 'store'] )->name( 'products_store' );
Route::get( '/products/{product}', [HomePageProductController::class, 'edit'] )->name( 'products_edit' );
Route::post( '/products/update/{product}', [HomePageProductController::class, 'update'] )->name( 'products_update' );
Route::get( '/products/delete/{product}', [HomePageProductController::class, 'delete'] )->name( 'products_delete' );


// products home page setting
Route::get( 'product-page-first-section', [HomePageSettingController::class, 'product_page_first_section'] )->name( 'product_page_first_section' );
Route::post( 'product-page-first-section-update', [HomePageSettingController::class, 'product_page_first_section_update'] )->name( 'product_page_first_section_update' );
Route::get( 'product-page-second-section', [HomePageSettingController::class, 'product_page_second_section'] )->name( 'product_page_second_section' );
Route::post( 'product-page-second-section-update', [HomePageSettingController::class, 'product_page_second_section_update'] )->name( 'product_page_second_section_update' );


// news home page setting
Route::get( 'news-page-section', [HomePageSettingController::class, 'home_page_news_section'] )->name( 'home_page_news_section' );
Route::post( 'home-page-news-section-update', [HomePageSettingController::class, 'news_section_update'] )->name( 'home_page_news_section_update' );



// recipe home page setting

Route::get( 'recipe-first-section', [HomePageSettingController::class, 'recipe_first_section'] )->name( 'recipe_first_section' );
Route::post( 'recipe-first-section-update', [HomePageSettingController::class, 'recipe_first_section_update'] )->name( 'recipe_first_section_update' );
Route::get( 'recipe-second-section', [HomePageSettingController::class, 'recipe_second_section'] )->name( 'recipe_second_section' );
Route::post( 'recipe-second-section-update', [HomePageSettingController::class, 'recipe_second_section_update'] )->name( 'recipe_second_section_update' );



Route::get( 'home-page-zina-section', [HomePageSettingController::class, 'home_page_zina_section'] )->name( 'home_page_zina_section' );
Route::post( 'home-page-zina-section-update', [HomePageSettingController::class, 'zina_section_update'] )->name( 'home_page_zina_section_update' );

Route::get( 'home-page-our-product', [HomePageSettingController::class, 'home_page_our_product'] )->name( 'home_page_our_product' );
Route::post( 'home-page-our-product-update', [HomePageSettingController::class, 'our_product_update'] )->name( 'our_product_update' );

Route::get( 'home-page-our-value', [HomePageSettingController::class, 'home_page_our_value'] )->name( 'home_page_our_value' );
Route::post( 'home-page-our-value-update', [HomePageSettingController::class, 'our_value_update'] )->name( 'our_value_update');

Route::get( 'home-page-zina-products', [HomePageSettingController::class, 'home_page_zina_products'] )->name( 'home_page_zina_products' );
Route::post( 'home-page-zina-products-update', [HomePageSettingController::class, 'zina_products_update'] )->name( 'zina_products_update');

Route::get( 'home-page-news', [HomePageSettingController::class, 'home_page_news'] )->name( 'home_page_news' );
Route::post( 'home-page-news-update', [HomePageSettingController::class, 'news_update'] )->name( 'news_update');

Route::get( 'home-page-recipes', [HomePageSettingController::class, 'home_page_recipes'] )->name( 'home_page_recipes' );
Route::post( 'home-page-recipes-update', [HomePageSettingController::class, 'recipes_update'] )->name( 'recipes_update');

Route::get( 'home-page-navbar', [HomePageSettingController::class, 'home_page_navbar'] )->name( 'home_page_navbar');
Route::post( 'home-page-navbar-update', [HomePageSettingController::class, 'navbar_update'] )->name( 'navbar_update');

Route::get( 'zina-house-section-one', [ZinaHouseController::class, 'zina_house_section_one'] )->name( 'zina_house_section_one');
Route::post( 'zina-house-section-one-update', [ZinaHouseController::class, 'zina_house_section_one_update'] )->name( 'zina_house_section_one_update_update');

Route::get( 'zina-house-section-two', [ZinaHouseController::class, 'zina_house_section_two'] )->name( 'zina_house_section_two');
Route::post( 'zina-house-section-two-update', [ZinaHouseController::class, 'zina_house_section_two_update'] )->name( 'zina_house_section_two_update_update');

Route::get( 'zina-house-section-four', [ZinaHouseController::class, 'zina_house_section_four'] )->name( 'zina_house_section_four');
Route::post( 'zina-house-section-four-update', [ZinaHouseController::class, 'zina_house_section_four_update'] )->name( 'zina_house_section_four_update');

Route::get( 'zina-house-section-five', [ZinaHouseController::class, 'zina_house_section_five'] )->name( 'zina_house_section_five');
Route::post( 'zina-house-section-five-update', [ZinaHouseController::class, 'zina_house_section_five_update'] )->name( 'zina_house_section_five_update');

Route::get( 'zina-house-section-six', [ZinaHouseController::class, 'zina_house_section_six'] )->name( 'zina_house_section_six');
Route::post( 'zina-house-section-six-update', [ZinaHouseController::class, 'zina_house_section_six_update'] )->name( 'zina_house_section_six_update');

Route::get( 'zina-house-section-seven', [ZinaHouseController::class, 'zina_house_section_seven'] )->name( 'zina_house_section_seven');
Route::post( 'zina-house-section-seven-update', [ZinaHouseController::class, 'zina_house_section_seven_update'] )->name( 'zina_house_section_seven_update');

Route::get( 'zina-house-section-eight', [ZinaHouseController::class, 'zina_house_section_eight'] )->name( 'zina_house_section_eight');
Route::post( 'zina-house-section-eight-update', [ZinaHouseController::class, 'zina_house_section_eight_update'] )->name( 'zina_house_section_eight_update');


Route::get( '/zina-house-slider', [ZinaHouseSliderController::class, 'index'] )->name( 'zina_house_slider' );
Route::get( '/zina-house_slider/add', [ZinaHouseSliderController::class, 'create'] )->name( 'zina_house_slider_create' );
Route::post( '/zina-house_slider/store', [ZinaHouseSliderController::class, 'store'] )->name( 'zina_house_slider_store' );
Route::get( '/zina-house_slider/{zina_house_slider}', [ZinaHouseSliderController::class, 'edit'] )->name( 'zina_house_slider_edit' );
Route::post( '/zina-house_slider/update/{zina_house_slider}', [ZinaHouseSliderController::class, 'update'] )->name( 'zina_house_slider_update' );
Route::get( '/zina-house_slider/delete/{zina_house_slider}', [ZinaHouseSliderController::class, 'delete'] )->name( 'zina_house_slider_delete' );


Route::get( 'our-value-section-one', [OurValueController::class, 'our_value_section_one'] )->name( 'our_value_section_one');
Route::post( 'our-value-section-one-update', [OurValueController::class, 'our_value_section_one_update'] )->name( 'our_value_section_one_update');

Route::get( 'our-value-section-two', [OurValueController::class, 'our_value_section_two'] )->name( 'our_value_section_two');
Route::post( 'our-value-section-two-update', [OurValueController::class, 'our_value_section_two_update'] )->name( 'our_value_section_two_update');

Route::get( 'our-value-section-three', [OurValueController::class, 'our_value_section_three'] )->name( 'our_value_section_three');
Route::post( 'our-value-section-three-update', [OurValueController::class, 'our_value_section_three_update'] )->name( 'our_value_section_three_update');

Route::get( 'our-value-section-four', [OurValueController::class, 'our_value_section_four'] )->name( 'our_value_section_four');
Route::post( 'our-value-section-four-update', [OurValueController::class, 'our_value_section_four_update'] )->name( 'our_value_section_four_update');

Route::get( 'our-value-section-five', [OurValueController::class, 'our_value_section_five'] )->name( 'our_value_section_five');
Route::post( 'our-value-section-five-update', [OurValueController::class, 'our_value_section_five_update'] )->name( 'our_value_section_five_update');

Route::get( 'our-value-section-six', [OurValueController::class, 'our_value_section_six'] )->name( 'our_value_section_six');
Route::post( 'our-value-section-six-update', [OurValueController::class, 'our_value_section_six_update'] )->name( 'our_value_section_six_update');

Route::get( 'our-value-section-seven', [OurValueController::class, 'our_value_section_seven'] )->name( 'our_value_section_seven');
Route::post( 'our-value-section-seven-update', [OurValueController::class, 'our_value_section_seven_update'] )->name( 'our_value_section_seven_update');


Route::get('contact-queries', [ProfileController::class, 'contact_queries'])->name('contact-queries');

// legal pages
Route::get( 'legal-pages', [LegalPageController::class, 'index'] )->name( 'legal_pages');
Route::get( 'legal_page/create', [LegalPageController::class, 'create'] )->name( 'legal_page_create');
Route::post( 'legal_page/save', [LegalPageController::class, 'save'] )->name( 'legal_page_store');
Route::get( 'legal_page/edit/{page}', [LegalPageController::class, 'edit'] )->name( 'legal_page_edit');
Route::post( 'legal_page/update/{page}', [LegalPageController::class, 'update'] )->name( 'legal_page_update');
Route::get( 'legal_page/delete/{page}', [LegalPageController::class, 'delete'] )->name( 'legal_page_destroy');


